import { parseJwt } from "../helper/parseJwt";

export class TokenUtil {
  static accessToken = null;
  static refreshToken = null;
  static userId = null;

  static loadToken() {
    if (typeof window === "undefined") {
      return;
    }

    const accessToken = localStorage.getItem("token");

    if (accessToken) {
      TokenUtil.setAccessToken(accessToken);
    }

    if (userId) {
      TokenUtil.setUserId(userId);
    }

    if (refreshToken) {
      TokenUtil.setRefreshToken(refreshToken);
    }
  }

  static setAccessToken(accessToken) {
    TokenUtil.accessToken = accessToken;
  }

  static setUserId(userId) {
    TokenUtil.userId = userId;
  }

  static setRefreshToken(refreshToken) {
    TokenUtil.refreshToken = refreshToken;
  }

  static clearAccessToken() {
    TokenUtil.accessToken = null;
  }
  static clearRefreshToken() {
    TokenUtil.accessToken = null;
  }
}
