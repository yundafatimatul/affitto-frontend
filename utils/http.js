import superagent from "superagent";
import superagentIntercept from "superagent-intercept";
import { appConfig } from "../config/app.js";
import { TokenUtil } from "./token";
import { attachSuperagentLogger } from "./http_logger";

export const AuthIntercept = superagentIntercept((err, res) => {
  if (res && res.status === 401) {
    TokenUtil.clearAccessToken();
    localStorage.removeItem("token");
    window.location.href = "/auth/login";
  }
});

export const http = {
  fetcher: async (url) => {
    let req = superagent
      .get(appConfig.apiUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);
    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    // }

    const resp = await req;

    // if(resp.body.results) {
    //     return resp.body.results;
    // }
    return resp.body;
  },
  get: (url, opts = {}) => {
    let req = superagent
      .get(appConfig.apiUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);
    if (TokenUtil.accessToken) {
      req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    }
    return req;
  },
  post: (url, opts) => {
    let req = superagent
      .post(appConfig.apiUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);
    console.log(localStorage.getItem("token"), "tokennya uy");

    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    return req;
  },
  put: (url, opts) => {
    let req = superagent
      .put(appConfig.apiUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);

    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    return req;
  },
  del: (url, opts) => {
    let req = superagent
      .del(appConfig.apiUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);
    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    return req;
  },
  upload: (url) => {
    let req = superagent.post(appConfig.apiUrl + url).use(AuthIntercept);
    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    // console.log(appConfig, "appconfig");
    // let req = superagent
    //   .post(appConfig.imageApiUrl + url)
    //   .use(AuthIntercept)
    //   .attach("file", file);
    // req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));

    return req;
  },
  uploadAntd: (args) => {
    const file = args.file;
    const request = http.upload(file).use(AuthIntercept);

    request
      .on("progress", (event) => {
        args.onProgress(event);
      })
      .then((it) => {
        args.onSuccess(it);
      })
      .catch((err) => {
        args.onError(err);
      });

    return request;
  },
};
