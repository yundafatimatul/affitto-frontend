import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  user: (id) => `/user/detail/${id}`,
  //   createKost: () => `/kos/`,
  detUser: () => `/user/det`,
};

const hooks = {
  useUser(filter) {
    return useSWR(url.user(filter), http.fetcher);
  },
  useDetUser(filter) {
    return useSWR(url.detUser(filter), http.fetcher);
  },
};

// const manipulateData = {
//   createKos(data) {
//     return http.post(url.createKost()).send(data);
//   },
//   uploadProductImage(data) {
//     return http.upload(url.uploadProductImage(), data);
//   },
// };

export const pelangganRepository = {
  url,
  //   manipulateData,
  hooks,
};
