import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  venue: (id) => `/venues/detail/${id}`,
  review: (id) => `/review/ven/${id}`,
  //   createKost: () => `/kos/`,
};

const hooks = {
  useVenue(filter) {
    return useSWR(url.venue(filter), http.fetcher);
  },
  useReview(filter) {
    return useSWR(url.review(filter), http.fetcher);
  },
};

// const manipulateData = {
//   createKos(data) {
//     return http.post(url.createKost()).send(data);
//   },
//   uploadProductImage(data) {
//     return http.upload(url.uploadProductImage(), data);
//   },
// };

export const tempatRepository = {
  url,
  //   manipulateData,
  hooks,
};
