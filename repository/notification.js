import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  notificationUser: () => `/notification/user`,
  readedAll: () => `/notification/list`,
};

const hooks = {
  useNotificationUser() {
    return useSWR(url.notificationUser(), http.fetcher);
  },
  useNotificationList() {
    return useSWR(url.readedAll(), http.fetcher);
  },
};

const manipulateData = {
  // updateReadAll() {
  //   return http.put(url.readedAll()).send();
  // },
};

export const notifficationRepository = {
  url,
  hooks,
  manipulateData,
};
