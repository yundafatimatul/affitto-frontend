import useSWR from "swr";
import { http } from "../utils/http";
const url = {
  image: () => `/upload/users`,
  venue: () => `/upload/venues`,
  payment: () => `/upload/payment`,
  review: () => `/upload/review`,
  report: () => `/upload/report`,
  adminPayment: () => `/upload/admin-payment`,
  venPhoto: (id) => `/venues/detail-photo/${id}`,
};
const hooks = {
  useImage(filter) {
    return useSWR(url.image(filter), http.fetcher);
  },
  useVenue(filter) {
    return useSWR(url.venue(filter), http.fetcher);
  },
  usePhoto(filter) {
    return useSWR(url.venPhoto(filter), http.fetcher);
  },
  useReview(filter) {
    return useSWR(url.review(filter), http.fetcher);
  },
  useReport(filter) {
    return useSWR(url.report(filter), http.fetcher);
  },
};
const manipulateData = {
  editImage(data) {
    return http.post(url.image()).send(data);
  },
  upload(file) {
    const formData = new FormData();
    formData.append("user", file);
    return http.post(url.image()).send(formData);
  },
  upPayment(file) {
    const formData = new FormData();
    formData.append("payment", file);
    return http.post(url.payment()).send(formData);
  },

  upAdminPayment(file) {
    const formData = new FormData();
    formData.append("admin_payment", file);
    return http.post(url.adminPayment()).send(formData);
  },
  upReview(file) {
    const formData = new FormData();
    formData.append("review", file);
    return http.post(url.review()).send(formData);
  },
  upReport(file) {
    const formData = new FormData();
    formData.append("report", file);
    return http.post(url.report()).send(formData);
  },
  upVenue(file) {
    const formData = new FormData();
    formData.append("venue", file);
    return http.post(url.venue()).send(formData);
  },
};
export const imageRepository = {
  url,
  manipulateData,
  hooks,
};
