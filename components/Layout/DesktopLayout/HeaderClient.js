import React, { useState, useEffect } from "react";
import { Layout, Typography, Dropdown, Menu, Space, Badge, Empty } from "antd";
import { Popover, Transition } from "@headlessui/react";
import {
  EyeInvisibleOutlined,
  EyeOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import logoNavbar from "../../../public/logonavbar.svg";
import { appConfig } from "../../../config/app";
import { parseJwt } from "../../../helper/parseJwt";
import { useRouter } from "next/router";
import { notifficationRepository } from "../../../repository/notification";
import { FaBell } from "react-icons/fa";
import { Fragment } from "react";

const { Sider, Header, Content, Footer } = Layout;
const { Paragraph, Text } = Typography;

const HeaderClient = (props) => {
  const [clicked, setClicked] = useState(false);
  const [user, setUser] = useState();
  const router = useRouter();
  const [notif, setNotif] = useState([]);
  const [notifCount, setNotifCount] = useState(0);

  const { data: userNotif } =
    notifficationRepository.hooks.useNotificationUser();

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }
    const dataJwt = parseJwt(token);
    setUser(dataJwt?.existUser);

    if (userNotif !== undefined && userNotif !== null) {
      console.log(userNotif);
      setNotif(userNotif[0]);
      setNotifCount(userNotif[1]);
    }
  }, [userNotif]);

  const readAllNotif = async () => {
    try {
      await notifficationRepository.manipulateData.updateReadAll();
      await mutate(notifficationRepository.url.notificationUser());
    } catch (error) {
      console.log(error);
      message.error("terjadi kesalahan");
    }
  };

  const handleClick = async () => {
    const a = localStorage.removeItem("token");
    router.push("/");
  };

  const menuAcc = (
    <Menu
      items={[
        {
          label: (
            <Link href={`/pengguna/pelanggan/detail-profil`}>Profil Saya</Link>
          ),
          key: "0",
        },
        {
          type: "divider",
        },
        {
          label: <div onClick={handleClick}>Logout</div>,
          key: "1",
        },
      ]}
    />
  );

  const profile = (
    <div>
      <Dropdown overlay={menuAcc} trigger={["click"]}>
        <a onClick={(e) => e.preventDefault()}>
          <Space>
            <div className="mr-5 mt-2">
              <img
                src={appConfig.apiUrl + `/upload/get-user/${user?.photo}`}
                className="rounded-full h-[40px] w-[40px]"
              />
            </div>
          </Space>
        </a>
      </Dropdown>
    </div>
  );

  const login = (
    <div>
      <Link href="/auth/login">
        <span className="text-white cursor-default">Login</span>
      </Link>
    </div>
  );
  const link = () => {
    if (user?.role?.name == "admin") {
      return "/notification/admin";
    } else if (user?.role?.name == "owner") {
      return "/notification/mitra";
    } else if (user?.role?.name == "client") {
      return "/notification/pelanggan";
    } else {
      return "#";
    }
  };

  return (
    <div className="w-full h-12 px-5 bg-primary flex justify-between font-base items-center z-20 top-0">
      <div className="h-fit ">
        <Link href="/">
          <Image src={logoNavbar} alt="logoNavbar" width={130} height={48} />
        </Link>
      </div>
      <div className="flex space-x-4 justify-center cursor-default">
        <Link className="" href="/dashboard/client">
          <span className="text-white">Beranda</span>
        </Link>
        <Link className="" href="/favorit/list">
          <span className="text-white mr-3">Favorit</span>
        </Link>
        <Link className="" href="/pesanan/list-client">
          <span className="text-white">Pesanan</span>
        </Link>
      </div>
      <div className="flex space-x-2 items-center justify-center">
        <div>
          <div>
            <Popover className="relative">
              {({ open }) => (
                <>
                  <Popover.Button
                    className={`
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-full p-3 text-base font-medium ${
                  open ? "hover:bg-main/40" : "hover:bg-slate-600/10"
                } transition-all text-white hover:text-opacity-100 focus:outline-none`}
                  >
                    <Badge size="large" count={notifCount}>
                      <FaBell
                        className={`${
                          open ? "text-gray-300" : "text-white"
                        } text-lg w-5 h-5 mt-2 flex items-center transition-all ease-in-out`}
                      />
                    </Badge>
                  </Popover.Button>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1"
                  >
                    <Popover.Panel className="absolute left-0 z-50 w-screen max-w-sm -translate-x-[80%] mt-5 transform px-4 sm:px-0 lg:max-w-xs">
                      <div className="overflow-hidden divide-y-[1px] divide-dashed divide-slate-600/50 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                        <div className="bg-white py-4 px-5 cursor-default flex justify-between">
                          <div>
                            <span className="text-base font-semibold text-gray-900">
                              Notifikasi
                            </span>
                            <span className="block text-sm text-gray-500">
                              {notifCount == 0 ? (
                                " "
                              ) : (
                                <span>
                                  Kamu mendapatkan {notifCount} pesan yang belum
                                  dibaca
                                </span>
                              )}
                            </span>
                          </div>

                          {notifCount !== 0 ? (
                            <Popover
                              title="tandai sudah dibaca semua"
                              trigger="hover"
                            >
                              <EyeOutlined
                                onClick={readAllNotif}
                                className="text-text hover:text-secondary duration-300 flex items-center text-xl cursor-pointer select-none p-4"
                              />
                            </Popover>
                          ) : (
                            <EyeInvisibleOutlined className="text-text duration-300 flex items-center text-xl cursor-pointer select-none p-4" />
                          )}
                        </div>
                        <div
                          className={`relative grid gap-8 max-h-96 bg-white p-7 lg:grid-cols-1 overflow-hidden`}
                        >
                          {notif?.length == 0 ? (
                            <div className="w-full h-auto flex items-center justify-center">
                              <Empty
                                image={Empty.PRESENTED_IMAGE_SIMPLE}
                                description={"tidak ada notifikasi terbaru"}
                              />
                            </div>
                          ) : (
                            notif?.map((item) => (
                              <a
                                key={item?.id}
                                href={item?.path}
                                className="-m-3 flex items-center rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50"
                              >
                                <InfoCircleTwoTone className="flex justify-center text-[24px]" />
                                <div className="ml-4">
                                  <p className="text-sm font-bold text-gray-900">
                                    {item?.title}
                                  </p>
                                  <p className="text-sm text-gray-500">
                                    {item?.message}
                                  </p>
                                </div>
                              </a>
                            ))
                          )}
                          {notif?.length == 0 ? (
                            ""
                          ) : (
                            <Link href={link()} className="h-fit">
                              <span className="text-primary text-center h-fit  cursor-pointer">
                                lihat semua
                              </span>
                            </Link>
                          )}
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
          </div>
        </div>
        <div>{user ? profile : login}</div>
      </div>
    </div>
  );
};

export default HeaderClient;
