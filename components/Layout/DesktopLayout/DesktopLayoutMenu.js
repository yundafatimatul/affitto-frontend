import React, { useState } from "react";
import { Menu, Layout, Result, Button } from "antd";
import Link from "next/link";
import {
  MdDashboard,
  MdLocationOn,
  MdReport,
  MdStorage,
  MdPeople,
  MdLiveHelp,
  MdOutlineMenuBook,
  MdGppGood,
  MdWatchLater,
  MdTextsms,
  MdMonetizationOn,
  MdCancelScheduleSend,
} from "react-icons/md";
import { FaClipboardList } from "react-icons/fa";

const getItem = (label, key, icon, children) => {
  return {
    key,
    icon,
    children,
    label,
  };
};

const items1 = [
  getItem("Lokasi", "2", <MdStorage className="text-[#1363DF] w-4 h-4" />, [
    getItem(<Link href={"/lokasi/tempat/list"}>Tempat</Link>, "3"),
    getItem(<Link href={"/lokasi/kategori/list"}>Kategori</Link>, "4"),
  ]),
  getItem(
    <Link href={"/fasilitas/list"}>Fasilitas</Link>,
    "5",
    <MdGppGood className="text-[#1363DF] w-4 h-4" />
  ),
  getItem("Pengguna", "6", <MdPeople className="text-[#1363DF] w-4 h-4" />, [
    getItem(<Link href={"/pengguna/mitra/list"}>Mitra</Link>, "7"),
    getItem(<Link href={"/pengguna/pelanggan/list"}>Pelanggan</Link>, "8"),
  ]),
  // getItem("Wilayah", "9", <MdLocationOn className="text-[#1363DF] w-4 h-4" />, [
  //   getItem(<Link href={"/wilayah/listProvinsi"}>Provinsi</Link>, "10"),
  //   getItem(<Link href={"/wilayah/listKota"}>Kota</Link>, "11"),
  //   getItem(<Link href={"/wilayah/listKecamatan"}>Kecamatan</Link>, "12"),
  // ]),
  getItem("Laporan", "13", <MdReport className="text-[#1363DF] w-4 h-4" />, [
    getItem(<Link href={"/laporan/tempat/list"}>Tempat</Link>, "14"),
    getItem(<Link href={"/laporan/ulasan/list"}>Ulasan</Link>, "15"),
  ]),
  getItem(
    "Pemasukan",
    "4",
    <MdMonetizationOn className="text-[#1363DF] w-4 h-4" />,
    [
      getItem(<Link href={"/pesanan/pemasukan/list"}>List</Link>, "5"),
      getItem(
        <Link href={"/pesanan/pemasukan/list-rekening"}>Rekening</Link>,
        "6"
      ),
    ]
  ),
  getItem(
    <Link href={"/pengajuan/list"}>Pengajuan Tempat</Link>,
    "16",
    <MdLiveHelp className="text-[#1363DF] w-4 h-4" />
  ),
  getItem(
    <Link href={"/pesanan/list-log"}>Log Pesanan</Link>,
    "17",
    <MdOutlineMenuBook className="text-[#1363DF] w-4 h-4" />
  ),
];

const items2 = [
  getItem(
    <Link href={"/lokasi/tempat/list"}>Tempat</Link>,
    "7",
    <MdStorage className="text-[#1363DF] w-4 h-4" />
  ),
  getItem(
    <Link href={"/pesanan/list-pesanan-baru"}>Pesanan Baru</Link>,
    "2",
    <FaClipboardList className="text-[#1363DF] w-4 h-4" />
  ),
  getItem(
    <Link href={"/ulasan/list"}>Ulasan</Link>,
    "3",
    <MdTextsms className="text-[#1363DF] w-4 h-4" />
  ),
  getItem(
    "Pemasukan",
    "4",
    <MdMonetizationOn className="text-[#1363DF] w-4 h-4" />,
    [
      getItem(<Link href={"/pesanan/pemasukan/list"}>List</Link>, "5"),
      getItem(
        <Link href={"/pesanan/pemasukan/list-rekening"}>Rekening</Link>,
        "6"
      ),
    ]
  ),
  getItem(
    <Link href={"/pesanan/riwayat/list"}>Riwayat Pesanan</Link>,
    "8",
    <MdWatchLater className="text-[#1363DF] w-4 h-4" />
  ),
  getItem(
    <Link href={"/pesanan/list-pembatalan"}>Pembatalan</Link>,
    "9",
    <MdCancelScheduleSend className="text-[#1363DF] w-4 h-4" />
  ),
];

const DesktopLayoutMenu = (props) => {
  if (props.role === "admin") {
    return (
      <div className="h-full shadow-md">
        <Menu
          defaultSelectedKey={["1"]}
          mode="inline"
          items={items1}
          style={{ height: "100%", borderRight: 0 }}
        />
      </div>
    );
  }

  if (props.role === "owner") {
    return (
      <div className="h-full shadow-md">
        <Menu
          defaultSelectedKey={["1"]}
          mode="inline"
          items={items2}
          style={{ height: "100%", borderRight: 0 }}
        />
      </div>
    );
  }

  if (props.role === "client") {
    return (
      <Result
        status="404"
        title="404"
        subTitle="Oops... Halaman yang anda cari tidak ada"
        extra={
          <Button type="primary" href="/home">
            Kembali ke beranda
          </Button>
        }
      />
    );
  }
};

export default DesktopLayoutMenu;
