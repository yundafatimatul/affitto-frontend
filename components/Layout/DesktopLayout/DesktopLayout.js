import React, { useEffect, useState } from "react";
import { Button, Layout, Result, Typography } from "antd";
import DesktopLayoutHeader from "./DesktopLayoutHeader";
import DesktopLayoutMenu from "./DesktopLayoutMenu";
import { parseJwt } from "../../../helper/parseJwt";

const { Sider, Header, Content, Footer } = Layout;
const { Paragraph, Text } = Typography;

const DesktopLayout = ({ children }) => {
  const [role, setRole] = useState("");

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt?.existUser) {
      setRole(decodeJwt.existUser.role.name);
    }
  }, []);

  if (role == "client") {
    return (
      <Result
        status="404"
        title="404"
        subTitle="Oops... Halaman yang anda cari tidak ada"
        extra={
          <Button type="primary" href="/home">
            Kembali ke beranda
          </Button>
        }
      />
    );
  } else {
    return (
      <Layout
        theme={"light"}
        className={"bg-[#e8e8e8] min-h-screen"}
        hasSider={true}
      >
        {role == "user" ? "asdas" : "das"}
        <Sider
          className={"bg-white h-screen fixed z-20"}
          width={200}
          style={{}}
        >
          <div
            className={" flex items-center justify-end mb-5 h-12 bg-[#1363DF]"}
          >
            <img
              src="/logonavbar.svg"
              style={{ width: 120, height: 50, marginRight: 50 }}
              alt="logo-navbar"
            />
          </div>
          <DesktopLayoutMenu role={role} />
        </Sider>
        <Layout>
          <DesktopLayoutHeader />
          <Content className={"pr-4 relative ml-[190px]"}>{children}</Content>
        </Layout>
      </Layout>
    );
  }
};

export default DesktopLayout;
