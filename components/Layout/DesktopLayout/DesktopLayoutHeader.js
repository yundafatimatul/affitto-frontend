import React, { useState, useEffect } from "react";
import { Menu, Layout, Typography, Dropdown, Badge, Space, Empty } from "antd";
import { Popover, Transition } from "@headlessui/react";
import { InfoCircleTwoTone } from "@ant-design/icons";
import Link from "next/link";
import { useRouter } from "next/router";
import { parseJwt } from "../../../helper/parseJwt";
import { appConfig } from "../../../config/app";
import { FaBell } from "react-icons/fa";
import { notifficationRepository } from "../../../repository/notification";
import { Fragment } from "react";
import { mutate } from "swr";
import axios from "axios";
import config from "../../../helper/config";
const { Sider, Header, Content, Footer } = Layout;
const { Paragraph, Text } = Typography;

const DesktopLayoutHeader = (props) => {
  const router = useRouter();
  const [user, setUser] = useState();
  const [notif, setNotif] = useState([]);
  const [notifCount, setNotifCount] = useState(0);

  const { data: userNotif } =
    notifficationRepository.hooks.useNotificationUser();

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }
    const dataJwt = parseJwt(token);
    setUser(dataJwt?.existUser);
    console.log(user?.role?.name, "USER");
    if (userNotif !== undefined && userNotif !== null) {
      setNotif(userNotif[0]);
      setNotifCount(userNotif[1]);
    }
  }, [userNotif]);

  const readAllNotif = async () => {
    try {
      // await notifficationRepository.manipulateData.updateReadAll();
      // await mutate(notifficationRepository.url.notificationUser());
      const res = await axios.put(
        `http://localhost:3222/notification/isReadAll`,
        "",
        config
      );
      // .then((response) => {
      //   console.log(response.data);
      //   // setNotif(response.data);
      // });
      // setNotif(re);
      console.log(res);
    } catch (error) {
      console.log(error);
      message.error("terjadi kesalahan");
    }
  };

  const handleClick = async () => {
    const a = localStorage.removeItem("token");
  };

  const menu = (
    <Menu
      items={[
        {
          label: (
            <Link href={`/pengguna/mitra/detail-profil`}>Profil Saya</Link>
          ),
          key: "0",
        },
        {
          type: "divider",
        },
        {
          label: (
            <Link href={"/auth/login"}>
              <a onClick={handleClick}>Logout</a>
            </Link>
          ),
          key: "1",
        },
      ]}
    />
  );

  const link = () => {
    if (user?.role?.name == "admin") {
      return "/notification/admin";
    } else if (user?.role?.name == "owner") {
      return "/notification/mitra";
    } else if (user?.role?.name == "client") {
      return "/notification/pelanggan";
    } else {
      return "#";
    }
  };
  const isRead = async (id) => {
    const res = await axios
      .put(`http://localhost:3222/notification/isRead/${id}`, "", config)
      .then((response) => {
        console.log(response.data);
      });
  };

  return (
    <Header
      className={
        "flex fixed w-full z-10 top-0 items-center justify-end h-12 bg-primary"
      }
    >
      <div className="flex space-x-2 items-center justify-center">
        <div>
          {/* <Badge className="mt-7 mr-3" count={"i"}>
            <FaBell onClick={openNotification} className="text-white w-6 h-6" />
          </Badge> */}
          <Popover className="relative">
            {({ open }) => (
              <>
                <Popover.Button
                  className={`
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-full p-3 text-base font-medium ${
                  open ? "hover:bg-main/40" : "hover:bg-slate-600/10"
                } transition-all text-white hover:text-opacity-100 focus:outline-none`}
                >
                  <Badge
                    size="small"
                    count={notifCount}
                    className=" absolute right-3 h-2"
                  >
                    <FaBell
                      className={`${
                        open ? "text-gray-300" : "text-white"
                      } text-lg w-4 h-10  flex items-center transition-all ease-in-out`}
                    />
                  </Badge>
                </Popover.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1"
                >
                  <Popover.Panel className="absolute left-0 z-10 w-screen max-w-sm -translate-x-[75%] transform px-4 sm:px-0 lg:max-w-xs">
                    <div className="overflow-hidden divide-y-[1px] divide-dashed divide-slate-600/50 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                      <div className="bg-white px-5 cursor-default flex justify-between">
                        <div>
                          <span className="text-base font-semibold text-gray-900">
                            Notifikasi
                          </span>
                          <span className="block text-sm text-gray-500 mb-5">
                            {notifCount == 0 ? (
                              " "
                            ) : (
                              <span className="">
                                Kamu mendapatkan {notifCount} pesan yang belum
                                dibaca
                              </span>
                            )}
                          </span>
                        </div>
                        {/* {notifCount !== 0 ? ( */}
                        {/* // <Popover */}
                        {/* // title="tandai sudah dibaca semua" // trigger="hover" */}
                        {/* // > */}
                        {/* //   <EyeOutlined */}
                        {/* //     onClick={readAllNotif} */}
                        {/* // className="text-text hover:text-secondary
                        duration-300 flex items-center text-xl cursor-pointer
                        select-none p-4" */}
                        {/* //   /> */}
                        {/* // </Popover> */}
                        {/* // ) : ( */}
                        {/* //   <EyeInvisibleOutlined className="text-text duration-300 flex items-center text-xl cursor-pointer select-none p-4" /> */}
                        {/* // )} */}
                      </div>
                      <div
                        className={`relative grid gap-8 bg-white p-7 lg:grid-cols-1 overflow-hidden`}
                      >
                        {notif?.length == 0 ? (
                          <div className="w-full h-auto flex items-center justify-center">
                            <Empty
                              image={Empty.PRESENTED_IMAGE_SIMPLE}
                              description={"tidak ada notifikasi terbaru"}
                            />
                          </div>
                        ) : (
                          notif?.map((item) => (
                            <a
                              // key={item?.id}
                              href={item?.path}
                              onClick={() => {
                                isRead(item?.id);
                              }}
                              className="-mt-3 flex items-center rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50"
                            >
                              <InfoCircleTwoTone className="flex justify-center text-[24px]" />
                              <div className="ml-4">
                                <p className="text-sm font-bold text-gray-900">
                                  {item?.title}
                                </p>
                                <p className="text-sm text-gray-500">
                                  {item?.message}
                                </p>
                              </div>
                            </a>
                          ))
                        )}
                        {notif?.length == 0 ? (
                          ""
                        ) : (
                          <Link href={link()} className="h-fit">
                            <span className="text-primary text-center h-fit -mt-8 cursor-pointer">
                              lihat semua
                            </span>
                          </Link>
                        )}
                      </div>
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
        </div>
        <div>
          <Dropdown overlay={menu} trigger={["click"]}>
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                <div className="mt-6">
                  <img
                    src={appConfig.apiUrl + `/upload/get-user/${user?.photo}`}
                    className="rounded-full h-[38px] w-[38px]"
                  />
                </div>
              </Space>
            </a>
          </Dropdown>
        </div>
      </div>
    </Header>
  );
};

export default DesktopLayoutHeader;
