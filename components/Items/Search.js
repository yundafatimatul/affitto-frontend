import { Button, Input } from "antd";
import React, { Component } from "react";
import { MdSearch } from "react-icons/md";

const Search = (props) => {
  return (
    <div className="mb-5">
      <label class="relative ">
        <span class="sr-only">Search</span>
        <span class="absolute inset-y-0 left-0 flex items-center pl-2">
          <MdSearch className="w-4 h-4 text-[#969696]" />
        </span>
        <input
          className="cursor-pointer relative w-40 h-6 bg-transparent pl-8 outline-none border-b-2 border-primary"
          placeholder="Cari..."
          type="search"
          name="search"
          onChange={props.onChange}
        />
        {/* <Button onClick={props.onClick}>search</Button> */}
      </label>
    </div>
  );
};

export default Search;
