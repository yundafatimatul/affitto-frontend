import React, { useEffect } from "react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import PropTypes from "prop-types";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Thumbs } from "swiper";
import Image from "next/image";
import axios from "axios";

const ImageVenueSlider = (props) => {
  useEffect(() => {
    loadTempat();
  }, []);
  // /venues/detail-photo/:id
  const loadTempat = async () => {
    console.log(props.id, "PROPS");
    // await axios
    //   .get(`http://localhost:3222/venues/detail-photo/${props.id}`)
    //   .then((response) => {
    //     console.log("list FOTO", response.data);
    //     setTempat(response.data);
    //     // return response.data.items;
    //     // console.log(setTempat)
    //   });
  };
  const dataFoto = [
    { image: "/assets/img1.jpg" },
    { image: "/assets/img2.jpg" },
    { image: "/assets/img3.jpg" },
    { image: "/assets/img4.jpg" },
    { image: "/assets/img5.jpg" },
  ];

  return (
    <div>
      <Swiper
        loop={true}
        spaceBetween={10}
        navigation={true}
        modules={[Navigation, Thumbs]}
        grabCursor={true}
        className="w-100"
      >
        {/* {props.images.map((item, index) => (
          <SwiperSlide key={index}>
            <img src={item} alt="Slider Images" />
          </SwiperSlide>
        ))} */}
        {dataFoto.map((v) => {
          return (
            <SwiperSlide>
              <Image
                src={v.image}
                alt="venue"
                width={550}
                height={375}
                className="rounded-lg"
              />
              <h1></h1>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

ImageVenueSlider.PropTypes = {
  images: PropTypes.array.isRequired,
};
export default ImageVenueSlider;
