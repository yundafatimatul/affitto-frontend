import React from "react";

const PageHeader = (props) => {
  return <h2 className="text-xl font-bold top-1 mb-5">{props.pageName}</h2>;
};
export default PageHeader;
