import React, { Component, useState } from "react";
import { Form } from "antd";
import FormItem from "antd/lib/form/FormItem";
import { FaUserAlt, FaAddressCard, FaPhoneAlt, FaHome } from "react-icons/fa";
import { MdEmail, MdLock } from "react-icons/md";
import Link from "next/link";
import ButtonAuth from "./button/ButtonAuth";
import axios from "axios";

const Forms = (props) => {
  const [nameUser, setName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [noPhone, setNoPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  let token;
  if (typeof window !== 'undefined') {
    token = localStorage.getItem('token')
  }
  const config = {
    headers: { Authorization: `Bearer ${token}` }
};
  const handleSubmit = async (e) => {
    // e.preventDefault();
    try {
      const data = {
        name : nameUser,
        username: username,
        password: password,
        no_phone: noPhone,
        email: email,
        address: address,
      };

      const endpoint =`http://localhost:3222/authorization/create/${props}`;

      const res = await axios.post(endpoint, data, config);
      console.log(res);
      setTimeout(message.success('Data Berhasil Dibuat'), 10000); 

    } catch (e) {
      // setError(response.data.message[1]);
      console.log(e.response)
      // alert(e.response.data);
    }};
  return (
    <>
      <div>
        <h1 className="text-2xl font-bold mt-20 text-primary px-8">
          Bergabung jadi {props.name} affitto dan nikmati berbagai
          keuntungannya!
        </h1>
        <div className="border-4 w-20 ml-8 border-primary inline-block"></div>
      </div>

      <div className="flex flex-col items-center mt-12">
        <Form onFinish={console.log("bisa")}>
          <div className="flex flex-row items-center justify-center">
            <FormItem>
              <div>
                <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md mr-8 ">
                  <FaAddressCard className="text-[#969696] m-2 h-5 w-5" />
                  <input
                    type="text"
                    name="name"
                    placeholder="Nama Lengkap"
                    value={nameUser}
                    onChange={(e) => {
                      e.target.name = nameUser
                      setName(e.target.value);
                      // console.log(nameFas)
                    }}
                    className="text-[#969696] bg-[#ECECEC] w-60 outline-none font-light text-sm flex-1 ml-2"
                  />
                </div>
              </div>
            </FormItem>

            <FormItem>
              <div>
                <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md">
                  <MdEmail className="text-[#969696] m-2 h-5 w-5" />
                  <input
                    type="email"
                    name="username"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => {
                      e.target.name = email
                      setEmail(e.target.value);
                      // console.log(nameFas)
                    }}
                    className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                  />
                </div>
              </div>
            </FormItem>
          </div>
          <div className="flex flex-row items-center justify-center">
            <FormItem>
              <div>
                <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md mr-8">
                  <FaUserAlt className="text-[#969696] m-2 h-5 w-5" />
                  <input
                    type="text"
                    name="username"
                    placeholder="Nama Pengguna"
                    value={username}
                    onChange={(e) => {
                      e.target.name = username
                      setUsername(e.target.value);
                      // console.log(nameFas)
                    }}
                    className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                  />
                </div>
              </div>
            </FormItem>

            <FormItem>
              <div>
                <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md">
                  <FaPhoneAlt className="text-[#969696] m-2 h-5 w-5" />
                  <input
                    type="text"
                    name="username"
                    placeholder="No Telepon"
                    value={noPhone}
                    onChange={(e) => {
                      e.target.name = noPhone
                      setNoPhone(e.target.value);
                      // console.log(nameFas)
                    }}
                    className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                  />
                </div>
              </div>
            </FormItem>
          </div>
          <div className="flex flex-row items-center justify-center">
            <FormItem>
              <div>
                <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md mr-8">
                  <MdLock className="text-[#969696] m-2 h-5 w-5" />
                  <input
                    type="password"
                    name="username"
                    placeholder="Kata Sandi"
                    value={password}
                    onChange={(e) => {
                      e.target.name = password
                      setPassword(e.target.value);
                      // console.log(nameFas)
                    }}
                    className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                  />
                </div>
              </div>
            </FormItem>

            <FormItem>
              <div>
                <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md">
                  <FaHome className="text-[#969696] m-2 h-5 w-5" />
                  <input
                    type="text"
                    name="username"
                    placeholder="Alamat"
                    value={address}
                    onChange={(e) => {
                      e.target.name = address
                      setAddress(e.target.value);
                      // console.log(nameFas)
                    }}
                    className="text-[#969696] bg-[#ECECEC] w-60 outline-none font-light text-sm flex-1 ml-2"
                  />
                </div>
              </div>
            </FormItem>
          </div>
          <div className="flex items-center font-light text-[13px] mt-2">
            {/* <label className="flex items-center font-light text-[13px] mt-2">
              <Link href={"registerMitra"} className="text-primary ml-1">
                <a href="/home" className="text-primary ml-1">
                  {" "}
                  Daftar
                </a>
              </Link>
            </label> */}
            {props.name2}
          </div>
          <div className="absolute -right-0 mr-20 ">
            <ButtonAuth buttonAuth={"Daftar"} />
          </div>
        </Form>
      </div>
    </>
  );
};

export default Forms;
