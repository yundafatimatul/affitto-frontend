import React from "react";
import { Button } from "antd";

const Buttons = (props) => {
  return (
    <div>
      <Button
        type="submit"
        className="border-2 border-primary font-semibold text-primary rounded-full px-8 py-1 inline-block hover:bg-primary hover:text-white"
      >
        {props.buttonAuth}
      </Button>
    </div>
  );
};

export default Buttons;
