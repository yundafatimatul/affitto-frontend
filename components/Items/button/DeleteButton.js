import React from "react";
import { Button, Modal } from "antd";
import { MdDelete } from "react-icons/md";

const DeleteButton = () => {
  const { confirm } = Modal;
  function showDeleteConfirm() {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      onOk() {
        console.log("OKEE");
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  return (
    <div>
      <a className="border-0 bg-transparent " onClick={showDeleteConfirm}>
        <MdDelete className="text-delete m-2 w-5 h-5" />
      </a>
    </div>
  );
};

export default DeleteButton;
