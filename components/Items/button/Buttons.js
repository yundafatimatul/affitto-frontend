import React from "react";
import { Button } from "antd";

const Buttons = (props) => {
  return (
    <div>
      <Button className="border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 inline-block hover:bg-primary hover:text-white">
        {props.buttonName}
      </Button>
    </div>
  );
};

export default Buttons;
