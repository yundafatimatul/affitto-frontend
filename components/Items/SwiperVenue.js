import React from "react";
import { FaMapMarkerAlt } from "react-icons/fa";
import Link from "next/link";
import { Routes } from "react-router-dom";
import { useRouter } from "next/router";
import { List, Card } from "antd";
import { FcLike } from "react-icons/fc";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Navigation, Thumbs } from "swiper";
import { appConfig } from "../../config/app";

const SwiperVenue = (props) => {
  const router = useRouter();

  const pgs = Math.floor(props.totalItems / 4);

  return (
    <div className=" flex z-10 mb-5 mt-5">
      <div className="w-full overflow-hidden px-5">
        <List
          grid={{
            gutter: 16,
            column: 4,
          }}
          pagination={{
            // position: ["none", "bottomCenter"],
            onChange: props.onCurrent,
            pageSize: 8,
            total: pgs,
          }}
          dataSource={props.data}
          renderItem={(item) => (
            <List.Item>
              <Link href={`/lokasi/tempat/client/${item.id}`}>
                <Card
                  title={item.nama}
                  className="drop-shadow-xl rounded-2xl h-96 mb-8"
                >
                  <div className="rounded-lg h-44 overflow-hidden bg-red relative">
                    <Swiper
                      loop={true}
                      spaceBetween={10}
                      navigation={true}
                      modules={[Navigation, Thumbs]}
                      grabCursor={true}
                      className="w-100"
                    >
                      {item.image.map((v) => {
                        return (
                          <SwiperSlide>
                            <img
                              src={appConfig.apiUrl + "/upload/get-venue/" + v}
                              alt="venue"
                              preview={false}
                              className="rounded-xl h-fit w-fit"
                            />
                          </SwiperSlide>
                        );
                      })}
                    </Swiper>
                  </div>
                  <div className="mt-3 flex flex-row justify-between">
                    <div>
                      <h4 className="text-sm font-semibold text-[#06283D] bg-[#DFF6FF] px-3 py-1 rounded-lg">
                        {item.kategori}
                      </h4>
                    </div>
                    <div className="flex inset-y-0">
                      <FcLike className="mt-[2px]" />
                      <h4 className="text-sm font-semibold ml-1 text-[#06283D]">
                        {item.likes}
                      </h4>
                    </div>
                  </div>
                  <div className=" flex justify-end items-end cursor-default mt-3 ml-1">
                    <h4 className="text-xl text-primary font-semibold ">
                      <span className="text-primary">
                        {new Intl.NumberFormat("id-ID", {
                          style: "currency",
                          currency: "IDR",
                        }).format(item.harga)}
                      </span>
                    </h4>
                  </div>
                  <div className="flex mt-[7px] ml-1 justify-end items-end">
                    <FaMapMarkerAlt className="text-delete mb-[4px] mr-1" />
                    <h6 className="text-[12px] text-gray-500">{item.kota}</h6>
                  </div>
                </Card>
              </Link>
            </List.Item>
          )}
        />
      </div>
    </div>
  );
};

export default SwiperVenue;
