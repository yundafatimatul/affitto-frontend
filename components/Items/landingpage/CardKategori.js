import React from "react";
import Image from "next/dist/client/image";

const CardKategori = (props) => {
  return (
    <div>
      <div
        v-for="feature in features"
        class="flex flex-col p-8 space-y-4 bg-white rounded-lg border border-gray-200 transition-shadow duration-500 ease-out cursor-pointer hover:shadow-xl"
      >
        <div class="flex justify-center items-center">
          <Image src={props.data.image} width={60} height={60} />
          {/* {props.data.img} */}
        </div>
        <div class="flex items-center space-x-3">
          <div class="relative">
            <h2 class="relative text-xl font-display">{props.data.title}</h2>
          </div>
        </div>
        <p class="text-sm leading-relaxed text-gray-500">{props.data.desc}</p>
      </div>
    </div>
  );
};

export default CardKategori;
