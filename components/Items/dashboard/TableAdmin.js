import React from "react";

const TableAdmin = () => {
  return (
    <div>
      <div className="p-6 mt-6 bg-[#DFF6FF] rounded-lg">
        <div className="flex justify-between items-center pb-4">
          <h2 className="text-xl font-semibold leading-loose text-[#06283D]">
            Log Pesanan
          </h2>
        </div>
        <table className="w-full">
          <thead>
            <tr className="text-sm font-semibold text-[#06283D]">
              <td className="py-4 border-b border-gray-700">Customer</td>
              <td className="py-4 border-b border-gray-700">Menu</td>
              <td className="py-4 border-b border-gray-700 text-right">
                Total Payment
              </td>
              <td className="py-4 border-b border-gray-700 text-center">
                Status
              </td>
            </tr>
          </thead>
          <tbody>
            <tr v-for="order in orders" className="text-sm text-gray-500">
              <td className="py-4">
                <div className="flex gap-4 items-center">
                  {/* <img width="32" :src="`/img/${order.avatar}`" alt="" /> */}
                  <span>nama</span>
                </div>
              </td>
              <td className="py-4">menu</td>
              <td className="py-4 tabular-nums text-right">total</td>
              <td className="py-4 flex justify-center">
                <span className="flex justify-center py-1 w-24 font-medium capitalize rounded-full">
                  tsaus
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default TableAdmin;
