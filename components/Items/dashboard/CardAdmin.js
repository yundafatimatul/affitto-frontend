import React from "react";
import { MdArrowCircleDown, MdArrowCircleUp, MdViewList } from "react-icons/md";

const stats = [
  {
    title: "Total Revenue",
    percentage: "+32.40%",
    value: "$10,243.00",
    status: "up",
  },
  {
    title: "Total Dish Ordered",
    percentage: "-12.40%",
    value: "23,456",
    status: "down",
  },
  {
    title: "Total Customer",
    percentage: "+2.40%",
    value: "1,234",
    status: "up",
  },
];

const CardAdmin = (props) => {
  return (
    <div>
      <div className="flex gap-6">
        <div className="flex flex-col p-4 w-1/3 bg-[#DFF6FF] rounded-lg gap-y-3">
          <div className="flex items-center gap-x-3">
            <div className="flex inset-y-0">
              <MdViewList className="w-5 h-5 mr-3" />
              <span>Total Tempat</span>
            </div>
          </div>
          <div className="text-3xl font-extrabold text-[#06283D] flex items-end justify-end">
            70
          </div>
        </div>

        <div className="ex flex-col p-4 w-1/3 bg-[#DFF6FF] rounded-lg gap-y-3">
          <div className="ex items-center gap-x-3">
            <div className="flex inset-y-0">
              <MdViewList className="w-5 h-5 mr-3" />
              <span>Total Mitra</span>
            </div>
          </div>
          <div className="text-3xl font-extrabold text-[#06283D] flex items-end justify-end">
            70
          </div>
        </div>

        <div className="ex flex-col p-4 w-1/3 bg-[#DFF6FF] rounded-lg gap-y-3">
          <div className="ex items-center gap-x-3">
            <div className="flex inset-y-0">
              <MdViewList className="w-5 h-5 mr-3" />
              <span>Total Pengguna</span>
            </div>
          </div>
          <div className="text-3xl font-extrabold text-[#06283D] flex items-end justify-end">
            70
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardAdmin;
