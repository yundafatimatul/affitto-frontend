export const appConfig = {
  apiUrl: process.env.NEXT_PUBLIC_BASE_URL,
  WEBSOCKET_URL: "wss://centrifugo.k3s.bangun-kreatif.com/connection/websocket",
  WEBSOCKET_TOKEN:
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJpbGhhbV93aWJ1IiwiZXhwIjoyMjg1OTI1MDEzfQ.OULRXO4Tu-tLQKkyZK7EhqDgfIFOKUQ-qFhJB60pklE",
};
