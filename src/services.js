import Centrifuge from "centrifuge";
import { appConfig } from "../config/app";

let centrifugeInstance = null;

let openNotificationInstance;

export const centrifugeInit = () => {
  if (centrifugeInstance) {
    return centrifugeInstance;
  }

  centrifugeInstance = new Centrifuge(appConfig.WEBSOCKET_URL);
  centrifugeInstance.setToken(appConfig.WEBSOCKET_TOKEN);

  centrifugeInstance.on("connect", function (ctx) {
    console.log("connected", ctx);
  });

  centrifugeInstance.on("disconnect", function (ctx) {
    console.log("disconnected", ctx);
  });

  centrifugeInstance.connect();

  return centrifugeInstance;
};

export const getCentrifugeInstance = () => {
  if (!centrifugeInstance) {
    centrifugeInit();
  }
  return centrifugeInstance;
};

export const setOpenNotification = (fn) => {
  openNotificationInstance = fn;
};

export const openNotification = (...args) => {
  if (openNotificationInstance) {
    openNotificationInstance(...args);
  }
};
