import { useRouter } from "next/router";
import { TokenUtil } from "../utils/token";

const useAuthenticationPage = (error) => {
  //   const router = useRouter();
  console.log(error);
  if (error.response?.data.statusCode === 401) {
    TokenUtil.clearAccessToken();
    localStorage.removeItem("token");
    window.location.href = "/auth/login";
  } else {
    return error;
  }
};

export default useAuthenticationPage;
