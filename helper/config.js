let token;
if (typeof window !== "undefined") {
  token = localStorage.getItem("token");
}
const config = {
  headers: { Authorization: `Bearer ${token}` },
};

export default config;
