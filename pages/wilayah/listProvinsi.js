import {
  Space,
  Table,
  Modal,
  Input,
  Button,
  Form,
  Card,
  Breadcrumb,
} from "antd";
import React, { useState, useEffect } from "react";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import { MdDelete, MdEdit } from "react-icons/md";
import Search from "../../components/Items/Search";
import { useForm } from "antd/lib/form/Form";
import PageHeader from "../../components/Items/PageHeader";
import DeleteButton from "../../components/Items/button/DeleteButton";

const listProvinsi = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();

  const columns = [
    {
      title: "Provinsi",
      dataIndex: "Provinsi",
      key: "Provinsi",
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="border-0"
            onClick={() => {
              functionEdit(record);
            }}
          >
            <MdEdit className="text-edit m-2 w-5 h-5" />
          </Button>
          <DeleteButton />
        </Space>
      ),
    },
  ];
  const data = [
    {
      key: "1",
      Provinsi: "Jawa Barat",
    },
    {
      key: "2",
      Provinsi: "Jawa Tengah",
    },
    {
      key: "3",
      Provinsi: "Jawa Timur",
    },
    {
      key: "4",
      Provinsi: "DKI Jakarta",
    },
    {
      key: "5",
      Provinsi: "Sumatera Utara",
    },
    {
      key: "6",
      Provinsi: "Sumatera Selatan",
    },
    {
      key: "7",
      Provinsi: "Sumatera Barat",
    },
    {
      key: "8",
      Provinsi: "Nusa Tenggara Barat",
    },
    {
      key: "9",
      Provinsi: "Nusa Tenggara Timur",
    },
  ];

  const tableProps = {
    size,
  };

  const [form] = useForm();
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);

  const showModal = () => {
    setModalCreate(true);
  };

  const handleCancel = () => {
    setModalCreate(false);
    setModalUpdate(false);
    form.resetFields();
  };

  const showModalUpdate = () => {
    setModalUpdate(true);
  };

  const functionEdit = (val) => {
    console.log(val);
    form.setFieldsValue({
      id: val.id,
      provinsi: val.name,
    });
    showModalUpdate();
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>Provinsi</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Provinsi"} />
        <div className="absolute top-7 right-8">
          <Button
            className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
            onClick={showModal}
          >
            + Tambah
          </Button>
        </div>
        <Search />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={data}
        />

        {/* modal Create */}
        <Modal
          name="form-create"
          visible={modalCreate}
          onCancel={handleCancel}
          title="Tambah Provinsi"
          footer={false}
        >
          <Form
            form={form}
            layout="vertical"
            //onFinish={onFinishCreate}
          >
            <Form.Item name="provinsi" label="Nama Provinsi">
              <Input placeholder="Masukkan Provinsi" />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Tambah
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* modal Update */}
        <Modal
          name="form-update"
          visible={modalUpdate}
          onCancel={handleCancel}
          title="Edit Provinsi"
          footer={false}
        >
          <Form
            form={form}
            layout="vertical"
            //onFinish={onFinishUpdate}
          >
            <Form.Item name="id_provinsi" hidden={true}>
              <Input readOnly={true} />
            </Form.Item>
            <Form.Item name="provinsi" label="Nama Provinsi">
              <Input placeholder="Masukkan nama Provinsi" />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Simpan
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    </div>
  );
});

listProvinsi.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listProvinsi;
