import React, { useState, useEffect } from "react";
import {
  Space,
  Table,
  Modal,
  Input,
  Button,
  Form,
  Card,
  Breadcrumb,
} from "antd";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import { MdDelete, MdEdit } from "react-icons/md";
import PageHeader from "../../components/Items/PageHeader";
import Search from "../../components/Items/Search";
import { useForm } from "antd/lib/form/Form";
import DeleteButton from "../../components/Items/button/DeleteButton";

const listKecamatan = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();

  const columns = [
    {
      title: "Kecamatan",
      dataIndex: "Kecamatan",
      key: "Kecamatan",
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="border-0"
            onClick={() => {
              functionEdit(record);
            }}
          >
            <MdEdit className="text-edit m-2 w-5 h-5" />
          </Button>
          <DeleteButton />
        </Space>
      ),
    },
  ];
  const data = [
    {
      key: "1",
      Kecamatan: "Bekasi Timur",
    },
    {
      key: "2",
      Kecamatan: "Bekasi Utara",
    },
    {
      key: "3",
      Kecamatan: "Bekasi Barat",
    },
    {
      key: "4",
      Kecamatan: "Bekasi Selatan",
    },
    {
      key: "5",
      Kecamatan: "Pondok Gede",
    },
    {
      key: "6",
      Kecamatan: "Harapan Baru",
    },
    {
      key: "7",
      Kecamatan: "Harapan Jaya",
    },
    {
      key: "8",
      Kecamatan: "Bantar Gebang",
    },
    {
      key: "9",
      Kecamatan: "Marga Mulya",
    },
  ];

  const tableProps = {
    size,
  };

  const [form] = useForm();
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);

  const showModal = () => {
    setModalCreate(true);
  };

  const handleCancel = () => {
    setModalCreate(false);
    setModalUpdate(false);
    form.resetFields();
  };

  const showModalUpdate = () => {
    setModalUpdate(true);
  };

  const functionEdit = (val) => {
    console.log(val);
    form.setFieldsValue({
      id: val.id,
      kecamatan: val.name,
    });
    showModalUpdate();
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>Kecamatan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Kecamatan"} />
        <div className="absolute top-7 right-8">
          <Button
            className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
            onClick={showModal}
          >
            + Tambah
          </Button>
        </div>
        <Search />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={data}
        />

        {/* modal Create */}
        <Modal
          name="form-create"
          visible={modalCreate}
          onCancel={handleCancel}
          title="Tambah Kecamatan"
          footer={false}
        >
          <Form
            form={form}
            layout="vertical"
            //onFinish={onFinishCreate}
          >
            <Form.Item name="kecamatan" label="Nama Kecamatan">
              <Input placeholder="Masukkan Kecamatan" />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Tambah
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* modal Update */}
        <Modal
          name="form-update"
          visible={modalUpdate}
          onCancel={handleCancel}
          title="Edit Kecamatan"
          footer={false}
        >
          <Form
            form={form}
            layout="vertical"
            //onFinish={onFinishUpdate}
          >
            <Form.Item name="id_kecamatan" hidden={true}>
              <Input readOnly={true} />
            </Form.Item>
            <Form.Item name="kecamatan" label="Nama Kecamatan">
              <Input placeholder="Masukkan nama Kecamatan" />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Simpan
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    </div>
  );
});

listKecamatan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listKecamatan;
