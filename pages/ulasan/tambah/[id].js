import React, { useState, useEffect } from "react";
import PageHeader from "../../../components/Items/PageHeader";
import HeaderClient from "../../../components/Layout/DesktopLayout/HeaderClient";
import {
  Button,
  Form,
  Input,
  Card,
  Breadcrumb,
  Upload,
  Layout,
  Typography,
  message,
} from "antd";
import { Rate } from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../config/app";
import { FaMapMarkerAlt } from "react-icons/fa";
import { imageRepository } from "../../../repository/image";
import useAuthenticationPage from "../../../helper/authenticationPage";
import config from "../../../helper/config";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Navigation, Thumbs } from "swiper";
const { TextArea } = Input;

const tambahUlasan = () => {
  const desc = [
    "Buruk",
    "Tidak Rekomendasi",
    "Standar",
    "Bagus",
    "Bagus Sekali",
  ];
  const [valueRate, setValueRate] = useState("1");
  const [description, setDesc] = useState("");
  const router = useRouter();
  const { id } = router.query;
  const [pesanan, setPesanan] = useState([]);
  const [imageUrl, setImageUrl] = useState();
  const [fileList, setFileList] = useState();
  const [loading, setLoading] = useState(false);

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Ubah foto
      </div>
    </div>
  );

  useEffect(() => {
    loadLogPesanan();
  }, [id]);

  const loadLogPesanan = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/detail/${id}`, config)
        .then((response) => {
          setPesanan(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const handleChange = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const processUpload = await imageRepository.manipulateData.upReview(file);
      setFileList(processUpload.body.data.filename);
      setImageUrl(
        appConfig.apiUrl +
          "/upload/get-review/" +
          processUpload.body.data.filename
      );

      setTimeout(message.success("Gambar Berhasil Di Unggah!"), 10000);
    } catch (e) {
      setTimeout(message.error("Gambar Gagal Di Unggah"), 10000);
    }
  };

  let photo = [];
  pesanan?.order?.venue?.venue_photo.map((a) => {
    photo.push(a.photo);
  });

  const onFinish = async () => {
    try {
      const data = {
        rate: valueRate,
        comment: description,
        photo: fileList,
      };
      const res = await axios.post(
        `http://localhost:3222/review/create/${pesanan?.order?.id}`,
        data,
        config
      );
      setTimeout(message.success("Ulasan Berhasil Dibuat!"), 10000);
      router.push(`/pesanan/list-client`);
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>
            <a href={"/pesanan/list-client"}>List Pesanan</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href={`/pesanan/detail/client/paid/${id}`}>Detail Pesanan</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Beri Ulasan</Breadcrumb.Item>
        </Breadcrumb>

        <div className="px-5">
          <Card className={"relative px-10 rounded-[10px] top-0"}>
            <PageHeader pageName={"Beri Ulasan"} />
            <div class="grid gap-4 grid-cols-2">
              <div>
                <Card className="shadow-md rounded-md">
                  <div className="flex felx-row">
                    <img
                      src={
                        appConfig.apiUrl +
                        "/upload/get-user/" +
                        pesanan?.order?.user?.photo
                      }
                      alt="jamalFoto"
                      width={50}
                      height={50}
                      className="rounded-full"
                    />
                    <div className="ml-4">
                      <Typography className="font-medium text-sm">
                        {pesanan?.order?.user?.name}
                      </Typography>
                      <Typography className="font-medium text-xs text-[#999999]">
                        {pesanan?.order?.user?.username}
                      </Typography>
                    </div>
                  </div>
                  <Form onFinish={onFinish}>
                    <FormItem>
                      {" "}
                      <Rate
                        tooltips={desc}
                        onChange={setValueRate}
                        value={valueRate}
                      />
                      {valueRate ? (
                        <span className="ant-rate-text">
                          {desc[valueRate - 1]}
                        </span>
                      ) : (
                        ""
                      )}
                    </FormItem>
                    <FormItem name="desc">
                      <TextArea
                        placeholder="Berikan Ulasan Anda"
                        className="rounded-md"
                        name="desc"
                        onChange={(e) => {
                          e.target.name = desc;
                          setDesc(e.target.value);
                        }}
                      ></TextArea>
                    </FormItem>
                    <FormItem>
                      <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        customRequest={(args) => handleChange(args)}
                      >
                        {imageUrl ? (
                          <img
                            src={imageUrl}
                            alt="avatar"
                            style={{
                              width: "100%",
                            }}
                          />
                        ) : (
                          uploadButton
                        )}
                      </Upload>
                    </FormItem>
                    <FormItem className="flex justify-end items-end">
                      <Button
                        htmlType="submit"
                        className="border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white"
                      >
                        Kirim
                      </Button>
                    </FormItem>
                  </Form>
                </Card>
              </div>
              <div className="relative">
                <Card className="shadow-md rounded-md px-1 py-1">
                  <Typography className="text-xl font-bold mt-[-15px] text-primary">
                    {pesanan?.order?.venue?.name}
                  </Typography>
                  <div className="flex">
                    <FaMapMarkerAlt className="text-delete mt-[4px] mr-2 mb-[15px]" />
                    {pesanan?.order?.venue?.district?.city?.name}
                  </div>
                  <Swiper
                    loop={true}
                    spaceBetween={10}
                    navigation={true}
                    modules={[Navigation, Thumbs]}
                    grabCursor={true}
                    className="w-100"
                  >
                    {photo.map((v) => {
                      return (
                        <SwiperSlide>
                          <img
                            src={appConfig.apiUrl + "/upload/get-venue/" + v}
                            alt="venue"
                            preview={false}
                            className="rounded-xl h-fit w-fit"
                          />
                        </SwiperSlide>
                      );
                    })}
                  </Swiper>
                </Card>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default tambahUlasan;
