import React, { useState, useEffect } from "react";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { Card, Breadcrumb, Table } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../components/Items/Search";
import PageHeader from "../../components/Items/PageHeader";
import axios from "axios";
import moment from "moment";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";

const listUlasan = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [rv, setReview] = useState([]);
  const [s, setS] = useState("");
  const store = useStore();

  useEffect(() => {
    loadReview("");
  }, []);

  const loadReview = async (sa) => {
    try {
      const res = await axios
        .get(`http://localhost:3222/review/owner?search=${sa}`, config)
        .then((response) => {
          setReview(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const date = (a) => {
    const date = new Date(a);
    const c = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return c;
  };
  const onChange = (e) => {
    loadReview(e.target.value);
  };

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "order",
      key: "order",
      render: (text, record) => (
        <Link href={`/ulasan/${record.id}`}>
          <a className="text-primary">{text?.order_code}</a>
        </Link>
      ),
    },
    {
      title: "Nama Pelanggan",
      dataIndex: "user",
      key: "user",
      render: (text, record) => <p>{text?.name}</p>,
    },

    {
      title: "Nama Tempat",
      dataIndex: "venue",
      key: "venue",
      render: (text) => <p>{text?.name}</p>,
    },
    {
      title: "Tanggal Ulasan",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
      render: (text) => (
        <>
          <p>{date(text)}</p>
        </>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Ulasan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Ulasan Pelanggan"} />
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={rv}
        />
      </Card>
    </div>
  );
});

listUlasan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listUlasan;
