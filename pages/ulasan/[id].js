import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  Card,
  Breadcrumb,
  Typography,
  Modal,
  message,
} from "antd";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import PageHeader from "../../components/Items/PageHeader";
import { Rate } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useRouter } from "next/router";
import axios from "axios";
import { FaMapMarkerAlt } from "react-icons/fa";
import { appConfig } from "../../config/app";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Navigation, Thumbs } from "swiper";

const detailUlasan = observer(() => {
  const [form] = Form.useForm();
  const [modalCreateReport, setModalCreateReport] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const [review, setReview] = useState([]);
  const [report, setReport] = useState("");

  useEffect(() => {
    loadReview();
  }, [id]);

  const loadReview = async () => {
    try {
      await axios
        .get(`http://localhost:3222/review/detail/${id}`, config)
        .then((response) => {
          setReview(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let photo = [];
  review?.venue?.venue_photo.map((a) => {
    photo.push(a.photo);
  });

  const showModal = () => {
    setModalCreateReport(true);
  };

  const handleCancel = () => {
    setModalCreateReport(false);
  };
  const onFinish = async () => {
    try {
      const data = { description: report };
      const res = await axios.post(
        `http://localhost:3222/report-review/report/${id}`,
        data,
        config
      );
      setTimeout(message.success("Ulasan Berhasil Dilaporkan"), 1000);
      router.push("/ulasan/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>
          <a href="/ulasan/list">List Ulasan</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Ulasan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Detail Ulasan"} />

        <div class="grid gap-4 grid-cols-2">
          <div>
            <Card className="shadow-md rounded-md">
              <div className="flex felx-row mb-2">
                <img
                  src={
                    appConfig.apiUrl + "/upload/get-user/" + review?.user?.photo
                  }
                  alt="jamalFoto"
                  width={50}
                  height={50}
                  className="rounded-full"
                />
                <div className="ml-4">
                  <Typography className="font-medium text-sm">
                    {review?.user?.name}
                  </Typography>
                  <Typography className="font-medium text-xs text-[#999999]">
                    @{review?.user?.username}
                  </Typography>
                </div>
              </div>
              <Rate disabled value={review?.rate} />
              <Typography className="font-light text-xs mb-3">
                {review?.comment}
              </Typography>
              <img
                src={appConfig.apiUrl + "/upload/get-review/" + review.photo}
                alt="fotoVenue"
                className="mb-5 h-48 rounded-lg"
              />
            </Card>
          </div>
          <div className="relative">
            <Card className="shadow-md rounded-md">
              <Typography className="text-lg font-bold text-primary">
                {review?.venue?.name}
              </Typography>
              <div className="flex">
                <FaMapMarkerAlt className="text-delete mt-[4px] mr-2" />
                {review?.venue?.district?.city?.name}
              </div>
              <div className="flex text-sm inset-y-0 left-8 font-normal mb-3"></div>
              <Swiper
                loop={true}
                spaceBetween={10}
                navigation={true}
                modules={[Navigation, Thumbs]}
                grabCursor={true}
                className="w-100"
              >
                {photo.map((v) => {
                  return (
                    <SwiperSlide>
                      <img
                        src={appConfig.apiUrl + "/upload/get-venue/" + v}
                        alt="venue"
                        preview={false}
                        className="rounded-xl h-fit w-fit"
                      />
                    </SwiperSlide>
                  );
                })}
              </Swiper>
              {/* <img
                src={appConfig.apiUrl + "/upload/get-venue/" + photo[0]}
                alt="fotoVenue"
                width={450}
                height={250}
                clssName=" mb-3 w-fit rounded-lg"
              /> */}
              <Typography className="text-[22px] font-semibold text-primary">
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(review?.venue?.price)}
              </Typography>
            </Card>
          </div>
        </div>
        <div className="flex items-end justify-end mt-3">
          <Button
            className="border-1 border-delete font-sm text-delete rounded-lg px-3 py-1 inline-block hover:bg-delete hover:text-white"
            onClick={showModal}
          >
            Laporkan Ulasan
          </Button>
        </div>

        {/* Modal Create */}
        <Modal
          name="form-create"
          visible={modalCreateReport}
          onCancel={handleCancel}
          title="Laporkan Ulasan"
          footer={false}
        >
          <Form form={form} layout="vertical" onFinish={onFinish}>
            <div className="flex felx-row mb-2">
              <img
                src={
                  appConfig.apiUrl +
                  "/upload/get-user/" +
                  review?.venue?.user?.photo
                }
                alt="jamalFoto"
                width={50}
                height={50}
                className="rounded-full"
              />
              <div className="ml-4 mt-2">
                <Typography className="font-medium text-sm">
                  {review?.venue?.user?.name}
                </Typography>
                <Typography className="font-medium text-xs text-[#999999]">
                  @{review?.venue?.user?.username}
                </Typography>
              </div>
            </div>
            <Form.Item name="description">
              <TextArea
                className="rounded-md"
                placeholder="Laporkan tentang ulasan ini"
                name="description"
                onChange={(e) => {
                  setReport(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Kirim
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    </div>
  );
});

detailUlasan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailUlasan;
