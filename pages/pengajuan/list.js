import React, { useState, useEffect } from "react";
import { Space, Table, Button, message } from "antd";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { Card, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../components/Items/Search";
import PageHeader from "../../components/Items/PageHeader";
import axios from "axios";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";

const listPengajuanTempat = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();
  useEffect(() => {
    loadPengajuanTempat("");
  }, []);

  const [statePengajuanTempat, setPengajuanTempat] = useState();

  const confirm = async (id) => {
    try {
      const endpoint = `http://localhost:3222/venues/approve/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Tempat Berhasil Dikonfirmasi"), 10000);
      loadPengajuanTempat("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const reject = async (id) => {
    try {
      const endpoint = `http://localhost:3222/venues/reject/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Tempat Berhasil Ditolak"), 10000);
      loadPengajuanTempat("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const loadPengajuanTempat = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/venues/pending?search=${val}`, config)
        .then((response) => {
          setPengajuanTempat(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadPengajuanTempat(e.target.value);
  };

  const columns = [
    {
      title: "Nama Tempat",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link href={`/pengajuan/detail/${record.id}`}>
          <a className="text-[#1363DF]">{text}</a>
        </Link>
      ),
    },
    {
      title: "Nama Mitra",
      dataIndex: "user",
      key: "user",
      render: (text, record) => (
        <Link href={`/pengguna/mitra/${record.id}`}>
          <span className="text-[#1363DF]">{text?.name}</span>
        </Link>
      ),
    },
    {
      title: "Kota",
      dataIndex: "district",
      key: "district",
      render: (text) => <span className="text-black">{text?.city?.name}</span>,
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg border-1 border-[#5ab036] font-sm text-[#5ab036] hover:bg-[#5ab036] hover:text-white"
            htmlType="submit"
            onClick={() => {
              confirm(record.id);
            }}
          >
            Konfirmasi
          </Button>
          <Button
            className="rounded-lg border-1 border-delete font-sm text-delete hover:bg-delete hover:text-white"
            htmlType="submit"
            onClick={() => {
              reject(record.id);
            }}
          >
            Tolak
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Pengajuan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Pengajuan Tempat"} />
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={statePengajuanTempat}
        />
      </Card>
    </div>
  );
});

listPengajuanTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listPengajuanTempat;
