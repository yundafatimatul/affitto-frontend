import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import PageHeader from "../../../components/Items/PageHeader";
import { appConfig } from "../../../config/app";
import { Card, Breadcrumb, Typography, Form, Button, message } from "antd";
import Link from "next/link";
import FormItem from "antd/lib/form/FormItem";
import {
  MdLocationOn,
  MdAccountTree,
  MdGppGood,
  MdCategory,
  MdFamilyRestroom,
} from "react-icons/md";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Thumbs } from "swiper";
import axios from "axios";
import useAuthenticationPage from "../../../helper/authenticationPage";
import { tempatRepository } from "../../../repository/tempat";
import { useRouter } from "next/router";
import config from "../../../helper/config";

const detailPengajuanTempat = (props) => {
  const [ft, setFoto] = useState([]);
  const [fas, setFas] = useState([]);
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    loadPengajuanTempat();
    loadFasilitas();
  }, [id]);

  const { data: dataTempat } = tempatRepository.hooks.useVenue(id);

  const loadPengajuanTempat = async () => {
    try {
      await axios
        .get(`http://localhost:3222/venues/detail-photo/${id}`)
        .then((response) => {
          setFoto(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadFasilitas = async () => {
    try {
      await axios
        .get(`http://localhost:3222/facility/fasilitas/${id}`)
        .then((response) => {
          setFas(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const fasilitas = fas.join(", ");

  const confirm = async () => {
    try {
      const endpoint = `http://localhost:3222/venues/approve/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Tempat Berhasil Dikonfirmasi"), 10000);
      router.push("/pengajuan/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const reject = async (id) => {
    try {
      const endpoint = `http://localhost:3222/venues/reject/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Tempat Berhasil Ditolak"), 10000);
      router.push("/pengajuan/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href="/pengajuan/list">List Pengajuan</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Pengajuan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0 h-min-screen"}>
        <PageHeader pageName={"Detail Pengajuan Tempat"} />

        <div class="grid gap-4 grid-cols-2">
          <div>
            <Card className="shadow-md rounded-md">
              <Typography className="text-[22px] font-semibold mb-5 ">
                {dataTempat?.data?.nama}
              </Typography>
              <div>
                <Swiper
                  loop={true}
                  spaceBetween={10}
                  navigation={true}
                  modules={[Navigation, Thumbs]}
                  grabCursor={true}
                  className="w-100"
                >
                  {ft.map((v) => {
                    return (
                      <SwiperSlide>
                        <img
                          src={
                            appConfig.apiUrl + "/upload/get-venue/" + v.image
                          }
                          alt="venue"
                          preview={false}
                          className="rounded-2xl h-80 w-fit "
                        />
                      </SwiperSlide>
                    );
                  })}
                </Swiper>
              </div>
            </Card>
          </div>
          <div className="relative">
            <Card className="shadow-md rounded-md">
              <Typography className="flex items-end justify-end text-primary text-[24px] font-semibold">
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(dataTempat?.data?.price)}
              </Typography>
              <Form className="relative">
                <FormItem
                  name="deskripsi"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdAccountTree className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Deskripsi
                    </Typography>
                  </div>
                  {dataTempat?.data?.description}
                </FormItem>
                <FormItem
                  name="luas"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdCategory className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Luas Bangunan
                    </Typography>
                  </div>
                  {dataTempat?.data?.building_are}m
                </FormItem>
                <FormItem
                  name="kapasitas"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdFamilyRestroom className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Kapasitas
                    </Typography>
                  </div>
                  {dataTempat?.data?.max_capacity} Orang
                </FormItem>
                <FormItem
                  name="alamat"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdLocationOn className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Alamat
                    </Typography>
                  </div>
                  {dataTempat?.data?.address}
                </FormItem>
                <FormItem
                  name="fasilitas"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0 text-[#999999]">
                    <MdGppGood className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999] ">
                      Fasilitas
                    </Typography>
                  </div>
                  {fasilitas}
                </FormItem>
              </Form>
            </Card>
          </div>
        </div>
        <div className="flex items-end justify-end mt-5">
          <Button
            className="rounded-lg mr-5 border-1 border-[#5ab036] font-sm text-[#5ab036] hover:bg-[#5ab036] hover:text-white"
            htmlType="submit"
            onClick={confirm}
          >
            Konfirmasi
          </Button>
          <Button
            className="rounded-lg border-1 border-delete font-sm text-delete hover:bg-delete hover:text-white"
            htmlType="submit"
            onClick={reject}
          >
            Tolak
          </Button>
        </div>
      </Card>
    </div>
  );
};

detailPengajuanTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailPengajuanTempat;
