import React, { useState, useEffect } from "react";
import { Space, Table, Button, Modal, message } from "antd";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, PageHeader, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import axios from "axios";
import useAuthenticationPage from "../../../helper/authenticationPage";
import config from "../../../helper/config";

const listPelanggan = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();
  useEffect(() => {
    loadPelanggan("", 1);
  }, []);

  const { confirm } = Modal;
  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const endpoint = `http://localhost:3222/user/delete/${data.id}`;
          const res = await axios.delete(endpoint, config);
          console.log(res);
          setTimeout(message.success("Pelanggan Berhasil Di Hapus"), 10000);
          loadPelanggan("", 1);
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const [statePelanggan, setPelanggan] = useState();
  const [totalItems, setTotal] = useState();

  const loadPelanggan = async (val, page) => {
    try {
      const limit = 10;
      await axios
        .get(
          `http://localhost:3222/user/list?search=${val}&limit=${limit}&page=${page}`,
          config
        )
        .then((response) => {
          setTotal(response.data.meta.totalItems);
          setPelanggan(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const onChange = (e) => {
    loadPelanggan(e.target.value, 1);
  };
  const onCurrent = (page) => {
    loadPelanggan("", page);
  };
  const columns = [
    {
      title: "Foto",
      dataIndex: "foto",
      key: "foto",
      render: (text, record, key) => (
        <div>
          <img
            src={
              process.env.NEXT_PUBLIC_BASE_URL +
              `/upload/get-user/${record.photo}`
            }
            alt="pictureList"
            className="rounded-full h-14 w-14"
          />
        </div>
      ),
    },
    {
      title: "Nama Pelanggan",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link href={`/pengguna/pelanggan/${record.id}`}>
          <a className="text-primary">{text}</a>
        </Link>
      ),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Aksi",
      key: "id",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
            onClick={() => {
              showDeleteConfirm(record);
            }}
          >
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>Pelanggan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Pelanggan"} />
        <div className="absolute top-7 right-8">
          <Button className=" border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white">
            <Link href="/pengguna/pelanggan/tambah">+ Tambah</Link>
          </Button>
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
            total: totalItems,
            onChange: onCurrent,
          }}
          columns={columns}
          dataSource={statePelanggan}
        />
      </Card>
    </div>
  );
});

listPelanggan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listPelanggan;
