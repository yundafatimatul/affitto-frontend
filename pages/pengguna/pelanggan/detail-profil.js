import {
  Button,
  Form,
  Space,
  Card,
  Breadcrumb,
  Input,
  Typography,
  Layout,
} from "antd";
import React from "react";
import HeaderClient from "../../../components/Layout/DesktopLayout/HeaderClient";
import PageHeader from "../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import { pelangganRepository } from "../../../repository/pelanggan";
import { appConfig } from "../../../config/app";

const detailProfilpelanggan = (props) => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const router = useRouter();
  const { detailPel } = router.query;

  const { data: dataUser } = pelangganRepository.hooks.useDetUser();
  console.log(dataUser, "DATAA");

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  let photo = appConfig.apiUrl + `/upload/get-user/${dataUser?.data?.photo}`;

  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <div className="px-5">
          <Breadcrumb className="mt-2 mb-2 ml-1 text-[12px]">
            <Breadcrumb.Item>Detail Profil</Breadcrumb.Item>
          </Breadcrumb>

          <Card className={"relative px-3 rounded-[10px] top-0"}>
            <PageHeader pageName={"Detail Profil"} />

            <div className="flex flex-row ">
              <div className="flex flex-col items-center justify-between mt-5 relative ">
                <div className="relative">
                  <Form
                    form={form}
                    layout="vertical"
                    className="grid grid-cols-3 gap-4"
                  >
                    <div className="px-10">
                      <div className="mt-3">
                        <img
                          src={photo}
                          alt="fotoProfil"
                          width={200}
                          height={200}
                          className="rounded-full mt-5"
                        />
                      </div>
                    </div>
                    <div className="mt-5">
                      <FormItem>
                        <Typography.Title className="font-semibold text-sm ml-5">
                          Nama Lengkap :
                        </Typography.Title>
                        <div className="flex items-center bg-white border w-96 h-8 rounded-lg ml-3 ">
                          <Typography className="font-light text-sm ml-3 text-red">
                            {dataUser?.data?.name}
                          </Typography>
                        </div>
                      </FormItem>

                      <FormItem>
                        <Typography.Title className="font-semibold text-sm ml-5">
                          Username :
                        </Typography.Title>
                        <div className="flex items-center bg-white border w-96 h-8 rounded-lg ml-3 ">
                          <Typography className="font-light text-sm ml-3">
                            {dataUser?.data?.username}
                          </Typography>
                        </div>
                      </FormItem>

                      <FormItem>
                        <Typography.Title className="font-semibold text-sm ml-5">
                          Email :
                        </Typography.Title>
                        <div className="flex items-center bg-white border w-96 h-8 rounded-lg ml-3 ">
                          <Typography className="font-light text-sm ml-3">
                            {dataUser?.data?.email}
                          </Typography>
                        </div>
                      </FormItem>
                    </div>
                    <div className="mt-5">
                      <FormItem>
                        <Typography.Title className="font-semibold text-sm ml-5">
                          No. Telp :
                        </Typography.Title>
                        <div className="flex items-center bg-white border w-96 h-8 rounded-lg ml-3 ">
                          <Typography className="font-light text-sm ml-3">
                            {dataUser?.data?.no_phone}
                          </Typography>
                        </div>
                      </FormItem>

                      <FormItem>
                        <Typography.Title className="font-semibold text-sm ml-5">
                          Alamat :
                        </Typography.Title>
                        <div className="flex items-center bg-white border w-96 h-28 rounded-lg ml-3 ">
                          <Typography className="font-light text-sm ml-5">
                            {dataUser?.data?.address}
                          </Typography>
                        </div>
                      </FormItem>

                      <Form.Item>
                        <Space className="flex items-center justify-end">
                          <Button
                            href={`/pengguna/pelanggan/client/${dataUser?.data?.id}`}
                            className="rounded-lg border-1 border-edit font-sm text-edit hover:bg-edit hover:text-white"
                            htmlType="submit"
                          >
                            Edit
                          </Button>
                        </Space>
                      </Form.Item>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default detailProfilpelanggan;
