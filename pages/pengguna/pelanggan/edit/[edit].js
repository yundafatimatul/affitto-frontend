import {
  Button,
  Form,
  message,
  Space,
  Card,
  Breadcrumb,
  Input,
  Upload,
} from "antd";
import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import PageHeader from "../../../../components/Items/PageHeader";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import FormItem from "antd/lib/form/FormItem";
import Link from "next/link";
import { pelangganRepository } from "../../../../repository/pelanggan";
import { useRouter } from "next/router";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import axios from "axios";
import { imageRepository } from "../../../../repository/image";
import { appConfig } from "../../../../config/app";
import config from "../../../../helper/config";

const editPelanggan = (props) => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState();
  const router = useRouter();
  const { edit } = router.query;
  const [fileList, setFileList] = useState();

  const { data: dataPelanggan } = pelangganRepository.hooks.useUser(edit);
  useEffect(() => {
    editPelanggan();
  }, [dataPelanggan]);

  const submitPelanggan = async () => {
    const id = dataPelanggan?.data?.id;
    const data = form.getFieldsValue();
    const allData = {
      name: data.name,
      username: data.username,
      no_phone: data.no_phone,
      email: data.email,
      address: data.address,
      photo: fileList,
    };
    try {
      const res = await axios.put(
        `http://localhost:3222/user/update/${id}`,
        allData,
        config
      );
      setTimeout(message.success("Data Berhasil Diperbarui!"), 10000);
      router.push(`/pengguna/pelanggan/${id}`);
    } catch (e) {
      useAuthenticationPage(e);
      setTimeout(message.error(e.response.data.error), 10000);
    }
  };

  const onFinishFailed = () => {
    message.error("Data Gagal Diperbarui!");
  };

  const validateMessages = {
    required: "${label} Harus Diisi!",
  };

  const handleChange = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const processUpload = await imageRepository.manipulateData.upload(file);
      setFileList(processUpload.body.data.filename);
      setImageUrl(
        appConfig.apiUrl +
          "/upload/get-user/" +
          processUpload.body.data.filename
      );
      setTimeout(message.success("Gambar Berhasil Di Unggah!"), 10000);
    } catch (e) {
      setTimeout(message.error("Gambar Gagal Di Unggah"), 10000);
    }
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Ubah foto
      </div>
    </div>
  );

  async function editPelanggan() {
    form.setFieldsValue({
      name: await dataPelanggan?.data?.name,
      username: await dataPelanggan?.data?.username,
      email: await dataPelanggan?.data?.email,
      no_phone: await dataPelanggan?.data?.no_phone,
      address: await dataPelanggan?.data?.address,
    });
  }

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href={"/pengguna/pelanggan/list"}>Detail Profil</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link href={`/pengguna/pelanggan/${edit}`}>Detail Profil</Link>
        </Breadcrumb.Item>

        <Breadcrumb.Item>Edit Profil</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Edit Profil"} />
        <div className="flex flex-row  mt-5">
          <div className="flex flex-col items-center justify-between mt-5 relative ">
            <div>
              <Form
                form={form}
                layout="vertical"
                className="grid grid-cols-3 gap-4"
                onFinish={submitPelanggan}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                validateMessages={validateMessages}
              >
                <div className="px-10">
                  <div className="mt-3">
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader ml-20"
                      showUploadList={false}
                      customRequest={(args) => handleChange(args)}
                    >
                      {imageUrl ? (
                        <img
                          src={imageUrl}
                          alt="avatar"
                          style={{
                            width: "100%",
                          }}
                        />
                      ) : (
                        uploadButton
                      )}
                    </Upload>
                  </div>
                </div>
                <div className="mt-5">
                  <FormItem
                    name="name"
                    label={" Nama Lengkap"}
                    className="font-semibold text-sm ml-5"
                    rules={[
                      {
                        type: "text",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Input
                      placeholder="Masukan Nama Lengkap"
                      className="text-[black] bg-white outline-none w-80 font-light text-sm h-9 flex-1 rounded-lg"
                    />
                  </FormItem>

                  <FormItem
                    name="username"
                    label="Username"
                    className="font-semibold text-sm ml-5"
                    rules={[
                      {
                        type: "username",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Input
                      placeholder="Masukan Nama Lengkap"
                      className="text-[black] bg-white outline-none w-80 font-light text-sm h-9 flex-1 rounded-lg"
                    />
                  </FormItem>

                  <FormItem
                    name="email"
                    label="Email"
                    className="font-semibold text-sm ml-5"
                    rules={[
                      {
                        type: "email",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Input
                      name="email"
                      placeholder="Masukan Nama Lengkap"
                      className="text-[black] bg-white outline-none w-80 font-light text-sm h-9 flex-1 rounded-lg"
                    />
                  </FormItem>
                </div>

                <div className="mt-5">
                  <FormItem
                    name="no_phone"
                    label="No. Telepon"
                    className="font-semibold text-sm ml-5"
                    rules={[
                      {
                        type: "text",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Input
                      name="no_phone"
                      placeholder="Masukan No. Telp"
                      className="text-[black] bg-white outline-none w-80 font-light text-sm h-9 flex-1 rounded-lg"
                    />
                  </FormItem>

                  <FormItem
                    name="address"
                    label="Alamat"
                    className="font-semibold text-sm ml-5"
                  >
                    <TextArea
                      name="address"
                      placeholder="Masukan Alamat"
                      className="text-[black] bg-white outline-none w-80 font-light text-sm h-14 flex-1 rounded-lg"
                      rules={[
                        {
                          type: "textarea",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    ></TextArea>
                  </FormItem>
                  <Form.Item>
                    <Space>
                      <Button
                        className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                        htmlType="submit"
                      >
                        Simpan
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

editPelanggan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default editPelanggan;
