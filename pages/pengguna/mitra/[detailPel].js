import {
  Button,
  Form,
  message,
  Space,
  Card,
  Breadcrumb,
  Input,
  Typography,
  Modal,
} from "antd";
import React from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import PageHeader from "../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import Link from "next/link";
import axios from "axios";
import { useRouter } from "next/router";
import { pelangganRepository } from "../../../repository/pelanggan";
import useAuthenticationPage from "../../../helper/authenticationPage";
import config from "../../../helper/config";

const detailPelanggan = (props) => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const router = useRouter();
  const { detailPel } = router.query;
  const { confirm } = Modal;

  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const endpoint = `http://localhost:3222/user/delete/${detailPel}`;
          const res = await axios.delete(endpoint, config);
          setTimeout(message.success("Pelanggan Berhasil Di Hapus"), 10000);
          router.push("/pengguna/pelanggan/list");
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }
  const { data: dataPelanggan } = pelangganRepository.hooks.useUser(detailPel);

  let photo =
    process.env.NEXT_PUBLIC_BASE_URL +
    `/upload/get-user/${dataPelanggan?.data?.photo}`;

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href={"/pengguna/mitra/list"}>List Profil</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Profil</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Detail Profil"} />
        <div className="flex flex-row  mt-5">
          <div className="flex flex-col items-center justify-between mt-5 relative ">
            <div className="relative">
              <Form
                form={form}
                layout="vertical"
                className=" grid grid-cols-3 gap-2"
              >
                <div className="w-full px-5">
                  <div className="mt-3">
                    <img
                      src={photo}
                      alt="fotoProfil"
                      width={200}
                      height={200}
                      className="rounded-full mt-8"
                    />
                  </div>
                </div>

                <div className="mt-5">
                  <FormItem>
                    <Typography.Title className="font-semibold text-sm">
                      Nama Lengkap :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg  ">
                      <Typography className="font-light text-sm ml-3 text-red">
                        {dataPelanggan?.data?.name}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm">
                      Username :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg  ">
                      <Typography className="font-light text-sm ml-3 text-red">
                        {dataPelanggan?.data?.username}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm">
                      Email :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg  ">
                      <Typography className="font-light text-sm ml-3 text-red">
                        {dataPelanggan?.data?.email}
                      </Typography>
                    </div>
                  </FormItem>
                </div>

                <div className="mt-5">
                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      No. Telp :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {dataPelanggan?.data?.no_phone}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Alamat :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-28 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-5">
                        {dataPelanggan?.data?.address}
                      </Typography>
                    </div>
                  </FormItem>

                  <Form.Item>
                    <Space className="flex items-center justify-end">
                      <Button
                        className="rounded-lg border-1 border-edit font-sm text-edit hover:bg-edit hover:text-white"
                        htmlType="submit"
                      >
                        <Link
                          href={`/pengguna/pelanggan/edit/${dataPelanggan?.data?.id}`}
                        >
                          Edit
                        </Link>
                      </Button>
                      <Button
                        onClick={() => {
                          showDeleteConfirm();
                        }}
                        className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
                      >
                        Hapus
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

detailPelanggan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailPelanggan;
