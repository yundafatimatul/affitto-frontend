import React, { useState, useEffect } from "react";
import { Space, Table, Tag, Button } from "antd";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb, Modal, message } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import PageHeader from "../../../components/Items/PageHeader";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import axios from "axios";
import { TokenUtil } from "../../../utils/token";
import useAuthenticationPage from "../../../helper/authenticationPage";
const listMitra = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();
  useEffect(() => {
    loadMitra("");
  }, []);
  const { confirm } = Modal;
  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const endpoint = `http://localhost:3222/user/delete/${data.id}`;
          const res = await axios.delete(endpoint, config);
          console.log(res);
          setTimeout(message.success("Mitra Berhasil Di Hapus"), 10000);
          loadMitra("");
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const [stateMitra, setMitra] = useState();
  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  const loadMitra = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/user/listOwner?search=${val}`, config)
        .then((response) => {
          setMitra(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadMitra(e.target.value);
  };

  const columns = [
    {
      title: "Foto",
      dataIndex: "foto",
      key: "foto",
      render: (text, record, key) => (
        <div>
          <img
            src={
              process.env.NEXT_PUBLIC_BASE_URL +
              `/upload/get-user/${record.photo}`
            }
            alt="pictureList"
            className="rounded-full h-14 w-14"
          />
        </div>
      ),
    },
    {
      title: "Nama Mitra",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link href={`/pengguna/mitra/${record.id}`}>
          <a className="text-primary">{text}</a>
        </Link>
      ),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Aksi",
      key: "id",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
            onClick={() => {
              showDeleteConfirm(record);
            }}
          >
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Mitra</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Mitra"} />
        <div className="absolute top-7 right-8">
          <Button className=" border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white">
            <Link href="/pengguna/mitra/tambah">+ Tambah</Link>
          </Button>
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateMitra}
        />
      </Card>
    </div>
  );
});

listMitra.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listMitra;
