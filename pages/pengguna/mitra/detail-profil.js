import { Button, Form, Space, Card, Breadcrumb, Input, Typography } from "antd";
import React from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import PageHeader from "../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import { mitraRepository } from "../../../repository/mitra";
import { appConfig } from "../../../config/app";

const detailProfilMitra = (props) => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const router = useRouter();

  const { data: dataUser } = mitraRepository.hooks.useDetUser();

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  let photo = appConfig.apiUrl + `/upload/get-user/${dataUser?.data?.photo}`;

  return (
    <div className="px-5">
      <Breadcrumb className="mt-2 mb-2 ml-1 text-[12px]">
        <Breadcrumb.Item>Detail Profil</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative rounded-[10px] mt-10 ml-3 px-2"}>
        <PageHeader pageName={"Detail Profil"} />

        <div className="flex flex-row ">
          <div className="flex flex-col items-center justify-between mt-5 relative ">
            <div className="relative">
              <Form
                form={form}
                layout="vertical"
                className=" grid grid-cols-3 gap-2"
              >
                <div className="w-full px-5">
                  <div className="mt-3">
                    {console.log(
                      process.env.NEXT_PUBLIC_BASE_URL +
                        `/upload/get-user/${dataUser?.data?.photo}`
                    )}
                    <img
                      src={photo}
                      alt="fotoProfil"
                      width={200}
                      height={200}
                      className="rounded-full mt-5"
                    />
                  </div>
                </div>
                <div className="mt-5">
                  <FormItem>
                    <Typography.Title className="font-semibold text-sm">
                      Nama Lengkap :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg  ">
                      <Typography className="font-light text-sm ml-3 text-red">
                        {dataUser?.data?.name}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm">
                      Username :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg">
                      <Typography className="font-light text-sm ml-3">
                        {dataUser?.data?.username}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm">
                      Email :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg">
                      <Typography className="font-light text-sm ml-3">
                        {dataUser?.data?.email}
                      </Typography>
                    </div>
                  </FormItem>
                </div>
                <div className="mt-5">
                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      No. Telp :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {dataUser?.data?.no_phone}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Alamat :
                    </Typography.Title>
                    <div className="flex items-center bg-white border w-full h-28 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-5">
                        {dataUser?.data?.address}
                      </Typography>
                    </div>
                  </FormItem>

                  <Form.Item>
                    <Space className="flex items-center justify-end">
                      <Button
                        href={`/pengguna/mitra/edit/${dataUser?.data?.id}`}
                        className="rounded-lg border-1 border-edit font-sm text-edit hover:bg-edit hover:text-white"
                        htmlType="submit"
                      >
                        Edit
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

detailProfilMitra.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailProfilMitra;
