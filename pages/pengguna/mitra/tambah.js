import {
  Button,
  Form,
  message,
  Space,
  Card,
  Breadcrumb,
  Input,
  Typography,
} from "antd";
import React, { useState } from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import PageHeader from "../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import Link from "next/link";
import useAuthenticationPage from "../../../helper/authenticationPage";
import axios from "axios";
import { useRouter } from "next/router";

const tambahMitra = (props) => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const [nameUser, setName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [noPhone, setNoPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const router = useRouter();

  const onFinish = async () => {
    try {
      const datas = {
        name: nameUser,
        username: username,
        password: password,
        no_phone: noPhone,
        email: email,
        address: address,
      };
      const props = "owner";

      const endpoint = `http://localhost:3222/authorization/create/${props}`;

      const res = await axios.post(endpoint, datas);
      console.log(res);
      setTimeout(message.success("Mitra Berhasil Dibuat"), 10000);
      router.push("/pengguna/mitra/list");
    } catch (e) {
      useAuthenticationPage(e);
      setTimeout(message.error(e.response.data.error), 800);
    }
  };

  const onFinishFailed = () => {
    message.error("Data Gagal Diperbarui!");
  };

  const validateMessages = {
    required: "${label} Harus Diisi!",
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href={"/pengguna/mitra/list"}>List Profil</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Tambah Mitra</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Tambah Mitra"} />
        <div className="flex flex-row  mt-5">
          <div className="flex flex-col items-center justify-between mt-5 relative">
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              validateMessages={validateMessages}
            >
              <div className="flex px-10">
                <div className="mr-10">
                  <FormItem
                    name="nama"
                    rules={[
                      {
                        type: "text",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Typography.Title className="font-light text-sm ml-3">
                      Nama Lengkap
                    </Typography.Title>
                    <div>
                      <Input
                        value={nameUser}
                        onChange={(e) => {
                          e.target.name = nameUser;
                          setName(e.target.value);
                        }}
                        placeholder="Masukan Nama Lengkap"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2 rounded-lg"
                      />
                    </div>
                  </FormItem>

                  <FormItem
                    name="username"
                    rules={[
                      {
                        type: "username",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Typography.Title className="font-light text-sm ml-3">
                      Username
                    </Typography.Title>
                    <div>
                      <Input
                        value={username}
                        onChange={(e) => {
                          e.target.name = username;
                          setUsername(e.target.value);
                        }}
                        placeholder="Masukan Username"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2 rounded-lg"
                      />
                    </div>
                  </FormItem>

                  <FormItem
                    name="password"
                    rules={[
                      {
                        type: "password",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Typography.Title className="font-light text-sm ml-3">
                      Kata Sandi
                    </Typography.Title>
                    <div>
                      <Input
                        type="password"
                        value={password}
                        onChange={(e) => {
                          e.target.name = password;
                          setPassword(e.target.value);
                        }}
                        placeholder="Masukan Kata Sandi"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2 rounded-lg"
                      />
                    </div>
                  </FormItem>
                </div>
                <div>
                  <FormItem
                    name="email"
                    rules={[
                      {
                        type: "email",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Typography.Title className="font-light text-sm ml-3">
                      Email
                    </Typography.Title>
                    <div>
                      <Input
                        value={email}
                        onChange={(e) => {
                          e.target.name = email;
                          setEmail(e.target.value);
                        }}
                        placeholder="Masukan Email"
                        type="email"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2 rounded-lg"
                      />
                    </div>
                  </FormItem>

                  <FormItem
                    name="no_telp"
                    rules={[
                      {
                        type: "text",
                        warningOnly: true,
                      },
                      {
                        type: "string",
                      },
                    ]}
                  >
                    <Typography.Title className="font-light text-sm ml-3">
                      No. Telp
                    </Typography.Title>
                    <div>
                      <Input
                        value={noPhone}
                        onChange={(e) => {
                          e.target.name = noPhone;
                          setNoPhone(e.target.value);
                        }}
                        placeholder="Masukan No. Telp"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-10 flex-1 ml-2 rounded-lg"
                      />
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-light text-sm ml-3">
                      Alamat
                    </Typography.Title>
                    <TextArea
                      name="alamat"
                      value={address}
                      onChange={(e) => {
                        e.target.name = address;
                        setAddress(e.target.value);
                      }}
                      placeholder="Masukan Alamat"
                      className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-10 flex-1 ml-2 rounded-lg"
                      rules={[
                        {
                          type: "textarea",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    ></TextArea>
                  </FormItem>

                  <Form.Item>
                    <Space>
                      <Button
                        className="rounded-lg absolute right-0 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                        htmlType="submit"
                      >
                        Simpan
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </div>
            </Form>
          </div>
        </div>
      </Card>
    </div>
  );
};

tambahMitra.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default tambahMitra;
