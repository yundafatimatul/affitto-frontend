import React, { useState } from "react";
import { FaUserAlt, FaLock } from "react-icons/fa";
import { observer } from "mobx-react-lite";
import { Form, Space, Button } from "antd";
import ParticlesLayout from "../../components/Layout/ParticlesLayout";
import FormItem from "antd/lib/form/FormItem";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import { parseJwt } from "../../helper/parseJwt";

const Login = observer(() => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [error, setError] = useState("");
  const router = useRouter();

  console.log(router);
  let tok;
  if (typeof window !== "undefined") {
    tok = localStorage.getItem("token");
  }
  if (tok) {
    const decodeJwt = parseJwt(tok);
    let role;
    if (decodeJwt.existUser) {
      role = decodeJwt.existUser?.role?.name;
      if (role == "client") {
        router.push("/dashboard/client");
      } else if (role == "admin") {
        router.push("/lokasi/tempat/list");
      } else if (role == "owner") {
        router.push("/pesanan/list-pesanan-baru");
      }
    }
  }

  const handleSubmit = async (e) => {
    // e.preventDefault();

    try {
      const data = {
        username: username,
        password: password,
      };

      const endpoint = "http://localhost:3222/authorization/login";

      const res = await axios.post(endpoint, data);
      console.log(res.data);
      const token = res.data;
      console.log(token);
      localStorage.setItem("token", token);
      const decodeJwt = parseJwt(token);
      let role;
      if (decodeJwt.existUser) {
        role = decodeJwt.existUser?.role?.name;
      }
      if (role == "client") {
        router.push("/dashboard/client");
        console.log("client");
      } else if (role == "admin") {
        router.push("/lokasi/tempat/list");
        console.log("admin");
      } else if (role == "owner") {
        router.push("/lokasi/tempat/list");
        console.log("owner");
      }
    } catch (e) {
      setError(e.response.data.error);
      console.log(e);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2 bg-white font-Poppins">
      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
        <div className="bg-white rounded-2xl shadow-2xl flex md:flex-col xl:flex-row w-2/3 max-w-4xl">
          <div className="xl:w-3/6 md:w-full sm:w-full p-5">
            <div>
              <img
                src="/logoAF.svg"
                style={{ width: 100, height: 30, marginLeft: 20 }}
                alt="logo"
              />
            </div>
            <h2 className="text-2xl font-bold mt-8 text-[#1363DF]">
              Masuk ke akun anda
            </h2>
            <div className="border-2 w-10 border-[#1363DF] inline-block mb-3"></div>

            <div className="text-[10px] text-delete font-semibold flex justify-center items-center">
              {error}
            </div>

            <Form onFinish={handleSubmit}>
              <FormItem
                rules={[{ required: false, message: "Username harus diisi!" }]}
              >
                <div className="flex flex-col items-center bg-[#FFFFFF]">
                  <div className="bg-[#ECECEC] w-64 md:w-64 p-1 flex items-center rounded-md mt-5">
                    <FaUserAlt className="text-[#969696] m-2 w-5 h-5" />
                    <input
                      type="text"
                      name="username"
                      value={username}
                      onChange={(e) => {
                        e.target.username = username;
                        setUsername(e.target.value);
                        console.log(username);
                      }}
                      placeholder="Nama Pengguna"
                      className="text-[#969696] bg-[#ECECEC] w-52 outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>
              <FormItem
                rules={[
                  { required: false, message: "Kata Sandi harus diisi!" },
                ]}
              >
                <div className="flex flex-col items-center">
                  <div className="bg-[#ECECEC] w-52 md:w-64 p-1 flex items-center rounded-md">
                    <FaLock className="text-[#969696] m-2 w-5 h-5" />
                    <input
                      type="password"
                      name="password"
                      value={password}
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                      placeholder="Kata Sandi"
                      className="text-[#969696] bg-[#ECECEC] w-52 outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>

                  <div className="flex w-64 mb-5">
                    <label className="flex items-center font-light text-[11px] mt-2">
                      Belum punya akun ?
                      <Link href={"register-pelanggan"}>
                        <span className="text-[#1363DF] ml-1 cursor-pointer">
                          Daftar
                        </span>
                      </Link>
                    </label>
                  </div>
                </div>
              </FormItem>
              <Space>
                <Button
                  className=" rounded-lg border-1 border-[#1363DF] font-sm text-[#1363DF] px-3 py-1 inline-block hover:bg-[#1363DF] hover:text-white"
                  htmlType="submit"
                >
                  Masuk
                </Button>
              </Space>
            </Form>
          </div>

          <div className="xl:w-3/6 xl:block md:hidden sm:hidden bg-[#DFF6FF] rounded-tr-2xl rounded-br-2xl py-36 px-12">
            <img
              src="/imgLogin.svg"
              style={{ width: 280, height: 180 }}
              alt="imglogin"
            />
          </div>
        </div>
      </main>
    </div>
  );
});

Login.getLayout = function Layout(page) {
  return <ParticlesLayout>{page}</ParticlesLayout>;
};

export default Login;
