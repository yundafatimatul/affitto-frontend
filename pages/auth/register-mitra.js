import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import ParticlesLayout from "../../components/Layout/ParticlesLayout";
import Image from "next/image";
import LogoRegister from "../../public/logoregister.svg";
import ImgRegister from "../../public/imgregister.svg";
import { Form, Button, message } from "antd";
import FormItem from "antd/lib/form/FormItem";
import { FaUserAlt, FaAddressCard, FaPhoneAlt, FaHome } from "react-icons/fa";
import { MdEmail, MdLock } from "react-icons/md";
import { useRouter } from "next/router";
import axios from "axios";
import Link from "next/link";

const Register = observer((props) => {
  const [loading, setLoading] = useState(false);
  const [nameUser, setName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [noPhone, setNoPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const router = useRouter();

  const handleSubmit = async (e) => {
    // e.preventDefault();
    try {
      const datas = {
        name: nameUser,
        username: username,
        password: password,
        no_phone: noPhone,
        email: email,
        address: address,
      };
      const props = "owner";

      const endpoint = `http://localhost:3222/authorization/create/${props}`;

      const res = await axios.post(endpoint, datas);
      console.log(res);
      setTimeout(message.success("Data Berhasil Dibuat"), 10000);
      router.push("/auth/login");
    } catch (e) {
      setTimeout(message.error(e.response.data.error), 800);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="flex min-h-screen bg-white">
      <div className="basis-5/12 flex items-center justify-center bg-[#DFF6FF]">
        <div className="flex flex-col">
          <div className="absolute inset-x-0 top-10 ml-20 ">
            <Image src={LogoRegister} alt="logoregister" width={150} />
          </div>
          <div>
            <Image src={ImgRegister} alt="imgregister" width={550} />
          </div>
        </div>
      </div>

      <div className="basis-7/12 relative">
        <div>
          <h1 className="text-2xl font-bold mt-20 text-primary px-8">
            Bergabung jadi Mitra affitto dan nikmati berbagai keuntungannya!
          </h1>
          <div className="border-4 w-20 ml-8 border-primary inline-block"></div>
        </div>

        <div className="flex flex-col items-center mt-12">
          <Form onFinish={handleSubmit}>
            <div className="flex flex-row items-center justify-center">
              <FormItem>
                <div>
                  <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md mr-8 ">
                    <FaAddressCard className="text-[#969696] m-2 h-5 w-5" />
                    <input
                      type="text"
                      name="name"
                      placeholder="Nama Lengkap"
                      value={nameUser}
                      onChange={(e) => {
                        e.target.name = nameUser;
                        setName(e.target.value);
                      }}
                      className="text-[#969696] bg-[#ECECEC] w-60 outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>

              <FormItem>
                <div>
                  <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md">
                    <MdEmail className="text-[#969696] m-2 h-5 w-5" />
                    <input
                      type="email"
                      name="username"
                      placeholder="Email"
                      value={email}
                      onChange={(e) => {
                        e.target.name = email;
                        setEmail(e.target.value);
                      }}
                      className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>
            </div>
            <div className="flex flex-row items-center justify-center">
              <FormItem>
                <div>
                  <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md mr-8">
                    <FaUserAlt className="text-[#969696] m-2 h-5 w-5" />
                    <input
                      type="text"
                      name="username"
                      placeholder="Nama Pengguna"
                      value={username}
                      onChange={(e) => {
                        e.target.name = username;
                        setUsername(e.target.value);
                      }}
                      className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>

              <FormItem>
                <div>
                  <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md">
                    <FaPhoneAlt className="text-[#969696] m-2 h-5 w-5" />
                    <input
                      type="text"
                      name="username"
                      placeholder="No Telepon"
                      value={noPhone}
                      onChange={(e) => {
                        e.target.name = noPhone;
                        setNoPhone(e.target.value);
                      }}
                      className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>
            </div>
            <div className="flex flex-row items-center justify-center">
              <FormItem>
                <div>
                  <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md mr-8">
                    <MdLock className="text-[#969696] m-2 h-5 w-5" />
                    <input
                      type="password"
                      name="username"
                      placeholder="Kata Sandi"
                      value={password}
                      onChange={(e) => {
                        e.target.name = password;
                        setPassword(e.target.value);
                      }}
                      className="text-[#969696] bg-[#ECECEC] w-60  outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>

              <FormItem>
                <div>
                  <div className="bg-[#ECECEC] w-72 p-1 flex items-center rounded-md">
                    <FaHome className="text-[#969696] m-2 h-5 w-5" />
                    <input
                      type="text"
                      name="username"
                      placeholder="Alamat"
                      value={address}
                      onChange={(e) => {
                        e.target.name = address;
                        setAddress(e.target.value);
                      }}
                      className="text-[#969696] bg-[#ECECEC] w-60 outline-none font-light text-sm flex-1 ml-2"
                    />
                  </div>
                </div>
              </FormItem>
            </div>
            <div className="flex items-center font-light text-[13px] mt-2">
              <h6 className="mt-1">Ingin daftar menjadi Pelanggan Affitto ?</h6>
              <Link href={"registerMitra"} className="text-primary ml-1">
                <a href="/home" className="text-primary ml-1">
                  Daftar
                </a>
              </Link>
            </div>
            <div className="absolute -right-0 mr-20 ">
              <Button
                className="rounded-md absolute right-8 border-1 border-primary font-sm text-primary px-5 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Daftar
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
});

Register.getLayout = function Layout(page) {
  return <ParticlesLayout>{page}</ParticlesLayout>;
};

export default Register;
