import React, { useEffect, useState } from "react";
import PageHeader from "../../components/Items/PageHeader";
import HeaderClient from "../../components/Layout/DesktopLayout/HeaderClient";
import { Card, Layout, Breadcrumb, List } from "antd";
import axios from "axios";
import config from "../../helper/config";
import useAuthenticationPage from "../../helper/authenticationPage";

const notificationPelanggan = () => {
  const [user, setUser] = useState();
  const [notif, setNotif] = useState([]);
  const [notifCount, setNotifCount] = useState(0);

  useEffect(() => {
    loadNotif(1);
  }, []);

  const onCurrent = (page) => {
    loadNotif(page);
  };

  const loadNotif = async (page) => {
    try {
      const limit = 9;
      const res = await axios
        .get(
          `http://localhost:3222/notification/list?page=${page}&limit=${limit}`,
          config
        )
        .then((response) => {
          console.log(response.data, "RES");
          setNotif(response?.data?.items);
          setNotifCount(response?.data?.meta?.totalItems);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>Pesanan</Breadcrumb.Item>
        </Breadcrumb>
      </div>

      <div className="px-5">
        <Card className={"relative px-32 rounded-[10px] top-0"}>
          <PageHeader pageName={"Notifikasi"} />

          <div className="mt-10">
            <List
              pagination={{
                onChange: onCurrent,
                pageSize: 9,
                total: notifCount,
              }}
              dataSource={notif}
              renderItem={(item) => (
                <List.Item>
                  <List.Item.Meta
                    title={
                      <a href="https://ant.design" className="font-semibold">
                        {item.title}
                      </a>
                    }
                    description={item.message}
                  />
                  <a href={item.path} className="text-primary text-xs">
                    Lihat Selengkapnya
                  </a>
                </List.Item>
              )}
            />
          </div>
        </Card>
      </div>
    </Layout>
  );
};

export default notificationPelanggan;
