import React, { useEffect, useState } from "react";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import PageHeader from "../../components/Items/PageHeader";
import { Button, Card, List } from "antd";
import { notifficationRepository } from "../../repository/notification";
import { parseJwt } from "../../helper/parseJwt";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";
import axios from "axios";
import { Link } from "react-router-dom";

const notificationMitra = () => {
  const [user, setUser] = useState();
  const [notif, setNotif] = useState([]);
  const [notifCount, setNotifCount] = useState(0);

  useEffect(() => {
    loadNotif(1);
  }, []);

  const onCurrent = (page) => {
    loadNotif(page);
  };

  const loadNotif = async (page) => {
    try {
      const limit = 9;
      const res = await axios
        .get(
          `http://localhost:3222/notification/list?page=${page}&limit=${limit}`,
          config
        )
        .then((response) => {
          console.log(response.data, "RES");
          setNotif(response?.data?.items);
          setNotifCount(response?.data?.meta?.totalItems);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  console.log(notif, "LIST");
  return (
    <div className="mt-16">
      <Card className={"relative px-3 ml-5 mt- rounded-[10px] top-0"}>
        <PageHeader pageName={"Notifikasi"} />
        <div className="mt-10">
          <List
            pagination={{
              onChange: onCurrent,
              pageSize: 9,
              total: notifCount,
            }}
            dataSource={notif}
            renderItem={(item) => (
              <List.Item>
                <List.Item.Meta
                  title={
                    <a href="https://ant.design" className="font-semibold">
                      {item.title}
                    </a>
                  }
                  description={item.message}
                />
                <a href={item.path} className="text-primary text-xs">
                  Lihat Selengkapnya
                </a>
              </List.Item>
            )}
          />
        </div>
      </Card>
    </div>
  );
};

notificationMitra.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};
export default notificationMitra;
