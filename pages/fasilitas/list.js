import DefaultLayout from "../../components/Layout/DefaultLayout";
import {
  Space,
  Table,
  Modal,
  Input,
  Button,
  Form,
  Card,
  Breadcrumb,
  message,
} from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import PageHeader from "../../components/Items/PageHeader";
import Search from "../../components/Items/Search";
import React from "react";
import { useEffect, useState } from "react";
import { useForm } from "antd/lib/form/Form";
import axios from "axios";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";
// import { config } from "../../helper/config";

React.useLayoutEffect = React.useEffect;
const listFasilitas = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [nameFas, setName] = useState("");
  const [stateFasilitas, setFasilitas] = useState();
  const store = useStore();

  useEffect(() => {
    loadFasilitas("");
  }, []);

  const { confirm } = Modal;

  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const endpoint = `http://localhost:3222/facility/delete/${data.id}`;
          const res = await axios.delete(endpoint, config);
          setTimeout(message.success("Fasilitas Berhasil Di Hapus"), 10000);
          loadFasilitas("");
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const handleSubmit = async (e) => {
    try {
      const data = {
        name: nameFas,
      };

      const endpoint = "http://localhost:3222/facility/create";
      const res = await axios.post(endpoint, data, config);
      setModalCreate(false);
      setTimeout(message.success("Fasilitas Berhasil Dibuat"), 10000);
      loadFasilitas("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadFasilitas = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/facility?search=${val}`)
        .then((response) => {
          setFasilitas(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const onChange = (e) => {
    loadFasilitas(e.target.value);
  };

  const columns = [
    {
      title: "Fasilitas",
      dataIndex: "name",
      key: "Fasilitas",
    },
    {
      title: "Aksi",
      key: "id",
      dataIndex: "id",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg border-1 border-edit font-sm text-edit hover:bg-edit hover:text-white"
            htmlType="submit"
            onClick={() => {
              functionEdit(record);
            }}
          >
            Edit
          </Button>
          <Button
            className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
            onClick={() => {
              showDeleteConfirm(record);
            }}
          >
            Hapus
          </Button>
        </Space>
      ),
    },
  ];
  const tableProps = {
    size,
  };

  const [form] = useForm();
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);

  const showModal = () => {
    setModalCreate(true);
  };

  const handleCancel = () => {
    setModalCreate(false);
    setModalUpdate(false);
    form.resetFields();
  };

  const showModalUpdate = () => {
    setModalUpdate(true);
  };

  const functionEdit = (val) => {
    form.setFieldsValue({
      id: val.id,
      fasilitas: val.name,
    });
    showModalUpdate();
  };

  const onFinishUpdate = async (e) => {
    try {
      const dataUp = form.getFieldValue();
      const data = {
        name: nameFas,
      };
      const endpoint = `http://localhost:3222/facility/update/${dataUp.id}`;

      const res = await axios.put(endpoint, data, config);
      setModalUpdate(false);
      setTimeout(message.success("Fasilitas Berhasil Diubah"), 10000);
      loadFasilitas("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Fasilitas</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Fasilitas"} />
        <div className="absolute top-7 right-8">
          <Button
            className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
            onClick={showModal}
          >
            + Tambah
          </Button>
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateFasilitas}
        />

        {/* modal Create */}
        <Modal
          name="form-create"
          visible={modalCreate}
          onCancel={handleCancel}
          title="Tambah Fasilitas"
          footer={false}
        >
          <Form form={form} layout="vertical" onFinish={handleSubmit}>
            <Form.Item name="fasilitas" label="Nama Fasilitas">
              <Input
                placeholder="Masukkan Fasilitas"
                name="name"
                onChange={(e) => {
                  e.target.name = nameFas;
                  setName(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Tambah
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* modal Update */}
        <Modal
          name="form-update"
          visible={modalUpdate}
          onCancel={handleCancel}
          title="Edit Fasilitas"
          footer={false}
        >
          <Form form={form} layout="vertical" onFinish={onFinishUpdate}>
            <Form.Item name="id" hidden={true}>
              <Input readOnly={true} />
            </Form.Item>
            <Form.Item name="fasilitas" label="Nama Fasilitas">
              <Input
                placeholder="Masukkan nama Fasilitas"
                name="name"
                onChange={(e) => {
                  e.target.name = nameFas;
                  setName(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Simpan
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    </div>
  );
});

listFasilitas.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listFasilitas;
