import React from "react";
import Link from "next/dist/client/link";
import Image from "next/dist/client/image";
import Logo from "../public/logonavbar.svg";
import Hero from "../public/Hero.svg";
import CardKategori from "../components/Items/landingpage/CardKategori";
import { Typography } from "antd";

const LandingPage = (props) => {
  return (
    <>
      <div clasNameName="relative flex flex-col">
        <div className="flex items-center justify-center">
          <div class="w-full min-h-screen font-sans text-gray-900">
            {/* HEADER */}
            <div class="bg-gradient-to-b from-[#DFF6FF] to-white">
              <div class="px-6 mx-auto max-w-6xl">
                <div class="flex justify-between items-center py-5">
                  <Image src={Logo} alt="Logo" width={130} />
                  <nav className="hidden md:flex md:items-center md:space-x-10">
                    <span className="font-semibold cursor-pointer  text-primary">
                      Beranda
                    </span>
                    <Link href={"/auth/register-pelanggan"}>
                      <span className="font-semibold cursor-pointer text-gray-700 hover:text-primary">
                        Register
                      </span>
                    </Link>
                    <Link href={"/auth/login"}>
                      <span className="font-semibold cursor-pointer text-gray-700 hover:text-primary">
                        Login
                      </span>
                    </Link>
                  </nav>
                </div>
              </div>
              <div class="px-6 mx-auto max-w-6xl">
                <div class="flex flex-col-reverse items-center md:flex-row lg:items-end">
                  <div class="pt-9 pr-8 pb-24 text-center md:pb-12 md:w-1/2 md:text-left">
                    <h1 class="relative z-10 text-5xl font-bold md:text-6xl font-display">
                      Sewa Berbagai Tempat Menarik di{" "}
                      <a className="text-primary">affitto</a>
                    </h1>
                    <p class="pt-8 text-lg leading-relaxed text-gray-500 md:max-w-md md:text-xl">
                      Temukan tempat berdasarkan kategori dan Lokasi dengan
                      harga terbaik sesuai dengan kebutuhan anda.
                    </p>
                    <div class="flex justify-center items-center pt-8 space-x-6 md:justify-start">
                      <Link href={"/dashboard/client"}>
                        <button class="flex justify-center items-center py-3 px-5 font-bold tracking-wide leading-7 text-white bg-blue-600 rounded-xl hover:shadow-xl">
                          Mulai Sekarang
                        </button>
                      </Link>
                    </div>
                  </div>
                  <div class="flex mb-36 ml-10 border-b border-gray-400">
                    <Image src={Hero} alt="Hero" width={500} />
                  </div>
                </div>
              </div>
            </div>

            {/* KATEGORI */}
            <div class="">
              <h4 className="flex justify-center mb-5 mt-3 items-center tracking-[0.2em] text-gray-500 font-semibold text-sm">
                JELAJAHI
              </h4>
              <h1 className="flex justify-center items-center tracking-[1em] font-extrabold text-xl">
                KATEGORI
              </h1>
              <div class="py-12 px-6 mx-auto max-w-6xl">
                <div class="flex flex-col md:flex-row gap-5 mb-20">
                  <div className="w-1/4 h-12 rounded-xl">
                    <CardKategori
                      data={{
                        image: "/meeting.svg",
                        title: "Conference Hall",
                        desc: "Ruang serbaguna dengan kapasitas mencapai ratusan/ribuan orang. ",
                      }}
                    >
                      {/* <div class="relative">
                        <Image
                          src={"/banquet.svg"}
                          alt="banquet"
                          width={40}
                          height={40}
                        />
                      </div> */}
                    </CardKategori>
                  </div>
                  <div className="w-1/4 h-12">
                    <CardKategori
                      data={{
                        image: "/gradu.svg",
                        title: "Function Hall",
                        desc: "Ruang serbaguna untuk rapat dengan kapasitas yang lebih kecil.",
                      }}
                    />
                  </div>
                  <div className="w-1/4 h-12">
                    <CardKategori
                      data={{
                        image: "/wedding.svg",
                        title: "Banquet Hall",
                        desc: "Ruang serbaguna sebagai area jamuan atau upacara pernikahan.",
                      }}
                    />
                  </div>
                  <div className="w-1/4 h-12">
                    <CardKategori
                      data={{
                        image: "/outdoor.svg",
                        title: "Outdoor",
                        desc: "Ruangan terbuka untuk mengadakan berbagai acara.",
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>

            {/* LANGKAH PESAN */}
            <div className="px-14 mt-48">
              <div className="max-w-7xl mx-auto my-28 relative px-4">
                <div className="flex">
                  <div className="w-6/12 pl-8 pr-20">
                    <div className="flex flex-col mb-16">
                      <div className="mb-6">
                        <h3 className="text-base mb-2 text-gray-500">
                          LANGKAH PEMESANAN
                        </h3>
                        <h3 className="text-4xl mb-2 font-extrabold text-gray-900">
                          Lakukan Pemesanan Dengan 3 Langkah
                        </h3>
                      </div>
                      <div className="">
                        <ul className="flex flex-col gap-y-10 pr-10 mb-5">
                          <li className="flex items-center">
                            <Image
                              src="/step1.svg"
                              alt="step 1"
                              width={60}
                              height={60}
                            />
                            <div className="flex flex-col ml-4 leading-5">
                              <h6 className="font-bold mb-1 text-gray-600">
                                Pilih Tempat
                              </h6>
                              <p className="font-[400] mb-1 text-gray-600">
                                Cari tempat dengan berbagai macam kategori dan
                                fasilitas sesuai keinginan anda.
                              </p>
                            </div>
                          </li>
                        </ul>
                        <ul className="flex flex-col gap-y-10 pr-10 mb-5">
                          <li className="flex items-center">
                            <Image
                              src="/step2.svg"
                              alt="step 2"
                              width={60}
                              height={60}
                            />
                            <div className="flex flex-col ml-4 leading-5">
                              <h6 className="font-bold mb-1 text-gray-600">
                                Lakukan Pembayaran
                              </h6>
                              <p className="font-[400] mb-1 text-gray-600">
                                Lakukan pembayaran agar pesanan anda bisa segera
                                dikonfirmasi oleh Mitra.
                              </p>
                            </div>
                          </li>
                        </ul>
                        <ul className="flex flex-col gap-y-10 pr-10 ">
                          <li className="flex items-center">
                            <Image
                              src="/step3.svg"
                              alt="step 3"
                              width={60}
                              height={60}
                            />
                            <div className="flex flex-col ml-4 leading-5">
                              <h6 className="font-bold mb-1 text-gray-600">
                                Datang sesuai Tanggal
                              </h6>
                              <p className="font-[400] mb-1 text-gray-600">
                                Anda dapat menggunakan tempat yang dipesan
                                sesuai dengan tanggal yang sudah ditentukan.
                              </p>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="w-6/12 flex items-center justify-center">
                    <div className="w-[400px] relative">
                      <div className="w-60 h-60 rounded-full blur-3xl absolute z-10 -top-40 -right-10 bg-[#DFF6FF]" />
                      <div className="absolute z-20 -top-56 -right-10">
                        <Image
                          src={"/Hero2.svg"}
                          width={800}
                          height={800}
                          alt="Hero 2"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* FOOTER */}
            <div className="bg-[#06283D] h-64">
              <div className="flex justify-center">
                <Image src={Logo} alt="Logo" width={130} />
              </div>
              <div>
                {" "}
                <Typography className="flex justify-center text-white text-xs mt-3">
                  Hubungi Kami :
                </Typography>
              </div>
              <div className="flex flex-row justify-center gap-3 mt-2">
                <div>
                  <a href="#">
                    <Image
                      src={"/youtube.svg"}
                      alt="youtube"
                      width={40}
                      height={40}
                    />
                  </a>
                </div>
                <div>
                  <a href="#">
                    <Image
                      src={"/instagram.svg"}
                      alt="instagram"
                      width={40}
                      height={40}
                    />
                  </a>
                </div>
                <div>
                  <a href="#">
                    <Image
                      src={"/twitter.svg"}
                      alt="twitter"
                      width={40}
                      height={40}
                    />
                  </a>
                </div>
                <div>
                  <a href="#">
                    <Image
                      src={"/facebook.svg"}
                      alt="facebook"
                      width={40}
                      height={40}
                    />
                  </a>
                </div>
              </div>
              <div>
                <Typography className="flex justify-center text-white text-xs mt-10 ">
                  All rights reserved @affitto
                </Typography>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LandingPage;
