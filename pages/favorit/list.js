import React, { useState, useEffect } from "react";
import { Card, Breadcrumb, Layout } from "antd";
import PageHeader from "../../components/Items/PageHeader";
import HeaderClient from "../../components/Layout/DesktopLayout/HeaderClient";
import SwiperVenue from "../../components/Items/SwiperVenue";
import "swiper/css";
import "swiper/css/pagination";
import axios from "axios";
import { appConfig } from "../../config/app";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";

const listFavorit = (props) => {
  const [current, setCurrent] = useState(1);
  const [totalItems, setTotal] = useState();
  const [venue, setVenue] = useState([]);
  useEffect(() => {
    loadTempat();
  }, []);

  const loadTempat = async () => {
    try {
      await axios
        .get(`http://localhost:3222/myfavorit`, config)
        .then((response) => {
          setCurrent(response.data.meta.currentPage);
          setTotal(response.data.meta.totalItems);
          setVenue(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const key = venue.map((dataOld) => {
    const dataAll = {
      image: dataOld?.venue?.venue_photo.map((v) => {
        return v.photo;
      }),
      nama: dataOld?.venue?.name,
      rate: "Text",
      favorit: "Text",
      harga: dataOld?.venue?.price,
      kategori: dataOld?.venue?.category?.name,
      id: dataOld?.venue?.id,
      kota: dataOld?.venue?.district?.city?.name,
    };
    return dataAll;
  });

  const onCurrent = (page) => {
    loadTempat(page);
  };

  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>Favorit</Breadcrumb.Item>
        </Breadcrumb>

        <div className="px-5 py-3">
          <Card className={"relative px-1 rounded-[10px] top-0"}>
            <PageHeader pageName={"Favorit Saya"} />
            <SwiperVenue
              data={key}
              onCurrent={onCurrent}
              totalItems={totalItems}
            />
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default listFavorit;
