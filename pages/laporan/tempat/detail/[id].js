import React, { useState, useEffect } from "react";
import { Button, Form, Card, Breadcrumb, Typography, message } from "antd";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import PageHeader from "../../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../../config/app";
import { FaMapMarkerAlt } from "react-icons/fa";
import config from "../../../../helper/config";
import useAuthenticationPage from "../../../../helper/authenticationPage";

const detailLaporanTempat = observer(() => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [report, setReport] = useState();

  useEffect(() => {
    loadReport();
  }, [id]);

  const loadReport = async () => {
    try {
      await axios
        .get(`http://localhost:3222/report-venue/detail/${id}`, config)
        .then((response) => {
          setReport(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const confirm = async () => {
    try {
      const endpoint = `http://localhost:3222/report-venue/confirm/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Dikonfirmasi"), 10000);
      router.push("/laporan/tempat/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const reject = async () => {
    try {
      const endpoint = `http://localhost:3222/report-venue/reject/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Ditolak"), 10000);
      router.push("/laporan/tempat/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const photo = report?.order?.venue?.venue_photo;
  const foto = () => {
    try {
      return photo[0].photo;
    } catch (e) {}
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>
          <a href="/laporan/tempat/list">List Laporan</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Laporan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Detail Laporan Tempat"} />

        <div class="grid gap-4 grid-cols-2">
          <div>
            <Card className="shadow-md rounded-md">
              <div className="flex felx-row mb-5">
                <img
                  src={
                    appConfig.apiUrl +
                    "/upload/get-user/" +
                    report?.order?.user?.photo
                  }
                  alt="profile user"
                  width={50}
                  height={50}
                  className="rounded-full"
                />
                <div className="ml-4">
                  <Typography className="font-medium text-sm">
                    {report?.order?.user?.name}
                  </Typography>
                  <Typography className="font-medium text-xs text-[#999999]">
                    @{report?.order?.user?.username}
                  </Typography>
                </div>
              </div>
              <Form layout="vertical" form={form}>
                <FormItem>
                  <Typography.Title className="font-semibold text-sm ml-5">
                    ID Pemesanan :
                  </Typography.Title>
                  <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                    <Typography className="font-light text-sm ml-3">
                      {report?.order?.order_code}
                    </Typography>
                  </div>
                </FormItem>

                <FormItem>
                  <Typography.Title className="font-semibold text-sm ml-5">
                    Deskripsi :
                  </Typography.Title>
                  <div className="flex items-center bg-abu border w-96 h-16 rounded-lg ml-3 ">
                    <Typography className="font-light text-sm ml-3 px-1">
                      {report?.description}
                    </Typography>
                  </div>
                </FormItem>

                <FormItem>
                  <Typography.Title className="font-semibold text-sm ml-5">
                    Foto :
                  </Typography.Title>
                  <div className="ml-3">
                    <img
                      src={
                        appConfig.apiUrl + "/upload/get-report/" + report?.photo
                      }
                      alt="fotoVenue"
                      className="w-5/6 rounded-lg"
                    />
                  </div>
                </FormItem>
              </Form>
            </Card>
          </div>
          <div className="relative">
            <Card className="shadow-md rounded-md">
              <Typography className="text-lg font-semibold text-primary">
                {report?.order?.venue?.name}
              </Typography>
              <div className="flex mb-1">
                <FaMapMarkerAlt className="text-delete mt-[4px] mr-2" />
                {report?.order?.venue?.district?.city?.name}
              </div>
              <img
                src={appConfig.apiUrl + "/upload/get-venue/" + foto()}
                alt="fotoVenue"
                className="w-fit rounded-lg"
              />
            </Card>
          </div>
        </div>
        <div className="flex items-end justify-end">
          <Button
            onClick={confirm}
            className="border-1 border-konfirmasi font-sm text-konfirmasi rounded-lg px-3 mr-3 py-1 inline-block hover:bg-konfirmasi hover:text-white"
          >
            Konfirmasi
          </Button>
          <Button
            onClick={reject}
            className="border-1 border-delete font-sm text-delete rounded-lg px-3 py-1 inline-block hover:bg-delete hover:text-white"
          >
            Tolak
          </Button>
        </div>
      </Card>
    </div>
  );
});

detailLaporanTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailLaporanTempat;
