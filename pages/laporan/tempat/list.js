import React, { useState, useEffect } from "react";
import { Space, Table, Button, message } from "antd";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import PageHeader from "../../../components/Items/PageHeader";
import axios from "axios";
import config from "../../../helper/config";
import useAuthenticationPage from "../../../helper/authenticationPage";

const listLaporanTempat = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [stateLaporanTempat, setLaporanTempat] = useState();
  const store = useStore();

  useEffect(() => {
    loadLaporanTempat("");
  }, []);

  const confirm = async (id) => {
    try {
      const endpoint = `http://localhost:3222/report-venue/confirm/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Dikonfirmasi"), 10000);
      loadLaporanTempat("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const reject = async (id) => {
    try {
      const endpoint = `http://localhost:3222/report-venue/reject/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Ditolak"), 10000);
      loadLaporanTempat("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadLaporanTempat = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/report-venue/list?search=${val}`, config)
        .then((response) => {
          setLaporanTempat(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadLaporanTempat(e.target.value);
  };

  const columns = [
    {
      title: "Nama Tempat",
      dataIndex: "venue",
      key: "venue",
      render: (text, record) => (
        <Link href={`/laporan/tempat/detail/${record.id}`}>
          <span className="text-[#1363DF]">{text?.name}</span>
        </Link>
      ),
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
      render: (text, record) => (
        <Link href={`/laporan/tempat/detail/${record.id}`}>
          <span className="text-[#1363DF]">{text}</span>
        </Link>
      ),
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg border-1 border-[#5ab036] font-sm text-[#5ab036] hover:bg-[#5ab036] hover:text-white"
            htmlType="submit"
            onClick={() => {
              confirm(record.id);
            }}
          >
            Konfirmasi
          </Button>
          <Button
            className="rounded-lg border-1 border-delete font-sm text-delete hover:bg-delete hover:text-white"
            htmlType="submit"
            onClick={() => {
              reject(record.id);
            }}
          >
            Tolak
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Laporan Tempat</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Laporan Tempat"} />
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateLaporanTempat}
        />
      </Card>
    </div>
  );
});

listLaporanTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listLaporanTempat;
