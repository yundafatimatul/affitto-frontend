import React, { useState, useEffect } from "react";
import { Space, Table, Button, message } from "antd";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import PageHeader from "../../../components/Items/PageHeader";
import axios from "axios";
import config from "../../../helper/config";
import useAuthenticationPage from "../../../helper/authenticationPage";

const listLaporanUlasan = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();

  useEffect(() => {
    loadLaporanUlasan("");
  }, []);

  const [stateLaporanUlasan, setLaporanUlasan] = useState();

  const confirm = async (id) => {
    try {
      const endpoint = `http://localhost:3222/report-review/confirm/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Dikonfirmasi"), 10000);
      loadLaporanUlasan("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const reject = async (id) => {
    try {
      const endpoint = `http://localhost:3222/report-review/reject/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Ditolak"), 10000);
      loadLaporanUlasan("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadLaporanUlasan = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/report-review/list?search=${val}`, config)
        .then((response) => {
          setLaporanUlasan(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const onChange = (e) => {
    loadLaporanUlasan(e.target.value);
  };

  const columns = [
    {
      title: "Foto",
      dataIndex: "user",
      key: "foto",
      render: (text) => (
        <div>
          <img
            src={
              process.env.NEXT_PUBLIC_BASE_URL +
              `/upload/get-user/${text?.photo}`
            }
            alt="pictureList"
            className="rounded-full h-12 w-12"
          />
        </div>
      ),
    },
    {
      title: "Nama Pelanggan",
      dataIndex: "user",
      key: "user",
      render: (text, record) => (
        <Link href={`/laporan/ulasan/detail/${record.id}`}>
          <a className="text-[#1363DF]">{text?.name}</a>
        </Link>
      ),
    },
    {
      title: "Nama Tempat",
      dataIndex: "review",
      key: "review",
      render: (text) => (
        <Link href={`/lokasi/tempat/${text.venue.id}`}>
          <span className="text-[#1363DF]">{text?.venue?.name}</span>
        </Link>
      ),
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg border-1 border-[#5ab036] font-sm text-[#5ab036] hover:bg-[#5ab036] hover:text-white"
            htmlType="submit"
            onClick={() => {
              confirm(record.id);
            }}
          >
            Konfirmasi
          </Button>
          <Button
            className="rounded-lg border-1 border-delete font-sm text-delete hover:bg-delete hover:text-white"
            htmlType="submit"
            onClick={() => {
              reject(record.id);
            }}
          >
            Tolak
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Laporan Ulasan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Laporan Ulasan"} />
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateLaporanUlasan}
        />
      </Card>
    </div>
  );
});

listLaporanUlasan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listLaporanUlasan;
