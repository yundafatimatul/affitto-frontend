import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  Card,
  Breadcrumb,
  Upload,
  Typography,
  message,
} from "antd";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import PageHeader from "../../../../components/Items/PageHeader";
import { Rate } from "antd";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../../config/app";
import config from "../../../../helper/config";
import useAuthenticationPage from "../../../../helper/authenticationPage";

const detailLaporanUlasan = observer(() => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [report, setReport] = useState();

  useEffect(() => {
    loadReport();
  }, [id]);

  const loadReport = async () => {
    try {
      await axios
        .get(`http://localhost:3222/report-review/detail/${id}`, config)
        .then((response) => {
          setReport(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const confirm = async () => {
    try {
      const endpoint = `http://localhost:3222/report-review/confirm/${id}`;
      const res = await axios.put(endpoint, "", config);
      message.success("Laporan Dikonfirmasi", 1);
      router.push("/laporan/ulasan/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const reject = async () => {
    try {
      const endpoint = `http://localhost:3222/report-review/reject/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Laporan Ditolak"), 10000);
      router.push("/laporan/ulasan/list");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>
          <a href="/laporan/ulasan/list">List Laporan Ulasan</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Laporan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Detail Laporan Ulasan"} />

        <div class="grid gap-4 grid-cols-2">
          <div>
            <Card className="shadow-md rounded-md">
              <div className="flex felx-row mb-5">
                <img
                  src={
                    appConfig.apiUrl + "/upload/get-user/" + report?.user?.photo
                  }
                  alt="profile user"
                  width={50}
                  height={50}
                  className="rounded-full"
                />
                <div className="ml-4">
                  <Typography className="font-medium text-sm">
                    {report?.user?.name}
                  </Typography>
                  <Typography className="font-medium text-xs text-[#999999]">
                    @{report?.user?.username}
                  </Typography>
                </div>
              </div>
              <Form layout="vertical" form={form}>
                <FormItem>
                  <Typography.Title className="font-semibold text-sm ml-5">
                    ID Pemesanan :
                  </Typography.Title>
                  <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                    <Typography className="font-light text-sm ml-3">
                      {report?.review?.order?.order_code}
                    </Typography>
                  </div>
                </FormItem>

                <FormItem>
                  <Typography.Title className="font-semibold text-sm ml-5">
                    Deskripsi :
                  </Typography.Title>
                  <div className="flex items-center bg-abu border w-96 h-16 rounded-lg ml-3 ">
                    <Typography className="font-light text-sm ml-3">
                      {report?.description}
                    </Typography>
                  </div>
                </FormItem>
              </Form>
            </Card>
          </div>
          <div className="relative">
            <Card className="shadow-md rounded-md">
              <div className="flex felx-row mb-2">
                <img
                  src={
                    appConfig.apiUrl +
                    "/upload/get-user/" +
                    report?.review?.user?.photo
                  }
                  alt="profile user"
                  width={50}
                  height={50}
                  className="rounded-full"
                />
                <div className="ml-4">
                  <Typography className="font-medium text-sm">
                    {report?.review?.user?.name}
                  </Typography>
                  <Typography className="font-medium text-xs text-[#999999]">
                    @{report?.review?.user?.username}
                  </Typography>
                </div>
              </div>
              <Rate disabled value={report?.review?.rate} />
              <Typography className="font-light text-xs mb-3">
                {report?.review?.comment}
              </Typography>
              <img
                src={
                  appConfig.apiUrl +
                  "/upload/get-review/" +
                  report?.review?.photo
                }
                alt="review-foto"
                className="rounded-lg w-56"
              />
            </Card>
          </div>
        </div>
        <div className="flex items-end justify-end mt-8">
          <Button
            onClick={confirm}
            className="border-1 border-konfirmasi font-sm text-konfirmasi rounded-lg px-3 mr-3 py-1 inline-block hover:bg-konfirmasi hover:text-white"
          >
            Konfirmasi
          </Button>
          <Button
            onClick={reject}
            className="border-1 border-delete font-sm text-delete rounded-lg px-3 py-1 inline-block hover:bg-delete hover:text-white"
          >
            Tolak
          </Button>
        </div>
      </Card>
    </div>
  );
});

detailLaporanUlasan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailLaporanUlasan;
