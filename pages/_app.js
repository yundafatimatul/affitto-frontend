import "../styles/globals.css";
import "tailwindcss/tailwind.css";
import { StoreProvider } from "../components/StoreProvider";
import { TokenUtil } from "../utils/token";
import { getCentrifugeInstance } from "../src/services";
import { useEffect, useState } from "react";
import { parseJwt } from "../helper/parseJwt";
import { notification } from "antd";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }) {
  const [idUser, setIdUser] = useState(null);
  const router = useRouter();

  useEffect(() => {
    let token;
    let userId;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    if (token) {
      const user = parseJwt(token);
      console.log(user?.existUser?.id, "userId in App");
      userId = user?.existUser?.id;
      setIdUser(userId);
    }

    if (token && userId) {
      init(userId);
    }
  }, []);

  const init = (userId) => {
    const instance = getCentrifugeInstance();
    // console.log(userId, "ini dari app dalam init");
    console.log(`notification_${userId}`, "id user for notif");
    // console.log(`notification_${TokenUtil.roleId}`, "id role user for notif");

    instance.subscribe(`notification_${userId}`, function (ctx) {
      console.log(ctx);
      console.log(
        "response send notif client",
        ctx?.data?.type == "Disabled Status"
      );

      if (
        ctx?.data?.type == "Disabled Status" ||
        ctx?.data?.type === "Blocked Account"
      ) {
        TokenUtil.clearAccessToken();
        TokenUtil.clearRefreshToken();
        router.push("/login");
      }

      openNotification("topRight", ctx);
    });
  };

  const openNotification = (placement, data) => {
    const key = "buka";
    notification.info({
      message: data?.data?.title,
      description: data?.data?.message,
      placement,
      maxCount: 1,
      duration: 5,
      key,
      onClick() {
        console.log(data?.data);
        router.push(data?.data?.path_detail);
        notification.close(key);
      },
    });
    // } else {
    //   if (data?.data?.type !== "Request Update Profile") {
    //     notification.info({
    //       message: data?.data?.type,
    //       description: data?.data?.message,
    //       placement,
    //       maxCount: 1,
    //       duration: 5,
    //       key,
    //       icon: <Avatar src={NotificationIcon(data?.data?.type)} />,
    //       onClick() {
    //         router.push(data?.data?.path_detail);
    //         notification.close(key);
    //       },
    //     });
    // }
  };

  // Use the layout defined at the page level, if available
  const getLayout = Component.getLayout || ((page) => page);

  return getLayout(
    <StoreProvider {...pageProps}>
      <Component {...pageProps} />
    </StoreProvider>
  );
}

export default MyApp;
