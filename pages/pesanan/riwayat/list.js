import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb, Table, Tag } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import PageHeader from "../../../components/Items/PageHeader";
import React, { useState, useEffect } from "react";
import axios from "axios";
import moment from "moment";
import useAuthenticationPage from "../../../helper/authenticationPage";
import config from "../../../helper/config";

const listRiwayatPesanan = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();
  const [order, setOrder] = useState();

  useEffect(() => {
    loadORder("");
  }, []);

  const loadORder = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/orders/ownerOrder?search=${val}`, config)
        .then((response) => {
          setOrder(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const onChange = (e) => {
    loadORder(e.target.value);
  };
  const date = (a) => {
    const date = new Date(a);
    const c = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return c;
  };
  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "done") {
      array[0] = "cyan";
      array[1] = "Selesai";
    } else if (stats === "approved_by_owner") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "canceled_by_client") {
      array[0] = "gold";
      array[1] = "Pengajuan Pembatalan";
    } else if (stats === "approved_cancel_by_owner") {
      array[0] = "volcano";
      array[1] = "Dibatalkan Pelanggan";
    } else if (stats === "rejected") {
      array[0] = "magenta";
      array[1] = "Ditolak";
    } else if (stats === "rejected_by_admin") {
      array[0] = "red";
      array[1] = "Dibatalkan Admin";
    }
    return array;
  };

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "order_code",
      key: "order_code",
      render: (text, record) => (
        <Link href={`/pesanan/detail/owner/${record.id}`}>
          <a className="text-primary">{text}</a>
        </Link>
      ),
    },
    {
      title: "Nama Tempat",
      dataIndex: "venue",
      key: "venue",
      render: (text) => <p>{text?.name}</p>,
    },
    {
      title: "Tanggal Pesanan",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
      render: (text) => <p>{date(text)}</p>,
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      filters: [
        {
          text: <span>Menunggu Dikonfirmasi</span>,
          value: "pending",
        },
        {
          text: <span>Dikonfirmasi</span>,
          value: "approved_by_owner",
        },
        {
          text: <span>Pengajuan Pembatalan</span>,
          value: "canceled_by_client",
        },
        {
          text: <span>Dibatalkan</span>,
          value: "approved_cancel_by_owner",
        },
        {
          text: <span>Ditolak</span>,
          value: "rejected",
        },
        {
          text: <span>Selesai</span>,
          value: "done",
        },
        {
          text: <span>Dibatalkan Admin</span>,
          value: "rejected_by_admin",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: true,
      width: "20%",
      render: (_, { status }) => (
        <Tag color={color(status)[0]} key={status}>
          {color(status)[1]}
        </Tag>
      ),
    },
  ];

  const tableProps = { size };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>Pesanan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Riwayat Pesanan"} />
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={order}
        />
      </Card>
    </div>
  );
});

listRiwayatPesanan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listRiwayatPesanan;
