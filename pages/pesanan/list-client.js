import React, { useState, useEffect } from "react";
import { Card, Breadcrumb, Table, Tag, Layout } from "antd";
import PageHeader from "../../components/Items/PageHeader";
import Search from "../../components/Items/Search";
import Link from "next/link";
import HeaderClient from "../../components/Layout/DesktopLayout/HeaderClient";
import axios from "axios";
import useAuthenticationPage from "../../helper/authenticationPage";
import { TokenUtil } from "../../utils/token";
import moment from "moment";
const listPesananClient = () => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [s, setS] = useState("");
  useEffect(() => {
    loadLogPesanan();
  }, []);

  // useAuthenticationPage();

  const [stateLogPesanan, setLogPesanan] = useState();
  const loadLogPesanan = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/user?search=${s}`, config)
        .then((response) => {
          console.log(response.data);
          setLogPesanan(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const func = (s) => {
    console.log(s.status);
    if (s.status == "paid") {
      return `/pesanan/detail/client/paid/${s?.order?.id}`;
    } else if (s.status == "pending" || "rejedted_by_admin") {
      return `/pesanan/detail/client/pending/${s?.order?.id}`;
    }
  };

  const onChange = (e) => {
    setS(e.target.value);
    loadLogPesanan();
  };

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "done") {
      array[0] = "cyan";
      array[1] = "Selesai";
    } else if (stats === "approved_by_owner") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "canceled_by_client") {
      array[0] = "gold";
      array[1] = "Pengajuan Pembatalan";
    } else if (stats === "approved_cancel_by_owner") {
      array[0] = "volcano";
      array[1] = "Dibatalkan Pelanggan";
    } else if (stats === "rejected") {
      array[0] = "magenta";
      array[1] = "Ditolak";
    } else if (stats === "rejected_by_admin") {
      array[0] = "red";
      array[1] = "Dibatalkan Admin";
    }
    return array;
  };

  const colorPay = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dibayar";
    } else if (stats === "paid") {
      array[0] = "green";
      array[1] = "Dibayar";
    } else if (stats === "rejected_by_admin") {
      array[0] = "volcano";
      array[1] = "Tidak Dibayar";
    }
    return array;
  };
  const date = (a) => {
    const date = new Date(a);
    const c = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return c;
  };

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "order",
      key: "order",
      render: (text, record) => (
        <div className="">
          <Link href={func(record)}>
            <span className="text-primary mt-2">{text?.order_code}</span>
          </Link>
        </div>
      ),
    },
    {
      title: "Nama Tempat",
      dataIndex: "order",
      key: "venue",
      render: (text) => <span className="mt-2">{text?.venue?.name}</span>,
    },
    {
      title: "Tanggal Pesanan",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
      render: (text) => <span className="mt-2">{date(text)}</span>,
    },
    {
      title: "Status Pesanan",
      key: "order",
      dataIndex: "order",
      filters: [
        {
          text: <span>Menunggu Dikonfirmasi</span>,
          value: "pending",
        },
        {
          text: <span>Dikonfirmasi</span>,
          value: "approved_by_owner",
        },
        {
          text: <span>Pengajuan Pembatalan</span>,
          value: "canceled_by_client",
        },
        {
          text: <span>Dibatalkan</span>,
          value: "approved_cancel_by_owner",
        },
        {
          text: <span>Ditolak</span>,
          value: "rejected",
        },
        {
          text: <span>Selesai</span>,
          value: "done",
        },
        {
          text: <span>Dibatalkan Admin</span>,
          value: "rejected_by_admin",
        },
      ],
      onFilter: (value, record) => record?.order?.status.startsWith(value),
      filterSearch: true,
      width: "20%",
      render: (_, record) => (
        <Tag
          color={color(record?.order?.status)[0]}
          key={record?.order?.status}
        >
          {color(record?.order?.status)[1]}
        </Tag>
      ),
    },
    {
      title: "Status Pembayaran",
      key: "status",
      dataIndex: "status",
      filters: [
        {
          text: <span>Menunggu Dibayar</span>,
          value: "pending",
        },
        {
          text: <span>Dibayar</span>,
          value: "paid",
        },
        {
          text: <span>Ditolak Admin</span>,
          value: "rejected_by_admin",
        },
      ],
      onFilter: (value, record) => record?.status.startsWith(value),
      filterSearch: true,
      width: "20%",
      render: (_, { status }) => (
        <Tag color={colorPay(status)[0]} key={status}>
          {colorPay(status)[1]}
        </Tag>
      ),
    },
  ];

  const tableProps = { size };
  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>Pesanan</Breadcrumb.Item>
        </Breadcrumb>

        <div className="px-5">
          <Card className={"relative px-10 rounded-[10px] top-0"}>
            <PageHeader pageName={"Pesanan"} />
            <Search onChange={onChange} />
            <Table
              {...tableProps}
              pagination={{
                position: [center, bottom],
              }}
              columns={columns}
              dataSource={stateLogPesanan}
            />
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default listPesananClient;
