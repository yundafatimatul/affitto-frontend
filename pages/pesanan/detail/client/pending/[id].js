import React, { useState, useEffect } from "react";
import {
  Card,
  Breadcrumb,
  Tag,
  Layout,
  Form,
  Typography,
  Button,
  Upload,
  message,
  List,
} from "antd";
import PageHeader from "../../../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import HeaderClient from "../../../../../components/Layout/DesktopLayout/HeaderClient";
import { useRouter } from "next/router";
import axios from "axios";
import { imageRepository } from "../../../../../repository/image";
import { appConfig } from "../../../../../config/app";
import { MdWarning } from "react-icons/md";
import useAuthenticationPage from "../../../../../helper/authenticationPage";
import config from "../../../../../helper/config";

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";

  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }

  const isLt2M = file.size / 1024 / 1024 < 2;

  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }

  return isJpgOrPng && isLt2M;
};

const detailPesananClientUpload = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState();
  const [pesanan, setPesanan] = useState([]);
  const [rekening, setRek] = useState([]);
  const router = useRouter();
  const [fileList, setFileList] = useState();
  const { id } = router.query;

  useEffect(() => {
    loadLogPesanan();
    loadRekening();
  }, [id]);

  const loadLogPesanan = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/detail/${id}`, config)
        .then((response) => {
          setPesanan(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const handleUpload = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const processUpload = await imageRepository.manipulateData.upPayment(
        file
      );
      console.log("ini PROCESS", processUpload);
      setFileList(processUpload.body.data.filename);
      setImageUrl(
        appConfig.apiUrl +
          "/upload/get-payment/" +
          processUpload.body.data.filename
      );
      setTimeout(message.success("Gambar Berhasil Di Unggah!"), 10000);
    } catch (e) {
      setTimeout(message.error("Gambar Gagal Di Unggah"), 10000);
    }
  };

  const onFinish = async () => {
    const data = { photo: fileList };
    if (imageUrl) {
      try {
        const res = await axios.put(
          `http://localhost:3222/payment/paid/${pesanan.id}`,
          data,
          config
        );
        setTimeout(message.success("Data Berhasil Diperbarui!"), 10000);
        router.push(`/pesanan/detail/client/paid/${id}`);
      } catch (e) {
        setTimeout(message.error(e.response.data.error), 10000);
      }
    } else {
      setTimeout(message.error("Mohon Unggah Bukti Pembayaran"), 10000);
    }
  };

  const s_date = `${new Date(pesanan?.order?.start_date).getDate()}-${new Date(
    pesanan?.order?.start_date
  ).getMonth()}-${new Date(pesanan?.order?.start_date).getFullYear()}`;

  const e_date = `${new Date(pesanan?.order?.end_date).getDate()}-${new Date(
    pesanan?.order?.end_date
  ).getMonth()}-${new Date(pesanan?.order?.end_date).getFullYear()}`;

  const s_hour = `${new Date(pesanan?.order?.start_date).getHours()}:${new Date(
    pesanan?.order?.start_date
  ).getMinutes()}`;

  const e_hour = `${new Date(pesanan?.order?.end_date).getHours()}:${new Date(
    pesanan?.order?.end_date
  ).getMinutes()}`;

  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "done") {
      array[0] = "cyan";
      array[1] = "Selesai";
    } else if (stats === "approved_by_owner") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "canceled_by_client") {
      array[0] = "gold";
      array[1] = "Pengajuan Pembatalan";
    } else if (stats === "approved_cancel_by_owner") {
      array[0] = "volcano";
      array[1] = "Dibatalkan Pelanggan";
    } else if (stats === "rejected") {
      array[0] = "magenta";
      array[1] = "Ditolak";
    } else if (stats === "rejected_by_admin") {
      array[0] = "red";
      array[1] = "Dibatalkan Admin";
    }
    return array;
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );

  const loadRekening = async () => {
    try {
      await axios
        .get(`http://localhost:3222/bank/modal`, config)
        .then((response) => {
          setRek(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>
            <a href="/pesanan/riwayat/list-client">Riwayat Pesanan</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Detail Pesanan</Breadcrumb.Item>
        </Breadcrumb>

        <div className="px-5">
          <Card className={"relative px-10 rounded-[10px] top-0"}>
            <div className="flex ml-48">
              <PageHeader pageName={"Pesanan Baru"} />
            </div>

            <div className="flex relative">
              <div>
                <Form layout="vertical" form={form} onFinish={onFinish}>
                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      ID Pesanan :
                    </Typography.Title>
                    <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {pesanan?.order?.order_code}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Tanggal Mulai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {s_date}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Tanggal Selesai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {e_date}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Jam Mulai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {s_hour}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Jam Selesai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {e_hour}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Status :
                    </Typography.Title>
                    <div className="flex items-center bg-white  border-none w-96 h-8 rounded-lg ml-3 ">
                      <Tag
                        color={color(pesanan?.order?.status)[0]}
                        key={pesanan?.order?.status}
                      >
                        {color(pesanan?.order?.status)[1]}
                      </Tag>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Harga :
                    </Typography.Title>
                    <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {new Intl.NumberFormat("id-ID", {
                          style: "currency",
                          currency: "IDR",
                        }).format(pesanan?.order?.price)}
                      </Typography>
                    </div>
                  </FormItem>
                </Form>
              </div>
              {pesanan?.status == "rejected_by_admin" ? (
                <div className="w-96 absolute right-5 top-0 flex h-96 text-justify">
                  <div className="mr-5 mt-4">
                    <MdWarning className="w-8 h-8 text-edit " />
                  </div>
                  Pesanan anda sudah dibatalkan oleh admin karena anda belum
                  melakukan pembayaran sebelum tanggal pemakaian
                </div>
              ) : (
                <div className="flex flex-col place-content-center ml-20 w-5/6">
                  <Typography className="text-primary font-semibold text-lg mb-5">
                    List Rekening
                  </Typography>
                  <List
                    grid={{
                      gutter: 20,
                      column: 3,
                    }}
                    dataSource={rekening}
                    renderItem={(item) => (
                      <List.Item>
                        <div className="width-80 px-1">
                          <List.Item.Meta
                            title={item.owner_name}
                            description={item.bank_name}
                          />
                          {item.account_number}
                        </div>
                      </List.Item>
                    )}
                  />

                  <Typography className="text-primary font-semibold text-lg mt-5 mb-5">
                    Bukti Pembayaran
                  </Typography>
                  <Upload
                    name="avatar"
                    listType="picture-card"
                    className="avatar-uploader center"
                    showUploadList={false}
                    beforeUpload={beforeUpload}
                    customRequest={(args) => handleUpload(args)}
                  >
                    {imageUrl ? (
                      <img src={imageUrl} alt="avatar" className="mt-10" />
                    ) : (
                      uploadButton
                    )}
                  </Upload>

                  <div className="mt-10">
                    <Button
                      onClick={onFinish}
                      className="border-1 mt-5 border-primary font-sm text-primary rounded-lg mr-5 px-3 py-1 inline-block hover:bg-primary hover:text-white"
                    >
                      Kirim
                    </Button>
                  </div>
                </div>
              )}
            </div>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default detailPesananClientUpload;
