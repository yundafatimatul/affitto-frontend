import React, { useState, useEffect } from "react";
import {
  Card,
  Breadcrumb,
  Tag,
  Layout,
  Form,
  Typography,
  Button,
  message,
  Modal,
} from "antd";
import PageHeader from "../../../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import HeaderClient from "../../../../../components/Layout/DesktopLayout/HeaderClient";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../../../config/app";
import useAuthenticationPage from "../../../../../helper/authenticationPage";
import { MdWarning } from "react-icons/md";

const detailPesananClientUpload = () => {
  const [form] = Form.useForm();
  const [rev, setRev] = useState("");
  const [rep, setRep] = useState("");
  const [pesanan, setPesanan] = useState([]);
  const [visible, setVisible] = useState(false);
  const router = useRouter();
  const { id } = router.query;

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    loadLogPesanan();
    loadRev();
    loadRep();
  }, [id]);

  const loadLogPesanan = async () => {
    await axios
      .get(`http://localhost:3222/orders/detail/${id}`, config)
      .then((response) => {
        setPesanan(response.data.data);
      });
  };

  const loadRev = async () => {
    await axios
      .get(`http://localhost:3222/review/review/${id}`)
      .then((response) => {
        setRev(response.data.data);
      });
  };

  const loadRep = async () => {
    try {
      await axios
        .get(`http://localhost:3222/report-venue/findOrder/${id}`)
        .then((response) => {
          setRep(response.data.data);
          console.log("report", rep);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadCancel = async () => {
    try {
      console.log(config);
      await axios
        .put(`http://localhost:3222/orders/cancel/${id}`, "", config)
        .then((response) => {});
      setTimeout(message.success("Berhasil Mengajukan Pembatalan"), 1000);
      router.push("/pesanan/list-client");
    } catch (e) {
      console.log(e);
      setTimeout(message.error("Gagal Mengajukan Pembatalan"), 1000);
    }
  };

  const showModal = () => {
    setVisible(true);
  };

  const hideModal = () => {
    setVisible(false);
  };

  const s_date = `${new Date(pesanan?.order?.start_date).getDate()}-${new Date(
    pesanan?.order?.start_date
  ).getMonth()}-${new Date(pesanan?.order?.start_date).getFullYear()}`;

  const e_date = `${new Date(pesanan?.order?.end_date).getDate()}-${new Date(
    pesanan?.order?.end_date
  ).getMonth()}-${new Date(pesanan?.order?.end_date).getFullYear()}`;

  const s_hour = `${new Date(pesanan?.order?.start_date).getHours()}:${new Date(
    pesanan?.order?.start_date
  ).getMinutes()}`;

  const e_hour = `${new Date(pesanan?.order?.end_date).getHours()}:${new Date(
    pesanan?.order?.end_date
  ).getMinutes()}`;

  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "approved_by_owner") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "canceled_by_client") {
      array[0] = "gold";
      array[1] = "Pengajuan Pembatalan";
    } else if (stats === "canceled_by_owner") {
      array[0] = "volcano";
      array[1] = "Dibatalkan";
    }
    return array;
  };

  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>
            <a href="/pesanan/list-client">Pesanan</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Detail Pesanan</Breadcrumb.Item>
        </Breadcrumb>

        <div className="px-5">
          <Card className={"relative px-10 rounded-[10px] top-0"}>
            <div className="flex ml-48">
              <PageHeader pageName={"Pesanan Baru"} />
            </div>

            <div className="flex flex-row justify-center relative ">
              <div>
                <Form layout="vertical" form={form}>
                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      ID Pesanan :
                    </Typography.Title>
                    <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {pesanan?.order?.order_code}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Tanggal Mulai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {s_date}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Tanggal Selesai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {e_date}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Jam Mulai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {s_hour}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Jam Selesai :
                    </Typography.Title>
                    <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {e_hour}
                      </Typography>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Status :
                    </Typography.Title>
                    <div className="flex items-center bg-white  border-none w-96 h-8 rounded-lg ml-3 ">
                      <Tag
                        color={color(pesanan?.order?.status)[0]}
                        key={pesanan?.order?.status}
                      >
                        {color(pesanan?.order?.status)[1]}
                      </Tag>
                    </div>
                  </FormItem>

                  <FormItem>
                    <Typography.Title className="font-semibold text-sm ml-5">
                      Harga :
                    </Typography.Title>
                    <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                      <Typography className="font-light text-sm ml-3">
                        {new Intl.NumberFormat("id-ID", {
                          style: "currency",
                          currency: "IDR",
                        }).format(pesanan?.order?.price)}
                      </Typography>
                    </div>
                  </FormItem>
                </Form>
              </div>
              <div className="flex flex-col items-center ml-20">
                <Typography className="text-primary font-semibold text-lg mt-5 mb-5">
                  Bukti Pembayaran
                </Typography>
                <img
                  src={
                    appConfig.apiUrl +
                    "/upload/get-payment/" +
                    pesanan?.trans_proof
                  }
                  alt="avatar"
                  width={250}
                  height={550}
                />
                <div>
                  {rev == "null" &&
                  pesanan?.order?.status == "approved_by_owner" ? (
                    <Button
                      href={`/ulasan/tambah/${pesanan?.order?.id}`}
                      className="border-1 mt-10 mr-5 border-primary font-sm text-primary rounded-lg px-3 py-1 inline-block hover:bg-primary hover:text-white"
                    >
                      Buat Ulasan
                    </Button>
                  ) : (
                    ""
                  )}
                  {rep == "null" &&
                  pesanan?.order?.status == "approved_by_owner" ? (
                    <Button
                      href={`/laporan/tempat/${pesanan?.order?.id}`}
                      className=" border-1 mt-10 border-delete mr-5 font-sm text-delete rounded-lg px-3 py-1 inline-block hover:bg-delete hover:text-white"
                    >
                      Laporkan Tempat
                    </Button>
                  ) : (
                    ""
                  )}
                  {pesanan?.order?.status == "approved_by_owner" ? (
                    <Button
                      onClick={showModal}
                      className="border-1 mt-10 border-[#d58a0a] mr-5 font-sm text-[#d58a0a] rounded-lg px-3 py-1 inline-block hover:bg-[#d58a0a] hover:text-white"
                    >
                      Ajukan Pembatalan
                    </Button>
                  ) : (
                    ""
                  )}
                  <Modal
                    title="Pembatalan Pesanan"
                    visible={visible}
                    onOk={loadCancel}
                    onCancel={hideModal}
                    okText="Batalkan Pesanan"
                    cancelText="Kembali"
                  >
                    <div className="flex justify-center items-center mb-5">
                      <MdWarning className="w-10 h-10 text-edit " />
                    </div>
                    <p className="font-normal text-base">
                      Membatalkan pesanan akan mengembalikan dana anda dengan
                      potongan <span className="font-bold">10%,</span> apakah
                      anda yakin?{" "}
                    </p>
                  </Modal>
                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default detailPesananClientUpload;
