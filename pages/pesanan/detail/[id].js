import React, { useState, useEffect } from "react";
import {
  Card,
  Breadcrumb,
  Layout,
  Typography,
  Form,
  Button,
  message,
} from "antd";
import PageHeader from "../../../components/Items/PageHeader";
import HeaderClient from "../../../components/Layout/DesktopLayout/HeaderClient";
import { Rate } from "antd";
import { FcLike } from "react-icons/fc";
import FormItem from "antd/lib/form/FormItem";
import { DatePicker } from "antd";
import { useForm } from "antd/lib/form/Form";
import {
  MdLocationOn,
  MdAccountTree,
  MdGppGood,
  MdCategory,
  MdFamilyRestroom,
} from "react-icons/md";
import { useRouter } from "next/router";
import { tempatRepository } from "../../../repository/tempat";
import axios from "axios";
import { appConfig } from "../../../config/app";
import config from "../../../helper/config";
import useAuthenticationPage from "../../../helper/authenticationPage";

const detailOrder = () => {
  const [form] = useForm();
  const [start, setStart] = useState();
  const [end, setEnd] = useState();
  const [ft, setFoto] = useState([]);
  const [price, setPrice] = useState();
  const [ownerPrice, setOwnerPrice] = useState();
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    loadTempat();
  }, [id]);

  const loadTempat = async () => {
    try {
      await axios
        .get(`http://localhost:3222/venues/detail-photo/${id}`)
        .then((response) => {
          setFoto(response.data);
        });
    } catch (e) {
      console.log(e);
    }
  };

  const { data: dataTempat } = tempatRepository.hooks.useVenue(id);

  const rating = dataTempat?.data?.rating;
  const onFinishCreate = async () => {
    const dataAll = {
      start_date: start,
      end_date: end,
      price: ownerPrice,
      profit: price,
    };
    try {
      const res = await axios.post(
        `http://localhost:3222/orders/create/${id}`,
        dataAll,
        config
      );
      setTimeout(message.success("Pesanan Berhasil Dibuat"), 10000);
      router.push("/pesanan/list-client");
    } catch (e) {
      useAuthenticationPage(e);
      setTimeout(message.error(e.response.data.error), 1000);
    }
  };

  const url = ft.map((f) => {
    return f.image;
  });

  const onFinish = async () => {
    const dataAll = {
      start_date: start,
      end_date: end,
    };
    try {
      const res = await axios.post(
        `http://localhost:3222/orders/price/${id}`,
        dataAll,
        config
      );
      setPrice(res.data.data.admin);
      setOwnerPrice(res.data.data.owner);
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const { RangePicker } = DatePicker;

  return (
    <div>
      <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
        <div>
          <HeaderClient />
          <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
            <Breadcrumb.Item>
              <a href="/lokasi/tempat/list"></a>List Tempat
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <a href={`/lokasi/tempat/client/${id}`}>Detail Tempat</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Rincian Pesanan</Breadcrumb.Item>
          </Breadcrumb>

          <div className="px-5">
            <Card className={"relative px-10 rounded-[10px] top-0"}>
              <PageHeader pageName={"Rincian Pesanan"} />
              <div className="flex flex-row justify-center">
                <div className="xl:w-4/6 md:w-full sm:w-full p-5">
                  <Card className="shadow-lg rounded-md">
                    <Form form={form} onFinish={onFinish()}>
                      <FormItem name="tanggal" label="Tanggal">
                        <RangePicker
                          showTime
                          name="tanggal"
                          onChange={(dateString) => {
                            setStart(dateString[0].toString());
                            setEnd(dateString[1].toString());
                          }}
                          className="w-full"
                        />
                      </FormItem>
                    </Form>
                  </Card>
                  <Form className="relative mt-10">
                    <FormItem
                      name="deskripsi"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdAccountTree className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Deskripsi
                        </Typography>
                      </div>
                      {dataTempat?.data?.description}
                    </FormItem>
                    <FormItem
                      name="luas"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdCategory className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Luas Bangunan
                        </Typography>
                      </div>
                      {dataTempat?.data?.building_are}m
                    </FormItem>
                    <FormItem
                      name="kapasitas"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdFamilyRestroom className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Kapasitas
                        </Typography>
                      </div>
                      {dataTempat?.data?.max_capacity} Orang
                    </FormItem>
                    <FormItem
                      name="alamat"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdLocationOn className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Alamat
                        </Typography>
                      </div>
                      {dataTempat?.data?.address}
                    </FormItem>
                    <FormItem
                      name="fasilitas"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0 text-[#999999]">
                        <MdGppGood className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999] ">
                          Fasilitas
                        </Typography>
                      </div>
                      Meja, Kursi, Dekorasi
                    </FormItem>
                  </Form>
                </div>
                <div className="xl:w-2/6 md:w-full sm:w-full p-5">
                  <Card className="shadow-lg rounded-md">
                    <Typography className="text-lg font-semibold text-primary">
                      {dataTempat?.data?.nama}
                    </Typography>
                    <Rate disabled value={rating} />
                    <div className="flex text-sm inset-y-0 left-8 font-normal mb-3">
                      <FcLike className="mr-3 w-4 h-4" />
                      {dataTempat?.data?.like} Suka
                    </div>

                    <img
                      src={
                        appConfig.apiUrl + "/upload/get-venue/" + `${url[0]}`
                      }
                      alt="fotoVenue"
                      width={500}
                      height={300}
                      className="rounded-lg"
                    />
                    <Typography className="text-2xl mt-3 font-semibold text-primary">
                      Total Harga :
                      {price
                        ? new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                          }).format(price)
                        : " "}
                    </Typography>
                  </Card>
                  <Button
                    className="mt-5 float-right rounded-lg border-1 border-primary font-sm text-primary px-3 py-1 hover:bg-primary hover:text-white"
                    onClick={onFinishCreate}
                  >
                    Pesan Sekarang
                  </Button>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default detailOrder;
