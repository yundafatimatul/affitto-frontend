import { Button, List, message, Tag, Upload } from "antd";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import React, { useState, useEffect } from "react";
import { Card, Breadcrumb, Form, Typography } from "antd";
import PageHeader from "../../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../../config/app";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import config from "../../../../helper/config";
import {
  LoadingOutlined,
  PlusOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { imageRepository } from "../../../../repository/image";

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";

  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }

  const isLt2M = file.size / 1024 / 1024 < 2;

  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }

  return isJpgOrPng && isLt2M;
};

const detailPesananOwner = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [pesanan, setPesanan] = useState([]);
  const [adminPayment, setPayment] = useState([]);
  const [loading, setLoading] = useState(false);
  const [fileList, setFileList] = useState();
  const [rekening, setRek] = useState([]);
  const [imageUrl, setImageUrl] = useState();

  useEffect(() => {
    loadLogPesanan();
    loadAdminPayment();
    loadRekening();
  }, [id]);
  const loadRekening = async () => {
    try {
      await axios
        .get(`http://localhost:3222/bank/admin/${id}`, config)
        .then((response) => {
          setRek(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadLogPesanan = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/detail/${id}`, config)
        .then((response) => {
          setPesanan(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const loadAdminPayment = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/admin-payment/${id}`, config)
        .then((response) => {
          setPayment(response.data.data);
          console.log(adminPayment, "ADMIN");
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const handleUpload = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const processUpload = await imageRepository.manipulateData.upAdminPayment(
        file
      );
      setFileList(processUpload.body.data.filename);
      setImageUrl(
        appConfig.apiUrl +
          "/upload/admin-payment/" +
          processUpload.body.data.filename
      );
      setTimeout(message.success("Gambar Berhasil Di Unggah!"), 10000);
    } catch (e) {
      setTimeout(message.error("Gambar Gagal Di Unggah"), 10000);
    }
  };

  const s_date = `${new Date(pesanan?.order?.start_date).getDate()}-${new Date(
    pesanan?.order?.start_date
  ).getMonth()}-${new Date(pesanan?.order?.start_date).getFullYear()}`;

  const e_date = `${new Date(pesanan?.order?.end_date).getDate()}-${new Date(
    pesanan?.order?.end_date
  ).getMonth()}-${new Date(pesanan?.order?.end_date).getFullYear()}`;

  const s_hour = `${new Date(pesanan?.order?.start_date).getHours()}:${new Date(
    pesanan?.order?.start_date
  ).getMinutes()}`;

  const e_hour = `${new Date(pesanan?.order?.end_date).getHours()}:${new Date(
    pesanan?.order?.end_date
  ).getMinutes()}`;

  const onFinish = async () => {
    const data = { photo: fileList };
    if (imageUrl) {
      try {
        const res = await axios.put(
          `http://localhost:3222/payment/admin-paid/${adminPayment.id}`,
          data,
          config
        );
        setTimeout(message.success("Data Berhasil Diperbarui!"), 10000);
        router.push(`/pesanan/list-log`);
      } catch (e) {
        setTimeout(message.error(e.response.data.error), 10000);
      }
    } else {
      useAuthenticationPage(e);
      setTimeout(message.error("Mohon Unggah Bukti Pembayaran"), 10000);
    }
  };

  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "done") {
      array[0] = "cyan";
      array[1] = "Selesai";
    } else if (stats === "approved_by_owner") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "canceled_by_client") {
      array[0] = "gold";
      array[1] = "Pengajuan Pembatalan";
    } else if (stats === "approved_cancel_by_owner") {
      array[0] = "volcano";
      array[1] = "Dibatalkan Pelanggan";
    } else if (stats === "rejected") {
      array[0] = "magenta";
      array[1] = "Ditolak";
    } else if (stats === "rejected_by_admin") {
      array[0] = "red";
      array[1] = "Dibatalkan Admin";
    }
    return array;
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <a href="/pesanan/list-log">Log Pesanan</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Pesanan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Detail Pesanan"} />
        <div className="flex flex-row relative ">
          <div>
            <Form layout="vertical" form={form}>
              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  ID Pemesanan :
                </Typography.Title>
                <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {pesanan?.order?.order_code}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Tanggal Mulai :
                </Typography.Title>
                <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {s_date}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Tanggal Selesai :
                </Typography.Title>
                <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {e_date}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Jam Mulai :
                </Typography.Title>
                <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {s_hour}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Jam Selesai :
                </Typography.Title>
                <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {e_hour}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Status :
                </Typography.Title>
                <div className="flex items-center bg-white  border-none w-96 h-8 rounded-lg ml-3 ">
                  <Tag
                    color={color(pesanan?.order?.status)[0]}
                    key={pesanan?.order?.status}
                  >
                    {color(pesanan?.order?.status)[1]}
                  </Tag>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Harga :
                </Typography.Title>
                <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {new Intl.NumberFormat("id-ID", {
                      style: "currency",
                      currency: "IDR",
                    }).format(pesanan?.order?.price)}
                  </Typography>
                </div>
              </FormItem>
            </Form>
          </div>
          {/* <div></div> */}
          <div className="flex flex-col">
            <div className=" mr-2 w-auto ml-10">
              <Typography className="text-primary font-semibold text-lg mb-5">
                List Rekening
              </Typography>
              <List
                grid={{
                  gutter: 20,
                  column: 3,
                }}
                dataSource={rekening}
                renderItem={(item) => (
                  <List.Item>
                    <div className="width-80 px-1">
                      <List.Item.Meta
                        title={item.owner_name}
                        description={item.bank_name}
                      />
                      {item.account_number}
                    </div>
                  </List.Item>
                )}
              />
            </div>

            <div className="flex flex-row ">
              <div className="ml-10">
                <Typography className="text-primary font-semibold text-md mt-5 mb-5">
                  Bukti Pembayaran client
                </Typography>

                {pesanan?.trans_proof !== "pending" ? (
                  <img
                    src={
                      appConfig.apiUrl +
                      "/upload/get-payment/" +
                      pesanan?.trans_proof
                    }
                    alt="buktiTf"
                    width={240}
                    height={300}
                    className="rounded-sm"
                  />
                ) : (
                  <Card>
                    <img
                      src={"/no_data.svg"}
                      className="h-60 w-48 mt-14 opacity-50"
                    />
                    <div className="text-lg font-semibold text-center mt-8 text-gray-400">
                      Belum Ada
                    </div>
                  </Card>
                )}
              </div>
              {adminPayment?.trans_proof == "pending" ? (
                <div className="flex flex-col items-center mt-5">
                  <div className="">
                    <Typography className="text-primary font-semibold text-md mb-5">
                      Bukti Pembayaran Admin
                    </Typography>
                  </div>

                  <Upload
                    name="avatar"
                    // listType="picture-card"
                    type="primary"
                    className="avatar-uploader px-14"
                    showUploadList={false}
                    beforeUpload={beforeUpload}
                    customRequest={(args) => handleUpload(args)}
                  >
                    {imageUrl ? (
                      <img
                        src={imageUrl}
                        alt="avatar"
                        className=" w-64 h-[360px]"
                        // width={240}
                        // height={300}
                      />
                    ) : (
                      <Button icon={<UploadOutlined />}>Pilih Gambar</Button>
                    )}
                  </Upload>
                  <div>
                    <Button
                      onClick={onFinish}
                      className="border-1 mt-5 border-primary font-sm text-primary rounded-lg mr-5 px-3 py-1 inline-block hover:bg-primary hover:text-white"
                    >
                      Kirim
                    </Button>
                  </div>
                </div>
              ) : (
                <div className="flex flex-col items-center ml-10 w-fit">
                  <Typography className="text-primary font-semibold text-md mt-5 mb-5">
                    Bukti Pembayaran Admin
                  </Typography>

                  <img
                    src={
                      appConfig.apiUrl +
                      "/upload/admin-payment/" +
                      adminPayment.trans_proof
                    }
                    alt="buktiTf"
                    width={240}
                    height={300}
                    className="rounded-sm"
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

detailPesananOwner.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailPesananOwner;
