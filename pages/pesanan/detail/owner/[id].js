import { Button, message, Tag } from "antd";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import React, { useState, useEffect } from "react";
import { Card, Breadcrumb, Form, Typography } from "antd";
import PageHeader from "../../../../components/Items/PageHeader";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../../config/app";
import config from "../../../../helper/config";
import useAuthenticationPage from "../../../../helper/authenticationPage";

const detailPesananOwner = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [pesanan, setPesanan] = useState([]);
  const [adminPayment, setPayment] = useState([]);

  useEffect(() => {
    loadLogPesanan();
    loadAdminPayment();
  }, [id]);

  const loadLogPesanan = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/detail/${id}`, config)
        .then((response) => {
          setPesanan(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const s_date = `${new Date(pesanan?.order?.start_date).getDate()}-${new Date(
    pesanan?.order?.start_date
  ).getMonth()}-${new Date(pesanan?.order?.start_date).getFullYear()}`;

  const e_date = `${new Date(pesanan?.order?.end_date).getDate()}-${new Date(
    pesanan?.order?.end_date
  ).getMonth()}-${new Date(pesanan?.order?.end_date).getFullYear()}`;

  const s_hour = `${new Date(pesanan?.order?.start_date).getHours()}:${new Date(
    pesanan?.order?.start_date
  ).getMinutes()}`;

  const e_hour = `${new Date(pesanan?.order?.end_date).getHours()}:${new Date(
    pesanan?.order?.end_date
  ).getMinutes()}`;

  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "done") {
      array[0] = "cyan";
      array[1] = "Selesai";
    } else if (stats === "approved_by_owner") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "canceled_by_client") {
      array[0] = "gold";
      array[1] = "Pengajuan Pembatalan";
    } else if (stats === "approved_cancel_by_owner") {
      array[0] = "volcano";
      array[1] = "Dibatalkan Pelanggan";
    } else if (stats === "rejected") {
      array[0] = "magenta";
      array[1] = "Ditolak";
    } else if (stats === "rejected_by_admin") {
      array[0] = "red";
      array[1] = "Dibatalkan Admin";
    }
    return array;
  };

  const loadAdminPayment = async () => {
    try {
      await axios
        .get(`http://localhost:3222/orders/admin-payment/${id}`, config)
        .then((response) => {
          setPayment(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const handleConfirm = async () => {
    try {
      const endpoint = `http://localhost:3222/orders/approve-order/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Pesanan Berhasil Dikonfirmasi"), 10000);
      router.push("/pesanan/list-pesanan-baru");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const handleReject = async () => {
    try {
      const endpoint = `http://localhost:3222/orders/reject-order/${id}`;
      const res = await axios.put(endpoint, "", config);
      setTimeout(message.success("Pesanan Ditolak"), 10000);
      router.push("/pesanan/list-pesanan-baru");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <a href="/pesanan/list-pesanan-baru">Pesanan Baru</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Pesanan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Detail Pesanan"} />
        <div className="flex flex-row relative ">
          <div>
            <Form layout="vertical" form={form}>
              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  ID Pemesanan :
                </Typography.Title>
                <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {pesanan?.order?.order_code}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Tanggal Mulai :
                </Typography.Title>
                <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {s_date}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Tanggal Selesai :
                </Typography.Title>
                <div className="flex items-center bg-abu border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {e_date}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Jam Mulai :
                </Typography.Title>
                <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {s_hour}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Jam Selesai :
                </Typography.Title>
                <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {e_hour}
                  </Typography>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Status :
                </Typography.Title>
                <div className="flex items-center bg-white  border-none w-96 h-8 rounded-lg ml-3 ">
                  <Tag
                    color={color(pesanan?.order?.status)[0]}
                    key={pesanan?.order?.status}
                  >
                    {color(pesanan?.order?.status)[1]}
                  </Tag>
                </div>
              </FormItem>

              <FormItem>
                <Typography.Title className="font-semibold text-sm ml-5">
                  Harga :
                </Typography.Title>
                <div className="flex items-center bg-abu  border w-96 h-8 rounded-lg ml-3 ">
                  <Typography className="font-light text-sm ml-3">
                    {new Intl.NumberFormat("id-ID", {
                      style: "currency",
                      currency: "IDR",
                    }).format(pesanan?.order?.price)}
                  </Typography>
                </div>
              </FormItem>
            </Form>
          </div>
          <div className="flex relative">
            <div className="flex flex-col items-center mr-2 w-auto ml-10">
              <Typography className="text-primary font-semibold text-md mt-5 mb-5">
                Bukti Pembayaran client
              </Typography>
              {pesanan?.trans_proof !== "pending" ? (
                <img
                  src={
                    appConfig.apiUrl +
                    "/upload/get-payment/" +
                    pesanan?.trans_proof
                  }
                  alt="buktiTf"
                  width={240}
                  height={300}
                  className="rounded-sm"
                />
              ) : (
                <Card>
                  <img src={"/no_data.svg"} className="h-60 mt-14 opacity-50" />
                  <div className="text-lg font-semibold text-center mt-8 text-gray-400">
                    Belum Ada
                  </div>
                </Card>
              )}

              <div className="mt-10">
                <Button
                  onClick={handleConfirm}
                  className="border-1 border-konfirmasi font-sm text-konfirmasi rounded-lg mr-5 px-3 py-1 inline-block hover:bg-konfirmasi hover:text-white"
                >
                  Konfirmasi
                </Button>
                <Button
                  onClick={handleReject}
                  className="border-1 border-delete font-sm text-delete rounded-lg px-3 py-1 inline-block hover:bg-delete hover:text-white"
                >
                  Tolak
                </Button>
              </div>
            </div>
            {adminPayment?.trans_proof == "pending" ? (
              <div className="ml-10 items-center">
                <Typography className="text-primary text-center font-semibold text-md mt-5 mb-5">
                  Bukti Pembayaran Admin
                </Typography>
                <Card>
                  <img src={"/no_data.svg"} className="h-60 mt-14 opacity-50" />
                  <div className="text-lg font-semibold text-center mt-8 text-gray-400">
                    Belum Ada
                  </div>
                </Card>
              </div>
            ) : (
              <div className=" ml-10 w-fit">
                <Typography className="text-primary font-semibold text-md mt-5 mb-5">
                  Bukti Pembayaran Admin
                </Typography>

                <img
                  src={
                    appConfig.apiUrl +
                    "/upload/admin-payment/" +
                    adminPayment.trans_proof
                  }
                  alt="buktiTf"
                  width={240}
                  height={300}
                  className="rounded-sm"
                />
              </div>
            )}
          </div>
        </div>
      </Card>
    </div>
  );
};

detailPesananOwner.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailPesananOwner;
