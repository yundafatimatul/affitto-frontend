import { Space, Table, Tag } from "antd";
import React, { useState, useEffect } from "react";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { Card, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import PageHeader from "../../components/Items/PageHeader";
import Link from "next/dist/client/link";
import Search from "../../components/Items/Search";
import axios from "axios";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";

const listPembatalanPesanan = observer(() => {
  const store = useStore();
  const [order, setOrder] = useState();

  useEffect(() => {
    loadORder("");
  }, []);

  const loadORder = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/orders/listCancel?search=${val}`, config)
        .then((response) => {
          setOrder(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadORder(e.target.value);
  };

  const date = (a) => {
    const date = new Date(a);
    const c = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return c;
  };

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "order_code",
      key: "order_code",
      render: (text, record) => (
        <Link href={`/pesanan/detail/pembatalan/${record.id}`}>
          <a className="text-primary">{text}</a>
        </Link>
      ),
    },
    {
      title: "Nama Tempat",
      dataIndex: "venue",
      key: "venue",
      render: (text) => <p>{text?.name}</p>,
    },
    {
      title: "Tanggal Pesanan",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
      render: (text) => <p>{date(text)}</p>,
    },
  ];

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>Pesanan Batal</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Pembatalan Pesanan"} />
        <Search onChange={onChange} />
        <Table columns={columns} dataSource={order} />
      </Card>
    </div>
  );
});

listPembatalanPesanan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listPembatalanPesanan;
