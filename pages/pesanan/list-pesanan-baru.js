import React, { useState, useEffect } from "react";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { Card, Breadcrumb, Table, Tag } from "antd";
import PageHeader from "../../components/Items/PageHeader";
import { observer } from "mobx-react-lite";
import { useStore } from "../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../components/Items/Search";
import axios from "axios";
import moment from "moment";
import useAuthenticationPage from "../../helper/authenticationPage";
import config from "../../helper/config";

const listPesananBaru = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();

  useEffect(() => {
    loadLogPesanan("");
  }, []);

  const [stateLogPesanan, setLogPesanan] = useState();
  const loadLogPesanan = async (sa) => {
    try {
      await axios
        .get(`http://localhost:3222/orders/owner?search=${sa}`, config)
        .then((response) => {
          setLogPesanan(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadLogPesanan(e.target.value);
  };

  const date = (a) => {
    const date = new Date(a);
    const c = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return c;
  };

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "order",
      key: "order",
      render: (text, record) => (
        <Link href={`/pesanan/detail/owner/${record?.order?.id}`}>
          <a className="text-primary">{text.order_code}</a>
        </Link>
      ),
    },
    {
      title: "Nama Tempat",
      dataIndex: "order",
      key: "order",
      render: (text) => <div>{text?.venue?.name}</div>,
    },
    {
      title: "Nama Pelanggan",
      dataIndex: "order",
      key: "order",
      render: (text) => <div>{text?.user?.name}</div>,
    },
    {
      title: "Tanggal Pesanan",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
      render: (text) => <div>{date(text)}</div>,
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>Pesanan Baru</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Pesanan Baru"} />
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateLogPesanan}
        />
      </Card>
    </div>
  );
});

listPesananBaru.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listPesananBaru;
