import React, { useState, useEffect } from "react";
import { Button, Table } from "antd";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import PageHeader from "../../../components/Items/PageHeader";
import axios from "axios";
import { TokenUtil } from "../../../utils/token";
import { parseJwt } from "../../../helper/parseJwt";
import moment from "moment";
import config from "../../../helper/config";
import useAuthenticationPage from "../../../helper/authenticationPage";
import { UserDeleteOutlined } from "@ant-design/icons";

const listPemasukan = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [pemasukan, setPemasukan] = useState();
  const store = useStore();

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }

  const decodeJwt = parseJwt(token);
  const usr = decodeJwt?.existUser?.id;

  const link = (ids) => {
    if (decodeJwt?.existUser?.role?.name == "owner") {
      return `/pesanan/detail/owner/${ids}`;
    } else if (decodeJwt?.existUser?.role?.name == "admin") {
      return `/pesanan/detail/admin/${ids}`;
    }
  };
  useEffect(() => {
    loadPemasukan("");
  }, []);
  const loadPemasukan = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/payment/income?search=${val}`, config)
        .then((response) => {
          setPemasukan(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  const onChange = (e) => {
    loadPemasukan(e.target.value);
  };

  const date = (a) => {
    const date = new Date(a);
    const c = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    return c;
  };

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "order",
      key: "order",
      render: (text, record) => (
        <div>
          <Link href={link(record?.order?.id)}>
            <a className="text-[#1363DF]">{text?.order_code}</a>
          </Link>
        </div>
      ),
    },
    {
      title: "Nama Tempat",
      dataIndex: "order",
      key: "order",
      render: (text) => <p>{text?.venue?.name}</p>,
    },
    {
      title: "Tanggal",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
      render: (text) => <p>{date(text)}</p>,
    },
    {
      title: "Pemasukan",
      dataIndex: "owner_price",
      key: "price",
      sorter: (a, b) => a?.price - b?.price,
      render: (text) => (
        <p>
          {new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
          }).format(text)}
        </p>
      ),
    },
    {
      title: "Jumlah",
      dataIndex: "order",
      key: "order",
      sorter: (a, b) => a?.order?.price - b?.order?.price,
      render: (text) => (
        <p>
          {new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
          }).format(text?.price)}
        </p>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Pemasukan</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Pemasukan"} />
        <div className="absolute top-7 right-8">
          <div className="absolute top-7 right-8">
            <Link href={`http://localhost:3222/payment/download-excel/${usr}`}>
              <div>
                {" "}
                <Button className="border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 inline-block hover:bg-primary hover:text-white">
                  Unduh Pemasukan
                </Button>
              </div>
            </Link>
          </div>
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={pemasukan}
        />
      </Card>
    </div>
  );
});

listPemasukan.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listPemasukan;
