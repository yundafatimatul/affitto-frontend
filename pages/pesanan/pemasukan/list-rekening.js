import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import {
  Space,
  Table,
  Modal,
  Input,
  Button,
  Form,
  Card,
  Breadcrumb,
  message,
} from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import { useForm } from "antd/lib/form/Form";
import Search from "../../../components/Items/Search";
import PageHeader from "../../../components/Items/PageHeader";
import axios from "axios";
import { TokenUtil } from "../../../utils/token";
import config from "../../../helper/config";
import useAuthenticationPage from "../../../helper/authenticationPage";

const listRekening = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [bank, setBank] = useState();

  const store = useStore();
  useEffect(() => {
    loadRekening("");
  }, []);

  const loadRekening = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/bank?search=${val}`, config)
        .then((response) => {
          setBank(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadRekening(e.target.value);
  };

  const { confirm } = Modal;
  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        const endpoint = `http://localhost:3222/bank/delete/${data.id}`;
        const res = await axios.delete(endpoint, config);
        setTimeout(message.success("Rekening Berhasil Di Hapus"), 10000);
        loadRekening("");
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const columns = [
    {
      title: "Nama",
      dataIndex: "owner_name",
      key: "owner_name",
    },
    {
      title: "No. Rek",
      dataIndex: "account_number",
      key: "account_number",
    },
    {
      title: "Nama Bank",
      dataIndex: "bank_name",
      key: "bank_name",
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg border-1 border-edit font-sm text-edit hover:bg-edit hover:text-white"
            htmlType="submit"
            onClick={() => {
              functionEdit(record);
            }}
          >
            Edit
          </Button>
          <Button
            className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
            onClick={() => {
              showDeleteConfirm(record);
            }}
          >
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  const [form1] = useForm();
  const [form2] = useForm();
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);

  const showModal = () => {
    setModalCreate(true);
  };

  const handleCancel = () => {
    setModalCreate(false);
    setModalUpdate(false);
    form1.resetFields();
    form2.resetFields();
  };

  const showModalUpdate = () => {
    setModalUpdate(true);
  };

  const functionEdit = (val) => {
    form1.setFieldsValue({
      id: val.id,
      owner_name: val.owner_name,
      account_number: val.account_number,
      bank_name: val.bank_name,
    });
    showModalUpdate();
  };
  const handleSubmit = async () => {
    try {
      const val = form2.getFieldsValue();
      const res = await axios.post(
        "http://localhost:3222/bank/create",
        val,
        config
      );
      setModalCreate(false);
      setTimeout(message.success("Rekening Berhasil Dibuat"), 10000);
      loadRekening("");
      form2.resetFields();
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onFinishUpdate = async () => {
    try {
      const val = form1.getFieldsValue();
      const data = {
        owner_name: val.owner_name,
        account_number: val.account_number,
        bank_name: val.bank_name,
      };
      const res = await axios.put(
        `http://localhost:3222/bank/update/${val.id}`,
        data,
        config
      );
      setModalUpdate(false);
      setTimeout(message.success("Rekening Berhasil Diubah"), 10000);
      loadRekening("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Rekening</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Rekening"} />
        <div className="absolute top-7 right-8">
          <Button
            className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
            onClick={showModal}
          >
            + Tambah
          </Button>
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={bank}
        />

        {/* modal Create */}
        <Modal
          name="form-create"
          visible={modalCreate}
          onCancel={handleCancel}
          title="Tambah Rekening"
          footer={false}
        >
          <Form form={form2} layout="vertical" onFinish={handleSubmit}>
            <Form.Item name="owner_name" label="Nama">
              <Input placeholder="Masukkan Nama Pemilik" />
            </Form.Item>
            <Form.Item name="account_number" label="No. Rek">
              <Input placeholder="Masukkan No Rekening" />
            </Form.Item>
            <Form.Item name="bank_name" label="Nama Bank">
              <Input placeholder="Masukkan Nama Bank" />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Tambah
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* modal Update */}
        <Modal
          name="form-update"
          visible={modalUpdate}
          onCancel={handleCancel}
          title="Edit Rekening"
          footer={false}
        >
          <Form form={form1} layout="vertical" onFinish={onFinishUpdate}>
            <Form.Item name="id" hidden={true}>
              <Input readOnly={true} />
            </Form.Item>
            <Form.Item name="owner_name" label="Nama">
              <Input placeholder="Masukkan Nama Pemilik" />
            </Form.Item>
            <Form.Item name="account_number" label="No. Rek">
              <Input placeholder="Masukkan No Rekening" />
            </Form.Item>
            <Form.Item name="bank_name" label="Nama Bank">
              <Input placeholder="Masukkan Nama Bank" />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Simpan
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    </div>
  );
});

listRekening.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listRekening;
