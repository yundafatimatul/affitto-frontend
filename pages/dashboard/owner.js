import React, { useState, useEffect } from "react";
import { Breadcrumb, Card } from "antd";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import PageHeader from "../../components/Items/PageHeader";

const dashboardOwner = observer(() => {
  const data = [
    {
      nama: "Total Tempat",
      value: "4,9",
      icon: "15.000.000",
    },
    {
      nama: "Total Mitra",
      value: "4,9",
      icon: "15.000.000",
    },
    {
      nama: "Total Pelanggan",
      value: "4,9",
      icon: "15.000.000",
    },
  ];

  return (
    <div className="mt-14">
      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Dashboard"} />
      </Card>
    </div>
  );
});

dashboardOwner.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default dashboardOwner;
