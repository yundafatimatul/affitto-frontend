import React, { useState, useEffect } from "react";
import {
  Card,
  Breadcrumb,
  Layout,
  Form,
  Typography,
  Button,
  Input,
} from "antd";
import HeaderClient from "../../components/Layout/DesktopLayout/HeaderClient";
import FormItem from "antd/lib/form/FormItem";
import { DatePicker } from "antd";
import { Radio } from "antd";
import Image from "next/image";
import bgimg from "../../public/dashboard.png";
import SwiperVenue from "../../components/Items/SwiperVenue";
import "swiper/css";
import "swiper/css/pagination";
import axios from "axios";

const listPesananClient = (props) => {
  const { RangePicker } = DatePicker;
  const [form] = Form.useForm();
  const [venue, setVenue] = useState([]);
  const [order, setorder] = useState([]);
  const [search, setSearch] = useState("");
  const [start, setStart] = useState("");
  const [end, setEnd] = useState("");
  const [current, setCurrent] = useState(1);
  const [totalItems, setTotal] = useState();
  const [value, setValue] = useState(1);
  const [stateKategori, setKategori] = useState([]);
  const [cat, setCat] = useState("");

  useEffect(() => {
    loadKategori();
    loadTempat(1);
    loadOrder();
  }, []);

  let dataKat = [];
  dataKat.push(stateKategori);
  const loadKategori = async () => {
    await axios.get("http://localhost:3222/category").then((response) => {
      setKategori(response.data.items);
    });
  };
  let kat = dataKat[0];

  const handleChangeCat = (value) => {
    setCat(value.target.value);
  };

  const limit = 32;
  const loadTempat = async (page) => {
    try {
      const url = "http://localhost:3222/venues/cc";
      await axios
        .get(
          `${url}?start_date=${start}&end_date=${end}&lokasi=${search}&page=${page}&limit=${limit}&cat=${cat}`
        )
        .then((response) => {
          setCurrent(response.data.meta.currentPage);
          setTotal(response.data.meta.totalItems);
          setVenue(response.data.items);
        });
    } catch (e) {
      console.log(e);
    }
  };

  const loadOrder = async () => {
    try {
      await axios.get(`http://localhost:3222/orders/find`).then((response) => {
        setorder(response.data);
      });
    } catch (e) {
      console.log(e);
    }
  };

  const key = venue.map((dataOld) => {
    const dataAll = {
      image: dataOld?.venue_photo.map((v) => {
        return v.photo;
      }),
      nama: dataOld.name,
      harga: dataOld.price,
      kategori: dataOld?.category?.name,
      id: dataOld.id,
      kota: dataOld?.district?.city?.name,
      likes: dataOld?.likes,
    };
    return dataAll;
  });

  const onFinish = () => {
    loadTempat(1);
  };

  const onCurrent = (page) => {
    loadTempat(page);
  };

  return (
    <>
      <Layout theme={"light"} className={"bg-[white] min-h-screen"}>
        <div>
          <HeaderClient />
          <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
            <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
          </Breadcrumb>

          <div className="px-10 relative">
            <div className="relative px-10 rounded-[10px] top-0">
              <div className="absolute z-20 mr-10">
                <Image src={bgimg} alt="bgimg" />
              </div>
              <div className="flex flex-col px-[100px] absolute mt-5 z-40">
                <Typography className="text-white text-3xl mb-3 font-extrabold flex justify-center tracking-wide">
                  Pastikan dapat tempat terbaik di
                  <a className="text-primary ml-2">affitto</a> !
                </Typography>
                <Card className="shadow-lg rounded-md w-[900px] h-60 z-30">
                  <Form form={form}>
                    <FormItem name="lokasi" label="Lokasi">
                      <div className="flex flex-y-0">
                        <Input
                          placeholder="contoh: Jakarta, Bogor, Bandung"
                          className="text-[black] outline-none font-light text-[12px] ml-4"
                          onChange={(e) => {
                            setSearch(e.target.value);
                          }}
                        />
                      </div>
                    </FormItem>
                    <FormItem name="tanggal" label="Tanggal">
                      <RangePicker
                        className="w-full"
                        onChange={(dateString) => {
                          setStart(
                            `${new Date(dateString[0]).getFullYear()},${
                              new Date(dateString[0]).getMonth() + 1
                            },${new Date(dateString[0]).getDate()}`
                          );
                          setEnd(
                            `${new Date(dateString[1]).getFullYear()},${
                              new Date(dateString[1]).getMonth() + 1
                            },${new Date(dateString[1]).getDate()}`
                          );
                        }}
                      />
                    </FormItem>
                    <FormItem name="kategori" label="Kategori">
                      <Radio.Group onChange={handleChangeCat} value={value}>
                        {kat.map((cat) => (
                          <Radio value={cat?.id}>{cat?.name}</Radio>
                        ))}
                      </Radio.Group>
                    </FormItem>
                    <FormItem>
                      <Button
                        onClick={onFinish}
                        className="border-1 border-primary font-sm text-primary rounded-full px-5 py-1 float-right hover:bg-primary hover:text-white"
                      >
                        Cari
                      </Button>
                    </FormItem>
                  </Form>
                </Card>
              </div>
            </div>
          </div>
        </div>

        <div className="px-5 mt-80 ">
          <SwiperVenue
            data={key}
            onCurrent={onCurrent}
            totalItems={totalItems}
          />
        </div>
      </Layout>
    </>
  );
};

export default listPesananClient;
