import React, { useState, useEffect } from "react";
import { Breadcrumb, Card } from "antd";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import { observer } from "mobx-react-lite";
import PageHeader from "../../components/Items/PageHeader";
import CardAdmin from "../../components/Items/dashboard/CardAdmin";
import TableAdmin from "../../components/Items/dashboard/TableAdmin";

const dashboardAdmin = (props) => {
  return (
    <div className="mt-14">
      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Dashboard"} />
        <CardAdmin />
        <TableAdmin />
      </Card>
    </div>
  );
};

dashboardAdmin.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default dashboardAdmin;
