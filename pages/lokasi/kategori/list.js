import { Space, Table, Tag, Button } from "antd";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb, Modal, Form, Input, message } from "antd";
import PageHeader from "../../../components/Items/PageHeader";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import Search from "../../../components/Items/Search";
import { useEffect, useState } from "react";
import axios from "axios";
import { useForm } from "antd/lib/form/Form";
import useAuthenticationPage from "../../../helper/authenticationPage";
import config from "../../../helper/config";

const listKategori = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const [nameCat, setName] = useState("");
  const [stateKategori, setKategori] = useState();
  const store = useStore();

  const handleSubmit = async (e) => {
    try {
      const data = {
        name: nameCat,
      };

      const endpoint = "http://localhost:3222/category/create";

      const res = await axios.post(endpoint, data, config);
      setModalCreate(false);
      setTimeout(message.success("Kategori Berhasil Dibuat"), 10000);
      loadKategori("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  useEffect(() => {
    loadKategori("");
  }, []);

  const { confirm } = Modal;
  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const endpoint = `http://localhost:3222/category/delete/${data.id}`;
          const res = await axios.delete(endpoint, config);
          setTimeout(message.success("Kategori Berhasil Di Hapus"), 10000);
          loadKategori("");
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const loadKategori = async (val) => {
    try {
      await axios
        .get(`http://localhost:3222/category?search=${val}`, config)
        .then((response) => {
          setKategori(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onChange = (e) => {
    loadKategori(e.target.value);
  };

  const columns = [
    {
      title: "Kategori",
      dataIndex: "name",
      key: "nama",
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Space>
          <Button
            className="rounded-lg border-1 border-edit font-sm text-edit hover:bg-edit hover:text-white"
            htmlType="submit"
            onClick={() => {
              functionEdit(record);
            }}
          >
            Edit
          </Button>
          <Button
            className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
            onClick={() => {
              showDeleteConfirm(record);
            }}
          >
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  const [form] = useForm();
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);

  const showModal = () => {
    setModalCreate(true);
  };

  const handleCancel = () => {
    setModalCreate(false);
    setModalUpdate(false);
    form.resetFields();
  };

  const showModalUpdate = () => {
    setModalUpdate(true);
  };

  const functionEdit = (val) => {
    form.setFieldsValue({
      id: val.id,
      kategori: val.name,
    });
    showModalUpdate();
  };

  const onFinishUpdate = async () => {
    try {
      const dataUp = form.getFieldValue();
      const data = {
        name: nameCat,
      };
      const endpoint = `http://localhost:3222/category/update/${dataUp.id}`;

      const res = await axios.put(endpoint, data, config);
      setModalUpdate(false);
      setTimeout(message.success("Kategori Berhasil Diubah"), 10000);
      loadKategori("");
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5">
        <Breadcrumb.Item>List Kategori</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Kategori"} />
        <div className="absolute top-7 right-8">
          <Button
            className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
            onClick={showModal}
          >
            + Tambah
          </Button>
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateKategori}
        />

        {/* modal Create */}
        <Modal
          name="form-create"
          visible={modalCreate}
          onCancel={handleCancel}
          title="Tambah Kategori"
          footer={false}
        >
          <Form form={form} layout="vertical" onFinish={handleSubmit}>
            <Form.Item name="kategori" label="Nama Kategori">
              <Input
                placeholder="Masukkan Kategori"
                name="name"
                value={nameCat}
                onChange={(e) => {
                  e.target.name = nameCat;
                  setName(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Tambah
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* modal Update */}
        <Modal
          name="form-update"
          visible={modalUpdate}
          onCancel={handleCancel}
          title="Edit Kategori"
          footer={false}
        >
          <Form form={form} layout="vertical" onFinish={onFinishUpdate}>
            <Form.Item name="id_kategori" hidden={true}>
              <Input readOnly={true} />
            </Form.Item>
            <Form.Item name="kategori" label="Nama Kategori">
              <Input
                placeholder="Masukkan nama Kategori"
                name="name"
                onChange={(e) => {
                  e.target.name = nameCat;
                  setName(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                htmlType="submit"
              >
                Simpan
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    </div>
  );
});

listKategori.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listKategori;
