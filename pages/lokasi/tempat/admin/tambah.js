import {
  Button,
  Form,
  message,
  Space,
  Card,
  Breadcrumb,
  Typography,
  Upload,
  Select,
  Input,
  Modal,
} from "antd";
import axios from "axios";
import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import PageHeader from "../../../../components/Items/PageHeader";
import { PlusOutlined } from "@ant-design/icons";
import FormItem from "antd/lib/form/FormItem";
import Link from "next/link";
import { imageRepository } from "../../../../repository/image";
import { appConfig } from "../../../../config/app";
import { useRouter } from "next/router";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import config from "../../../../helper/config";
import { parseJwt } from "../../../../helper/parseJwt";

const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => resolve(reader.result);

    reader.onerror = (error) => reject(error);
  });

const tambahTempat = (props) => {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [cat, setCat] = useState("");
  const [kec, setKec] = useState("");
  const [mit, setMit] = useState("");
  const [stateMitra, setMitra] = useState([]);
  const [fileList, setFileList] = useState([]);
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const router = useRouter();

  const handleChangeFasilitas = (value) => {
    setFa(value);
  };

  useEffect(() => {
    loadKategori();
    loadProvinsi();
    loadFas();
    loadMitra();
  }, []);
  const [stateKategori, setKategori] = useState([]);
  const [fas, setFas] = useState([]);
  const [fa, setFa] = useState([]);
  const [provinsi, setProvinsi] = useState([]);
  const [kota, setKota] = useState([]);
  const [kecamatan, setKecamatan] = useState([]);

  let dataKat = [];
  dataKat.push(stateKategori);
  const loadKategori = async () => {
    try {
      await axios.get("http://localhost:3222/category").then((response) => {
        setKategori(response.data.items);
      });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const decodeJwt = parseJwt(token);

  let role;
  if (decodeJwt?.existUser) {
    role = decodeJwt.existUser?.role?.name;
  }

  let dataFas = [];
  dataFas.push(fas);
  const loadFas = async () => {
    try {
      await axios
        .get("http://localhost:3222/facility", config)
        .then((response) => {
          setFas(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let dataProv = [];
  dataProv.push(provinsi);
  const loadProvinsi = async () => {
    try {
      await axios
        .get("http://localhost:3222/provinces", config)
        .then((response) => {
          setProvinsi(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  let dataKota = [];
  dataKota.push(kota);
  const loadKota = async (idProv) => {
    try {
      await axios
        .get(`http://localhost:3222/cities/${idProv}`, config)
        .then((response) => {
          setKota(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let dataKec = [];
  dataKec.push(kecamatan);
  const loadKec = async (idCity) => {
    try {
      await axios
        .get(`http://localhost:3222/districts/${idCity}`, config)
        .then((response) => {
          setKecamatan(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  let kat = dataKat[0];
  let ctg;
  let district;

  const onChangeProv = (value) => {
    loadKota(value);
  };
  const onChangeKota = (value) => {
    loadKec(value);
  };

  const onChangeDist = (value) => {
    district = value;
    setKec(value);
  };

  const onChangeMit = (value) => {
    setMit(value);
  };

  const onSearch = (value) => {
    console.log("search:", value);
  };
  const handleChangeCat = (value) => {
    ctg = value;
    setCat(value);
  };

  let dataMitra = [];
  dataMitra.push(stateMitra);
  const loadMitra = async () => {
    try {
      await axios
        .get(`http://localhost:3222/user/listOwner`, config)
        .then((response) => {
          setMitra(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onFinish = async () => {
    const data = form.getFieldsValue();
    const files = fileList.map((file) => {
      return file.name;
    });
    const dataAll = {
      name: data.Nama,
      address: data.Alamat,
      building_area: data.Luas_Bangunan,
      max_capacity: data.Kapasitas,
      price: data.Harga,
      description: data.Deskripsi,
      files: files,
      facility: fa,
    };
    if (role == "admin") {
      dataAll.user = mit;
    }
    try {
      const res = await axios.post(
        `http://localhost:3222/venues/create/${cat}/${kec}`,
        dataAll,
        config
      );
      setTimeout(message.success("Data Berhasil Dibuat!"), 10000);
      router.push(`/lokasi/tempat/list`);
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const onFinishFailed = () => {
    message.error("Data Gagal Diperbarui!");
  };

  const validateMessages = {
    required: "${label} Harus Diisi!",
  };

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    setPreviewImage(file.url);
    setPreviewVisible(true);
    setPreviewTitle("Gambar");
  };

  const handleChange = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const processUpload = await imageRepository.manipulateData.upVenue(file);

      setFileList([
        ...fileList,
        {
          url:
            appConfig.apiUrl +
            "/upload/get-venue/" +
            processUpload.body.data.filename,
          name: processUpload.body.data.filename,
        },
      ]);

      message.success("Gambar Berhasil Di Unggah!");
    } catch (e) {
      console.log(e, "ini catch e");
      setTimeout(message.error("Gambar Gagal Di Unggah Plis Deh"), 10000);
    }
  };
  const onRemoveFile = (info) => {
    const index = fileList.indexOf(info);
    console.log("onRemove = ", fileList);
  };

  const handleChanges = ({ fileList: newFileList }) => setFileList(newFileList);

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload Foto
      </div>
    </div>
  );

  const selectMitAdmin = (
    <Select
      placeholder="Pilih mitra"
      style={{
        width: 380,
      }}
      onChange={onChangeMit}
    >
      {dataMitra[0].map((cat) => (
        <Select.Option value={cat?.id}>
          {cat?.name} - {cat?.username}
        </Select.Option>
      ))}
    </Select>
  );

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href={"/lokasi/tempat/list"}>List Tempat</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Tambah Tempat</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Tambah Tempat"} />
        <div className="flex flex-row  mt-5">
          <div className="flex flex-col items-center justify-between mt-5 relative ">
            <Card className="rounded-sm">
              <div className="">
                <Form
                  form={form}
                  layout="vertical"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                  validateMessages={validateMessages}
                >
                  <div>
                    <FormItem
                      name="Nama"
                      rules={[
                        {
                          required: true,
                        },
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-8">
                          Nama Tempat
                        </Typography.Title>

                        <Input
                          placeholder="Masukan Nama Tempat"
                          className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2"
                        />
                      </div>
                    </FormItem>

                    <FormItem
                      name="Kategori"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[75px]">
                          Kategori
                        </Typography.Title>
                        <div className="ml-2">
                          <Select
                            style={{
                              width: 380,
                            }}
                            onChange={handleChangeCat}
                          >
                            {kat.map((cat) => (
                              <Select.Option value={cat?.id}>
                                {cat?.name}
                              </Select.Option>
                            ))}
                          </Select>
                        </div>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Mitra"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[75px]">
                          Mitra
                        </Typography.Title>

                        <div className="ml-8">{selectMitAdmin}</div>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Luas_Bangunan"
                      rules={[
                        {
                          required: true,
                        },
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[25px]">
                          Luas Bangunan
                        </Typography.Title>

                        <Input
                          placeholder="Masukan Masukan Luas Bangunan"
                          className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2"
                        />
                      </div>
                    </FormItem>

                    <FormItem
                      name="Kapasitas"
                      rules={[
                        {
                          required: true,
                        },
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[63px]">
                          Kapasitas
                        </Typography.Title>

                        <Input
                          placeholder="Masukan Masukan Kapasitas"
                          className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2"
                        />
                      </div>
                    </FormItem>

                    <FormItem
                      name="fasilitas"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[76px]">
                          Fasilitas
                        </Typography.Title>

                        <Select
                          mode="multiple"
                          placeholder="Pilih atau tambah Fasilitas"
                          style={{
                            width: "100%",
                          }}
                          className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2"
                          onChange={handleChangeFasilitas}
                        >
                          {dataFas[0].map((fas) => (
                            <Select.Option value={fas?.id}>
                              {fas?.name}
                            </Select.Option>
                          ))}
                        </Select>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Deskripsi"
                      rules={[
                        {
                          required: true,
                        },
                        {
                          type: "string",
                        },
                        {
                          type: "textarea",
                          warningOnly: true,
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[70px]">
                          Deskripsi
                        </Typography.Title>
                        <TextArea
                          placeholder="Masukan Deskripsi"
                          className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-10 flex-1 ml-2 "
                        ></TextArea>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Provinsi"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[85px]">
                          Provinsi
                        </Typography.Title>

                        <Select
                          className="font-light text-[12px]"
                          showSearch
                          placeholder="Pilih Provinsi"
                          optionFilterProp="children"
                          onChange={onChangeProv}
                          onSearch={onSearch}
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .includes(input.toLowerCase())
                          }
                        >
                          {dataProv[0].map((prov) => (
                            <Select.Option value={prov?.id}>
                              {prov?.name}
                            </Select.Option>
                          ))}
                        </Select>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Kota"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[105px]">
                          Kota
                        </Typography.Title>

                        <Select
                          className="font-light text-[12px]"
                          showSearch
                          placeholder="Pilih Kota"
                          optionFilterProp="children"
                          onChange={onChangeKota}
                          onSearch={onSearch}
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .includes(input.toLowerCase())
                          }
                        >
                          {dataKota[0].map((kota) => (
                            <Select.Option value={kota?.id}>
                              {kota?.name}
                            </Select.Option>
                          ))}
                        </Select>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Kecamatan"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[55px]">
                          Kecamatan
                        </Typography.Title>

                        <Select
                          className="font-light text-[12px]"
                          showSearch
                          placeholder="Pilih Kecamatan"
                          optionFilterProp="children"
                          onChange={onChangeDist}
                          onSearch={onSearch}
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .includes(input.toLowerCase())
                          }
                        >
                          {dataKec[0].map((kec) => (
                            <Select.Option value={kec?.id}>
                              {kec?.name}
                            </Select.Option>
                          ))}
                        </Select>
                      </div>
                    </FormItem>

                    <FormItem
                      name="Alamat"
                      rules={[
                        {
                          required: true,
                        },
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[78px]">
                          Alamat
                        </Typography.Title>

                        <Input
                          placeholder="Masukan Masukan Alamat"
                          className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 ml-2"
                        />
                      </div>
                    </FormItem>

                    <FormItem
                      name="Harga"
                      rules={[
                        {
                          required: true,
                        },
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <div className="flex inset-y-0">
                        <Typography.Title className="font-light text-sm ml-5 mr-[93px]">
                          Harga
                        </Typography.Title>

                        <Input prefix="Rp." suffix="IDR" />
                        <span className=" absolute top-10 left-36 text-xs font-thin w-auto px-3">
                          *Cantumkan harga per jam
                        </span>
                      </div>
                    </FormItem>

                    <Form.Item>
                      <Space>
                        <Button
                          className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                          htmlType="submit"
                        >
                          Tambah
                        </Button>
                      </Space>
                    </Form.Item>
                  </div>
                </Form>
              </div>
            </Card>
          </div>
          <div className="px-10">
            <div className="mt-3 ml-[40px]">
              <Upload
                listType="picture-card"
                fileList={fileList}
                onPreview={handlePreview}
                customRequest={(args) => handleChange(args)}
                onChange={handleChanges}
                maxCount={5}
              >
                {uploadButton}
              </Upload>
              <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={null}
                onCancel={handleCancel}
              >
                <img
                  alt="example"
                  style={{
                    width: "100%",
                  }}
                  src={previewImage}
                />
              </Modal>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

tambahTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default tambahTempat;
