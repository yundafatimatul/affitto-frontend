import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import PageHeader from "../../../components/Items/PageHeader";
import {
  Card,
  Breadcrumb,
  Typography,
  Form,
  Button,
  Comment,
  List,
  message,
  Modal,
} from "antd";
import Link from "next/link";
import { appConfig } from "../../../config/app";
import { Rate } from "antd";
import FormItem from "antd/lib/form/FormItem";
import { useRouter } from "next/router";
import {
  MdLocationOn,
  MdAccountTree,
  MdGppGood,
  MdCategory,
  MdFamilyRestroom,
} from "react-icons/md";
import { tempatRepository } from "../../../repository/tempat";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Thumbs } from "swiper";
import axios from "axios";
import useAuthenticationPage from "../../../helper/authenticationPage";
import { FcLike } from "react-icons/fc";
import config from "../../../helper/config";

const detailTempat = (props) => {
  const router = useRouter();
  const { id } = router.query;
  const [ft, setFoto] = useState([]);
  const [rv, setReview] = useState([]);
  const [fas, setFas] = useState([]);

  useEffect(() => {
    loadTempat();
    loadReview();
    loadFasilitas();
  }, [id]);

  const { data: dataTempat } = tempatRepository.hooks.useVenue(id);

  const rating = dataTempat?.data?.rating;
  const date = (a) => {
    return `${new Date(a).getDate()}-${new Date(a).getMonth()}-${new Date(
      a
    ).getFullYear()} || ${new Date(a).getHours()}:${new Date(a).getMinutes()}`;
  };

  const loadTempat = async () => {
    try {
      await axios
        .get(`http://localhost:3222/venues/detail-photo/${id}`)
        .then((response) => {
          setFoto(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadFasilitas = async () => {
    try {
      await axios
        .get(`http://localhost:3222/facility/fasilitas/${id}`)
        .then((response) => {
          setFas(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const fasilitas = fas.join(", ");
  const loadReview = async () => {
    try {
      await axios
        .get(`http://localhost:3222/review/ven/${id}`)
        .then((response) => {
          setReview(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const { confirm } = Modal;
  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const id = data;
          const endpoint = `http://localhost:3222/venues/delete/${id}`;
          const res = await axios.delete(endpoint, config);
          setTimeout(message.success("Tempat Berhasil Di Hapus"), 10000);
          router.push("/lokasi/tempat/list");
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href="/lokasi/tempat/list">List Tempat</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Tempat</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0 h-min-screen"}>
        <PageHeader pageName={"Detail Tempat"} />
        <div className="flex items-end justify-end mb-2">
          <Button
            href={`/lokasi/tempat/edit/${dataTempat?.data?.id}`}
            className="border-1 mr-5 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white"
          >
            Edit
          </Button>
          <Button
            className="rounded-lg border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
            onClick={() => {
              showDeleteConfirm(id);
            }}
          >
            Hapus
          </Button>
        </div>

        <div class="grid gap-4 grid-cols-2">
          <div>
            <Card className="shadow-md rounded-md">
              <Typography className="text-[22px] font-semibold ">
                {dataTempat?.data?.nama}
              </Typography>
              <div className="text-lg text-[#EBCB00]">
                <Rate disabled value={rating} className="mr-3 mb-2" />
                {dataTempat?.data?.rating}
              </div>

              <div className="flex text-sm inset-y-0 left-8 font-normal mb-3">
                <FcLike className="mr-3 w-4 h-4" />
                {dataTempat?.data?.like}
              </div>
              <div>
                <Swiper
                  loop={true}
                  spaceBetween={10}
                  navigation={true}
                  modules={[Navigation, Thumbs]}
                  grabCursor={true}
                  className="w-100"
                >
                  {ft.map((v) => {
                    return (
                      <SwiperSlide>
                        <img
                          src={
                            appConfig.apiUrl + "/upload/get-venue/" + v.image
                          }
                          alt="venue"
                          preview={false}
                          className="rounded-2xl h-80 w-fit "
                        />
                        <h1></h1>
                      </SwiperSlide>
                    );
                  })}
                </Swiper>
              </div>
              <div className="text-[#999999] flex items-end justify-end font-medium text-sm mt-2">
                <img src={"/transaction.png"} className="w-5 mr-2" />
                {dataTempat?.data?.order
                  ? dataTempat?.data?.order + "x dipesan"
                  : "0x dipesan"}
              </div>
            </Card>
          </div>
          <div className="relative">
            <Card className="shadow-md rounded-md h-[500px] ">
              <Typography className="flex items-end justify-end text-primary text-[24px] font-semibold">
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(dataTempat?.data?.price)}
              </Typography>
              <Form className="relative">
                <FormItem
                  name="deskripsi"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdAccountTree className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Deskripsi
                    </Typography>
                  </div>
                  {dataTempat?.data?.description}
                </FormItem>
                <FormItem
                  name="luas"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdCategory className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Luas Bangunan
                    </Typography>
                  </div>
                  {dataTempat?.data?.building_are}m
                </FormItem>
                <FormItem
                  name="kapasitas"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdFamilyRestroom className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Kapasitas
                    </Typography>
                  </div>
                  {dataTempat?.data?.max_capacity} Orang
                </FormItem>
                <FormItem
                  name="alamat"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0">
                    <MdLocationOn className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999]">
                      Alamat
                    </Typography>
                  </div>
                  {dataTempat?.data?.address}
                </FormItem>
                <FormItem
                  name="fasilitas"
                  className="flex text-[#999999] font-medium text-xs flex-row"
                >
                  <div className="flex inset-y-0 text-[#999999]">
                    <MdGppGood className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                    <Typography className="text-md font-bold text-[#999999] ">
                      Fasilitas
                    </Typography>
                  </div>
                  {fasilitas}
                </FormItem>
              </Form>
            </Card>
          </div>
        </div>
        <Card className="mt-5 rounded-md shadow-lg">
          <List
            className="comment-list"
            itemLayout="horizontal"
            dataSource={rv}
            renderItem={(item) => (
              <li>
                <Comment
                  author={item?.user?.name}
                  avatar={
                    appConfig.apiUrl + "/upload/get-user/" + item?.user?.photo
                  }
                  content={item.comment}
                  datetime={date(item.createdAt)}
                />
                <Rate disabled className="mr-3 ml-10" value={item.rate} />
                {item.rate}
              </li>
            )}
          />
        </Card>
      </Card>
    </div>
  );
};

detailTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default detailTempat;
