import React, { useEffect, useState } from "react";
import PageHeader from "../../../../components/Items/PageHeader";
import HeaderClient from "../../../../components/Layout/DesktopLayout/HeaderClient";
import {
  Card,
  Breadcrumb,
  Layout,
  Image,
  Typography,
  Form,
  Button,
  Comment,
  List,
} from "antd";
import { FcLike, FcLikePlaceholder } from "react-icons/fc";
import { Rate } from "antd";
import FormItem from "antd/lib/form/FormItem";
import {
  MdLocationOn,
  MdAccountTree,
  MdGppGood,
  MdCategory,
  MdFamilyRestroom,
} from "react-icons/md";
import { FaWhatsappSquare } from "react-icons/fa";
import { useRouter } from "next/router";
import axios from "axios";
import { tempatRepository } from "../../../../repository/tempat";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Navigation, Thumbs } from "swiper";
import { appConfig } from "../../../../config/app";
import config from "../../../../helper/config";
import useAuthenticationPage from "../../../../helper/authenticationPage";

const detailVenueClient = () => {
  const router = useRouter();
  const { id } = router.query;
  const [ft, setFoto] = useState([]);
  const [rv, setReview] = useState([]);
  const [fav, setFav] = useState();
  const [like, setLike] = useState();
  const [fasilitas, setFas] = useState([]);

  useEffect(() => {
    loadTempat();
    loadReview();
    loadFavorit();
    loadLike();
    loadFasilitas();
  }, [id]);

  const { data: dataTempat } = tempatRepository.hooks.useVenue(id);

  const handleClick = () => {
    router.push(`https://wa.me/${dataTempat?.data?.user}`);
  };

  const handleOrder = () => {
    const token = localStorage.getItem("token");
    if (token == null) {
      router.push("/auth/login");
    } else {
      router.push(`/pesanan/detail/${id}`);
    }
  };

  const rating = dataTempat?.data?.rating;

  const loadTempat = async () => {
    try {
      await axios
        .get(`http://localhost:3222/venues/detail-photo/${id}`)
        .then((response) => {
          setFoto(response.data);
        });
    } catch (e) {
      console.log(e);
    }
  };
  const loadFasilitas = async () => {
    try {
      await axios
        .get(`http://localhost:3222/facility/fasilitas/${id}`)
        .then((response) => {
          setFas(response.data);
        });
    } catch (e) {
      console.log(e);
    }
  };

  const loadReview = async () => {
    try {
      await axios
        .get(`http://localhost:3222/review/ven/${id}`)
        .then((response) => {
          setReview(response.data.data);
        });
    } catch (e) {
      console.log(e);
    }
  };

  const loadFavorit = async () => {
    try {
      await axios
        .get(`http://localhost:3222/myfavorit/detail/${id}`, config)
        .then((response) => {
          setFav(response.data.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadLike = async () => {
    try {
      await axios
        .get(`http://localhost:3222/venues/like/${id}`, config)
        .then((response) => {
          setLike(response.data.count);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };
  let a;

  const handleFav = async () => {
    await axios
      .post(`http://localhost:3222/myfavorit/create/${id}`, "", config)
      .then((response) => {
        loadLike();
        setFav("true");
      });
  };
  const deleteFav = async () => {
    await axios
      .delete(`http://localhost:3222/myfavorit/delete/${id}`, config)
      .then((response) => {
        loadLike();
        setFav("null");
      });
  };

  if (fav == "null") {
    a = (
      <div className="flex">
        <FcLikePlaceholder className="mr-3 w-4 h-4" onClick={handleFav} />{" "}
        {like} {" suka "}
      </div>
    );
  } else if (fav == "true") {
    a = (
      <div className="flex">
        <FcLike className="mr-3 w-4 h-4" onClick={deleteFav} /> {like}
        {" suka "}
      </div>
    );
  } else {
    a = (
      <div className="flex">
        <FcLike className="mr-3 w-4 h-4" /> {like}
        {" suka "}
      </div>
    );
  }

  const date = (date) => {
    return `${new Date(date).getFullYear()}-${new Date(
      date
    ).getMonth()}-${new Date(date).getDate()} || ${new Date(
      date
    ).getHours()}:${new Date(date).getMinutes()}`;
  };

  return (
    <Layout theme={"light"} className={"bg-[#E2E2E2] min-h-screen"}>
      <div>
        <HeaderClient />
        <Breadcrumb className="mb-2 mt-3 ml-5 text-[12px]">
          <Breadcrumb.Item>
            <a href="/dashboard/client">List Tempat</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Detail Tempat</Breadcrumb.Item>
        </Breadcrumb>

        <div className="px-5">
          <Card className="relative px-10 rounded-[10px] top-0 ">
            <PageHeader pageName={"Detail Tempat"} />
            <div class="grid gap-4 grid-cols-2">
              <div>
                <Card className="shadow-md rounded-md">
                  <Typography className="text-[22px] font-semibold ">
                    {dataTempat?.data?.nama}
                  </Typography>
                  <div className="text-lg text-[#EBCB00]">
                    <Rate disabled value={rating} className="mr-3 mb-2" />
                    {dataTempat?.data?.rating}
                  </div>
                  <div className="flex text-sm inset-y-0 left-8 font-normal mb-3">
                    {a}
                  </div>
                  <div>
                    <Swiper
                      loop={true}
                      spaceBetween={10}
                      navigation={true}
                      modules={[Navigation, Thumbs]}
                      grabCursor={true}
                      className="w-100"
                    >
                      {ft.map((v) => {
                        return (
                          <SwiperSlide>
                            <Image
                              src={
                                appConfig.apiUrl +
                                "/upload/get-venue/" +
                                v.image
                              }
                              alt="venue"
                              preview={false}
                              className="rounded-xl h-fit w-fit"
                            />
                          </SwiperSlide>
                        );
                      })}
                    </Swiper>
                  </div>
                  <div className="text-[#999999] flex items-end justify-end font-medium text-sm mt-2">
                    <img src={"/transaction.png"} className="w-5 mr-2" />
                    {dataTempat?.data?.order
                      ? dataTempat?.data?.order + "x dipesan"
                      : "0x dipesan"}
                  </div>
                </Card>
              </div>
              <div className="relative">
                <Card className="shadow-md rounded-md h-[553px] ">
                  <Typography className="flex items-end justify-end text-primary text-[24px] font-semibold ">
                    {new Intl.NumberFormat("id-ID", {
                      style: "currency",
                      currency: "IDR",
                    }).format(dataTempat?.data?.price)}
                  </Typography>
                  <Form className="relative">
                    <FormItem
                      name="deskripsi"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdAccountTree className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Deskripsi
                        </Typography>
                      </div>
                      {dataTempat?.data?.description}
                    </FormItem>
                    <FormItem
                      name="luas"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdCategory className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Luas Bangunan
                        </Typography>
                      </div>
                      {dataTempat?.data?.building_are}m
                    </FormItem>
                    <FormItem
                      name="kapasitas"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdFamilyRestroom className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Kapasitas
                        </Typography>
                      </div>
                      {dataTempat?.data?.max_capacity} Orang
                    </FormItem>
                    <FormItem
                      name="alamat"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0">
                        <MdLocationOn className="w-4 h-4 mb-2 mr-2  text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999]">
                          Alamat
                        </Typography>
                      </div>
                      {dataTempat?.data?.address}
                    </FormItem>
                    <FormItem
                      name="fasilitas"
                      className="flex text-[#999999] font-medium text-xs flex-row"
                    >
                      <div className="flex inset-y-0 text-[#999999]">
                        <MdGppGood className="w-4 h-4 mb-2 mr-2 text-[#999999]" />
                        <Typography className="text-md font-bold text-[#999999] ">
                          Fasilitas
                        </Typography>
                      </div>
                      {fasilitas.join(", ")}
                    </FormItem>
                  </Form>
                  <div className="flex inset-y-0 items-end justify-end">
                    <FaWhatsappSquare
                      className="w-8 h-8 text-green-600 mr-3"
                      onClick={handleClick}
                    />
                    <Button
                      onClick={handleOrder}
                      className="border-1 border-primary font-sm text-primary  rounded-lg px-3 py-1 inline-block hover:bg-primary hover:text-white"
                    >
                      Pesan Sekarang
                    </Button>
                  </div>
                </Card>
              </div>
            </div>
            <Card className="mt-5 rounded-md shadow-lg">
              <List
                className="comment-list"
                itemLayout="horizontal"
                dataSource={rv}
                renderItem={(item) => (
                  <li>
                    <Comment
                      author={item?.user?.name}
                      avatar={
                        appConfig.apiUrl +
                        "/upload/get-user/" +
                        item?.user?.photo
                      }
                      content={item.comment}
                      datetime={date(item.createdAt)}
                    />
                    <Rate disabled className="mr-3 ml-10" value={item.rate} />
                    {item.rate}
                    <img
                      src={
                        appConfig.apiUrl + "/upload/get-review/" + item.photo
                      }
                      width={100}
                      height={100}
                      className="ml-10 rounded-lg"
                    />
                  </li>
                )}
              />
            </Card>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default detailVenueClient;
