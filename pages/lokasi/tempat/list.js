import { Table, Tag, Button, message, Modal } from "antd";
import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import { Card, Breadcrumb } from "antd";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../components/StoreProvider";
import PageHeader from "../../../components/Items/PageHeader";
import Link from "next/dist/client/link";
import Search from "../../../components/Items/Search";
import axios from "axios";
import { parseJwt } from "../../../helper/parseJwt";
import useAuthenticationPage from "../../../helper/authenticationPage";
import config from "../../../helper/config";

const listTempat = observer(() => {
  const [size] = useState("middle");
  const [bottom] = useState("bottomCenter");
  const [center] = useState("none");
  const store = useStore();

  useEffect(() => {
    loadTempat("");
    // loadButton();
  }, []);

  const [stateTempat, setTempat] = useState();
  const color = (stats) => {
    let array = [];
    if (stats === "pending") {
      array[0] = "purple";
      array[1] = "Menunggu Dikonfirmasi";
    } else if (stats === "confirmed") {
      array[0] = "green";
      array[1] = "Dikonfirmasi";
    } else if (stats === "reported_by_client") {
      array[0] = "gold";
      array[1] = "Dilaporkan Pelanggan";
    } else if (stats === "reported_by_admin") {
      array[0] = "volcano";
      array[1] = "Dilaporkan Admin";
    } else if (stats === "rejected") {
      array[0] = "magenta";
      array[1] = "Ditolak";
    }
    return array;
  };

  const loadTempat = async (val) => {
    try {
      let token;
      if (typeof window !== "undefined") {
        token = localStorage.getItem("token");
      }
      const decodeJwt = parseJwt(token);
      let role;
      if (decodeJwt?.existUser) {
        role = decodeJwt?.existUser?.role?.name;
      }
      await axios
        .get(`http://localhost:3222/venues/list/${role}?search=${val}`, config)
        .then((response) => {
          setTempat(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const loadButton = () => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }
    const decodeJwt = parseJwt(token);
    let role;
    if (decodeJwt?.existUser) {
      role = decodeJwt?.existUser?.role?.name;
    }
    if (role == "admin") {
      return (
        <Button
          href="/lokasi/tempat/admin/tambah"
          className=" border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white"
        >
          + Tambah
        </Button>
      );
    } else {
      return (
        <Button
          href="/lokasi/tempat/tambah"
          className=" border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white"
        >
          + Tambah
        </Button>
      );
    }
  };

  const onChange = (e) => {
    loadTempat(e.target.value);
  };

  const { confirm } = Modal;
  function showDeleteConfirm(data) {
    confirm({
      title: "Hapus Data",
      content: "Anda yakin ingin menghapus?",
      okText: "Hapus",
      okType: "danger",
      cancelText: "Batal",
      async onOk() {
        try {
          const endpoint = `http://localhost:3222/venues/delete/${data.id}`;
          const res = await axios.delete(endpoint, config);
          setTimeout(message.success("Tempat Berhasil Di Hapus"), 10000);
          loadTempat("");
        } catch (e) {
          useAuthenticationPage(e);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const columns = [
    {
      title: "Nama Tempat",
      dataIndex: "name",
      render: (text, record) => (
        <Link href={`/lokasi/tempat/${record.id}`}>
          <a className="text-[#1363DF]">{text}</a>
        </Link>
      ),
    },
    {
      title: "Kota/Kabupaten",
      dataIndex: "district",
      render: (text) => <div>{text?.city?.name}</div>,
    },
    {
      title: "Kategori",
      dataIndex: "category",
      render: (text) => <div>{text?.name}</div>,
    },

    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      filters: [
        {
          text: <span>Menunggu Dikonfirmasi</span>,
          value: "pending",
        },
        {
          text: <span>Dikonfirmasi</span>,
          value: "confirmed",
        },
        {
          text: <span>Dilaporkan Pelanggan</span>,
          value: "reported_by_client",
        },
        {
          text: <span>Dilaporkan Admin</span>,
          value: "approved_cancel_by_owner",
        },
        {
          text: <span>Ditolak</span>,
          value: "rejected",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: true,
      width: "20%",
      render: (_, { status }) => (
        <Tag color={color(status)[0]} key={status}>
          {color(status)[1]}
        </Tag>
      ),
    },
    {
      title: "Aksi",
      key: "Aksi",
      render: (_, record) => (
        <Button
          className="rounded-lg  border-1 border-delete font-sm text-delete  hover:bg-delete hover:text-white"
          onClick={() => {
            showDeleteConfirm(record);
          }}
        >
          Hapus
        </Button>
      ),
    },
  ];

  const tableProps = {
    size,
  };

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>List Tempat</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0 h-min-screen"}>
        <PageHeader pageName={"List Tempat"} />
        <div className="flex justify-end items-end">
          {/* <Button
            href="/lokasi/tempat/tambah"
            className=" border-1 border-primary font-sm text-primary rounded-lg px-3 py-1 hover:bg-primary hover:text-white"
          >
            + Tambah
          </Button> */}
          {loadButton()}
        </div>
        <Search onChange={onChange} />
        <Table
          {...tableProps}
          pagination={{
            position: [center, bottom],
          }}
          columns={columns}
          dataSource={stateTempat}
        />
      </Card>
    </div>
  );
});

listTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default listTempat;
