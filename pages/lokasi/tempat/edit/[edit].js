import {
  Button,
  Form,
  message,
  Space,
  Card,
  Breadcrumb,
  Upload,
  Input,
  Modal,
  Select,
} from "antd";
import React, { useState, useEffect } from "react";
import DefaultLayout from "../../../../components/Layout/DefaultLayout";
import PageHeader from "../../../../components/Items/PageHeader";
import { PlusOutlined } from "@ant-design/icons";
import FormItem from "antd/lib/form/FormItem";
import Link from "next/link";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import { tempatRepository } from "../../../../repository/tempat";
import { useRouter } from "next/router";
import axios from "axios";
import { appConfig } from "../../../../config/app";
import { imageRepository } from "../../../../repository/image";
import config from "../../../../helper/config";

const editTempat = (props) => {
  const { TextArea } = Input;
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);
  const router = useRouter();
  const { edit } = router.query;
  const [form] = Form.useForm();
  const [previewVisible, setPreviewVisible] = useState(false);
  const [stateKategori, setKategori] = useState([]);
  const [provinsi, setProvinsi] = useState([]);
  const [cat, setCat] = useState("");
  const [kec, setKec] = useState("");
  const [kota, setKota] = useState([]);
  const [kecamatan, setKecamatan] = useState([]);

  const { data: dataVenue } = tempatRepository.hooks.useVenue(edit);

  useEffect(() => {
    editTempat();
    loadKategori();
    loadPhoto();
    loadProvinsi();
    loadKota(dataVenue?.data?.provId);
    loadKec(dataVenue?.data?.cityId);
  }, [dataVenue]);

  let dataKat = [];
  dataKat.push(stateKategori);
  const loadKategori = async () => {
    try {
      await axios
        .get("http://localhost:3222/category", config)
        .then((response) => {
          setKategori(response.data.items);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let dataProv = [];
  dataProv.push(provinsi);
  const loadProvinsi = async () => {
    try {
      await axios
        .get("http://localhost:3222/provinces", config)
        .then((response) => {
          setProvinsi(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let arr = [];
  const loadPhoto = async () => {
    try {
      await axios
        .get(
          `http://localhost:3222/venues/detail-photo/${dataVenue?.data?.id}`,
          config
        )
        .then((response) => {
          const b = response.data;
          b.map((b) => {
            arr.push({
              url: `${appConfig.apiUrl}/upload/get-venue/${b.image}`,
              name: b.image,
              status: "done",
            });
          });
          setFileList(arr);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let foto = [];

  const handleChange = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const processUpload = await imageRepository.manipulateData.upVenue(file);

      setFileList([
        ...fileList,
        {
          url:
            appConfig.apiUrl +
            "/upload/get-venue/" +
            processUpload.body.data.filename,
          name: processUpload.body.data.filename,
          status: "done",
        },
      ]);
      foto.push(processUpload.body.data.filename);

      message.success("Gambar Berhasil Di Unggah!");
    } catch (e) {
      setTimeout(message.error("Gambar Gagal Di Unggah Plis Deh"), 10000);
    }
  };
  let dataKota = [];
  dataKota.push(kota);
  const loadKota = async (idProv) => {
    try {
      await axios
        .get(`http://localhost:3222/cities/${idProv}`, config)
        .then((response) => {
          setKota(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  let dataKec = [];
  dataKec.push(kecamatan);
  const loadKec = async (idCity) => {
    try {
      await axios
        .get(`http://localhost:3222/districts/${idCity}`, config)
        .then((response) => {
          setKecamatan(response.data);
        });
    } catch (e) {
      useAuthenticationPage(e);
    }
  };

  const submitVenue = async () => {
    const id = dataVenue?.data?.id;
    const data = form.getFieldsValue();

    const dataAll = {
      name: data.nama,
      adress: data.alamat,
      building_area: data.luas_bangunan,
      max_capacity: data.kapasitas,
      price: data.harga,
      description: data.deskripsi,
      files: fileList,
      category: cat,
      district: kec,
    };
    try {
      const response = await axios.delete(
        `http://localhost:3222/venues/delete-photos/${edit}`,
        config
      );
      const res = await axios.put(
        `http://localhost:3222/venues/update/${id}`,
        dataAll,
        config
      );
      setTimeout(message.success("Data Berhasil Diperbarui!"), 10000);
      router.push(`/lokasi/tempat/list`);
    } catch (e) {
      useAuthenticationPage(e);
      setTimeout(message.error(e.response.data.error), 10000);
    }
  };

  const handleChangeOption = (value) => {
    setCat(value);
  };

  const onFinishFailed = () => {
    message.error("Data Gagal Diperbarui!");
  };

  const onChangeProv = (value) => {
    loadKota(value);
  };
  const onChangeKota = (value) => {
    loadKec(value);
  };

  const onChangeDist = (value) => {
    setKec(value);
  };

  const validateMessages = {
    required: "${label} Harus Diisi!",
  };

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    setPreviewImage(file.url);
    setPreviewVisible(true);
    setPreviewTitle("Gambar");
  };

  const handleChanges = ({ fileList: newFileList }) => setFileList(newFileList);

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div
        style={{
          marginTop: 8,
        }}
      >
        Ubah Foto
      </div>
    </div>
  );

  async function editTempat() {
    form.setFieldsValue({
      nama: await dataVenue?.data?.nama,
      alamat: await dataVenue?.data?.address,
      luas_bangunan: await dataVenue?.data?.building_are,
      kapasitas: await dataVenue?.data?.max_capacity,
      harga: await dataVenue?.data?.price,
      deskripsi: await dataVenue?.data?.description,
      category: await dataVenue?.data?.category,
      provinsi: await dataVenue?.data?.province,
      kota: await dataVenue?.data?.city,
      kecamatan: await dataVenue?.data?.district,
    });
  }

  return (
    <div className="mt-14">
      <Breadcrumb className="mt-2 mb-2 ml-5 text-[12px]">
        <Breadcrumb.Item>
          <Link href={"/lokasi/tempat/list"}>List Tempat</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link href={`/lokasi/tempat/${edit}`}>Detail Tempat</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Edit Tempat</Breadcrumb.Item>
      </Breadcrumb>

      <Card className={"relative px-3 ml-5 rounded-[10px] top-0"}>
        <PageHeader pageName={"Edit Tempat"} />
        <div className="flex flex-row  mt-5">
          <div className="flex flex-col items-center justify-between mt-5 relative ">
            <Card className="rounded-sm">
              <div className="">
                <Form
                  form={form}
                  layout="vertical"
                  onFinish={submitVenue}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                  validateMessages={validateMessages}
                >
                  <div>
                    <FormItem
                      name="nama"
                      label="Nama Tempat"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Input
                        placeholder="Masukan Nama Tempat"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1 "
                      />
                    </FormItem>

                    <FormItem
                      name="category"
                      label="Kategori"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Select
                        style={{
                          width: 380,
                        }}
                        onChange={handleChangeOption}
                      >
                        {dataKat[0].map((cat) => (
                          <Select.Option value={cat?.id}>
                            {cat?.name}
                          </Select.Option>
                        ))}
                      </Select>
                    </FormItem>

                    <FormItem
                      name="luas_bangunan"
                      label="Luas Bangunan"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Input
                        placeholder="Masukan Masukan Luas Bangunan"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1"
                      />
                    </FormItem>

                    <FormItem
                      name="kapasitas"
                      label="Kapasitas"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                      ]}
                    >
                      <Input
                        placeholder="Masukan Masukan Kapasitas (muatan Orang)"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1"
                      />
                    </FormItem>

                    <FormItem
                      name="deskripsi"
                      label="Deskripsi"
                      rules={[
                        {
                          type: "string",
                        },
                        {
                          type: "textarea",
                          warningOnly: true,
                        },
                      ]}
                    >
                      <TextArea
                        placeholder="Masukan Deskripsi"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-10 flex-1"
                      ></TextArea>
                    </FormItem>

                    <FormItem
                      name="provinsi"
                      label="Provinsi"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Select
                        className="font-light text-[12px]"
                        showSearch
                        optionFilterProp="children"
                        onChange={onChangeProv}
                        filterOption={(input, option) =>
                          option.children
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                      >
                        {dataProv[0].map((prov) => (
                          <Select.Option value={prov?.id}>
                            {prov?.name}
                          </Select.Option>
                        ))}
                      </Select>
                    </FormItem>

                    <FormItem
                      name="kota"
                      label="Kota"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Select
                        className="font-light text-[12px]"
                        showSearch
                        optionFilterProp="children"
                        onChange={onChangeKota}
                        filterOption={(input, option) =>
                          option.children
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                      >
                        {dataKota[0].map((kota) => (
                          <Select.Option value={kota?.id}>
                            {kota?.name}
                          </Select.Option>
                        ))}
                      </Select>
                    </FormItem>

                    <FormItem
                      name="kecamatan"
                      label="Kecamatan"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Select
                        className="font-light text-[12px]"
                        showSearch
                        optionFilterProp="children"
                        onChange={onChangeDist}
                        filterOption={(input, option) =>
                          option.children
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                      >
                        {dataKec[0].map((kec) => (
                          <Select.Option value={kec?.id}>
                            {kec?.name}
                          </Select.Option>
                        ))}
                      </Select>
                    </FormItem>

                    <FormItem
                      name="alamat"
                      label="Alamat"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                        {
                          type: "string",
                        },
                      ]}
                    >
                      <Input
                        placeholder="Masukan Masukan Alamat"
                        className="text-[black] bg-white outline-none w-96 font-light text-[12px] h-8 flex-1"
                      />
                    </FormItem>

                    <FormItem
                      name="harga"
                      label="Harga"
                      rules={[
                        {
                          type: "text",
                          warningOnly: true,
                        },
                      ]}
                    >
                      <Input prefix="Rp." suffix="IDR" />
                    </FormItem>

                    <Form.Item>
                      <Space>
                        <Button
                          className="rounded-lg absolute right-8 border-1 border-primary font-sm text-primary px-3 py-1 inline-block hover:bg-primary hover:text-white"
                          htmlType="submit"
                        >
                          Simpan
                        </Button>
                      </Space>
                    </Form.Item>
                  </div>
                </Form>
              </div>
            </Card>
          </div>
          <div className="px-10">
            <div className="mt-3 ml-[40px]">
              <Upload
                listType="picture-card"
                fileList={fileList}
                onPreview={handlePreview}
                customRequest={(args) => handleChange(args)}
                onChange={handleChanges}
                maxCount={5}
              >
                {uploadButton}
              </Upload>
              <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={null}
                onCancel={handleCancel}
              >
                <img
                  alt="gambar"
                  style={{
                    width: "100%",
                  }}
                  src={previewImage}
                />
              </Modal>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

editTempat.getLayout = function Layout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default editTempat;
