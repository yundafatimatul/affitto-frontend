import { Button, Result } from "antd";
import React from "react";

export default function Custom404() {
  return (
    <>
      <Result
        status="404"
        title="404"
        subTitle="Oops... Halaman yang anda cari tidak ada"
        extra={
          <Button type="primary" href="/">
            Kembali ke beranda
          </Button>
        }
      />
    </>
  );
}
