import Input from "antd/lib/input/Input";
import { Header } from "antd/lib/layout/layout";
import { useState } from "react";
import ImageVenueSlider from "../components/Items/ImageVenueSlider";
import UploadImage from "../components/Items/UploadImage";
import { venueImages } from "../public";

export default function Test() {
  const [text, setText] = useState("");
  const string = "ambil dari props";

  return (
    <div>
      <Header data={string} />
      <div className="flex flex-col items-center justify-center">
        {/* <h2 className="text-lg">{text}</h2>
        <Input onChange={(e) => setText(e.target.value)}></Input> */}
        <UploadImage />
      </div>
    </div>
  );
}
