const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["Poppins", "sans-serif"],
    },
    extend: {
      colors: {
        primary: "#1363DF",
        delete: "#dd4444",
        edit: "#ffdb4d",
        abu: "#E0E0E0",
        konfirmasi: "#5ab036",
      },
      transitionProperty: {
        width: "width",
        spacing: "margin, padding",
      },
      fontFamily: {
        sans: ["Poppins", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  important: "#tailwind-selector",
};
